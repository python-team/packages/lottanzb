<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="remote-access">
<info>
  <link type="guide" xref="index#advanced-functions"/>
  <link type="guide" xref="prefs"/>
  <link type="seealso" xref="web-interface"/>
  <desc>Gerenciador de downloads de outro computador</desc>
  <revision pkgversion="0.6" version="0.1" date="2010-09-13" status="review"/>
  <credit type="author">
    <name>Severin Heiniger</name>
    <email>severinheiniger@gmail.com</email>
  </credit>
  <copyright>
    <year>2010</year>
    <name>Grupo de desenvolvimento LottaNZB</name>
  </copyright>
  <license>
    <p>Creative Commons Attribution-Share Alike 3.0</p>
  </license>
</info>
<title>Acesso Remoto</title>
<p>É possível gerenciar os downloads de outro computador assim como transferi-los para o mesmo computador. Em algumas situações onde isso é útil.</p>
<list>
  <item>
    <p>O computador em casa está fazendo um download e você quer gerenciar os downloads usando o seu laptop enquanto você está distante.</p>
  </item>
  <item>
    <p>Você quer tornar possível para outra pessoa em sua rede para <link xref="adding-downloads">adicionar downloads</link>.</p>
  </item>
  <item>
    <p>The other computer is actually a device without a screen, such as a
    <em>Network-Attached Storage (NAS)</em>.</p>
  </item>
</list>
<p>Por padrão, não é possível para outra pessoa gerencia os downloads do seu computador por causa da sua privacidade.</p>
<section>
  <title>Primeiros Passos</title>
  <p>Para seguir os passos corretamente inicie usando o acesso remoto.</p>
  <steps>
    <item>
      <p>On the computer where downloads are taking place, allow remote access
      by selecting <guiseq>
      <gui style="menu">Edit</gui><gui style="menuitem">Preferences</gui>
      </guiseq>, navigating to the
      <gui style="tab">Remote Access</gui> tab and activating the check-box
      <gui style="checkbox">Allow access from other computers</gui>.
      It's recommended to protect the remote access by activating
      <gui style="checkbox">Require authentication</gui> and entering
      a username and a password.</p>
      <p>Confirme suas mudanças para fechar a janela.</p>
    </item>
    <item>
      <p>On the computer using which you want to manage the downloads:</p>
      <list>
        <item>
          <p>If <app>LottaNZB</app> is started for the first time, select 
          <gui style="checkbox">Manage the downloads of another computer</gui>
          in the <gui style="label">Start Downloading</gui> dialog.</p>
        </item>
        <item>
          <p>If you've already been using <app>LottaNZB</app>, select <guiseq>
          <gui style="menu">File</gui><gui style="menuitem">Monitor another
          Computer...</gui></guiseq>.</p>
        </item>
      </list>
      <p>Enter the name or IP address of the computer whose downloads you would
      like to manage. When asked, also enter the username and password
      you provided. Click <gui style="button">OK</gui> to start managing the
      downloads.</p>
    </item>
  </steps>
</section>
</page>
