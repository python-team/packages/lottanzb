<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="adding-downloads">
<info>
  <title type="sort">1</title>
  <link type="guide" xref="index#basic-functions"/>
  <link type="seealso" xref="managing-downloads"/>
  <desc>Add new downloads to the download list</desc>
  <revision pkgversion="0.6" version="0.1" date="2010-09-13" status="review"/>
  <credit type="author">
    <name>Severin Heiniger</name>
    <email>severinheiniger@gmail.com</email>
  </credit>
  <copyright>
    <year>2010</year>
    <name>Équipe de développement de LottaNZB</name>
  </copyright>
  <license>
    <p>Creative Commons Attribution-Share Alike 3.0</p>
  </license>
</info>
<title>Adding Downloads</title>
<p>You can add a download to <app>LottaNZB</app> by getting a NZB file
(typically from a Usenet search engine) and opening it using
<app>LottaNZB</app>, which will then download all files from the Usenet that are
listed in the NZB file.</p>
<p>There are many ways to add a NZB file.</p>
<terms>
  <item>
    <title>Sélection manuelle</title>
    <p>Choose one or more NZB files located on the hard drive after clicking on
    the <gui style="button">Add</gui> toolbar button or selecting <guiseq>
    <gui style="menu">File</gui><gui style="menuitem">Add File...</gui>
    </guiseq>.</p>
  </item>
  <item>
    <title>Glisser et déposer</title>
    <p>Drag one or more NZB files and drop them on <app>LottaNZB</app>'s
    main window.</p>
  </item>
  <item>
    <title>File Association</title>
    <p>Click on a NZB file in your file manager. NZB files are opened using
    <app>LottaNZB</app>.</p>
  </item>
  <item>
    <title>Download from URL</title>
    <p>Enter the URL of a NZB file in the dialog opened by selecting <guiseq>
    <gui style="menu">File</gui><gui style="menuitem">Add URL...</gui>
    </guiseq>, which will cause <app>LottaNZB</app> to download that NZB file.
    </p>
  </item>
  <item>
    <title><link xref="observed-nzb-folder">Automatically from Observed
    Folder</link></title>
    <p>New NZB files found in this folder will be added automatically.</p>
  </item>
</terms>
<note>
  <p>Usenet search engines usually indicate how many days ago the files
  listed in a NZB file have been uploaded to the Usenet. Please make sure that
  the retention time of your news server is longer, otherwise
  <app>LottaNZB</app> won't be able to complete the download because the
  files will have already been deleted from your news server.</p>
</note>
<p>Newly added downloads appear at the bottom of the download list after a
few seconds, which means that any previously added download will be completed
first until the new download will take place. Learn how to
<link xref="managing-downloads">manage the downloads</link> after they have been
added to <app>LottaNZB</app>.</p>
</page>
