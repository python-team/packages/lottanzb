<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="remote-access">
<info>
  <link type="guide" xref="index#advanced-functions"/>
  <link type="guide" xref="prefs"/>
  <link type="seealso" xref="web-interface"/>
  <desc>Maneje las descargas de otra computadora</desc>
  <revision pkgversion="0.6" version="0.1" date="2010-09-13" status="review"/>
  <credit type="author">
    <name>Severin Heiniger</name>
    <email>severinheiniger@gmail.com</email>
  </credit>
  <copyright>
    <year>2010</year>
    <name>Equipo de desarrollo LottaNZB</name>
  </copyright>
  <license>
    <p>Creative Commons Attribution-Share Alike 3.0</p>
  </license>
</info>
<title>Acceso remoto</title>
<p>Es posible gestionar las descargas de otra computador en la misma forma que las descargas de la misma computadora. Hay muchas situaciones donde esto es útil:</p>
<list>
  <item>
    <p>La computadora de la casa esta descargando y desea manejar la descarga con su computadora portátil mientras está ausente.</p>
  </item>
  <item>
    <p>Usted quiere permitir que otra gente en la red local <link xref="adding-downloads">agregue descargas</link>.</p>
  </item>
  <item>
    <p>La otra computadora es realmente un dispositivo sin pantalla, como un <em>Network-Attached Storage (NAS)</em>.</p>
  </item>
</list>
<p>De forma predeterminada, no es posible administrar las descargas de su equipo desde otra computadora por su privacidad.</p>
<section>
  <title>Empezando</title>
  <p>Siga los siguientes pasos para iniciar de forma segura el acceso remoto.</p>
  <steps>
    <item>
      <p>En la computadora donde las descargas tenienen lugar, permita el acceso remoto mediante la selección de <guiseq><gui style="menu">Editar</gui><gui style="menuitem">Preferencias</gui></guiseq>, navegando a <gui style="tab">acceso remoto</gui> y activar la casilla de verificación <gui style="checkbox">Permitir el acceso desde otras computadoras</gui>. Se recomienda proteger el acceso remoto mediante la activación de <gui style="checkbox">Requerir autenticación</gui> e introduzca un nombre de usuario y una contraseña.</p>
      <p>Confirme los cambios al cerrar la ventana.</p>
    </item>
    <item>
      <p>En la computadora que quiere manejar las descargas:</p>
      <list>
        <item>
          <p>Si <app>LottaNZB</app> se inicia por primera vez, seleccione <gui style="checkbox">Administrar las descargas de otra computadora</gui> en el diálogo <gui style="label">empieze a descargar</gui>.</p>
        </item>
        <item>
          <p>Si ya ha estado usando <app>LottaNZB</app>, seleccione <guiseq><gui style="menu">Archivo</gui><gui style="menuitem">Manejar otra computadora...</gui></guiseq>.</p>
        </item>
      </list>
      <p>Introduzca el nombre o la dirección IP de la computadora cuya descargas desea administrar. Cuando se le pregune, también entre el nombre de usuario y contraseña que usted proporcionó. Haga clic en <gui style="button">Aceptar</gui> para empezar a manejar las descargas.</p>
    </item>
  </steps>
</section>
</page>
