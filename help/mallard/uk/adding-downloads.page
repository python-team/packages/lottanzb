<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="adding-downloads">
<info>
  <title type="sort">1</title>
  <link type="guide" xref="index#basic-functions"/>
  <link type="seealso" xref="managing-downloads"/>
  <desc>Додавання нових завантажень до списку</desc>
  <revision pkgversion="0.6" version="0.1" date="2010-09-13" status="review"/>
  <credit type="author">
    <name>Severin Heiniger</name>
    <email>severinheiniger@gmail.com</email>
  </credit>
  <copyright>
    <year>2010</year>
    <name>Команда розробників LottaNZB</name>
  </copyright>
  <license>
    <p>Creative Commons Attribution-Share Alike 3.0</p>
  </license>
</info>
<title>Додавання завантажень</title>
<p>Ви можете додавати завантаження до <app>LottaNZB</app> отримуючи NZB-файл (зазвичай, з пошукових рушіїв Usenet) та відкривши його в <app>LottaNZB</app>, яка завантажить всі файли з Usenet, що перелічені в NZB-файлі.</p>
<p>Можна додавати NZB-файли кількома способами.</p>
<terms>
  <item>
    <title>Вибір вручну</title>
    <p>Виберіть один або кілька NZB-файлів розташованих на жорсткому диску натиснувши кнопку <gui style="button">Додати</gui> на панелі інструментів або виберіть в меню <guiseq><gui style="menu">Файл</gui><gui style="menuitem">Додати файл...</gui></guiseq>.</p>
  </item>
  <item>
    <title>Перетягування</title>
    <p>Перетягніть один або кілька NZB-файлів до головного вікна <app>LottaNZB</app>.</p>
  </item>
  <item>
    <title>Прив’язка файла</title>
    <p>Клацніть на NZB-файлі у файловому менеджері. NZB-файли відкриваються за допомогою <app>LottaNZB</app>.</p>
  </item>
  <item>
    <title>Завантажити з адреси</title>
    <p>Введіть адресу NZB-файла в діалозі, що відкривається з меню <guiseq><gui style="menu">Файл</gui><gui style="menuitem">Додати адресу...</gui></guiseq> і <app>LottaNZB</app> завантажить цей NZB-файл.</p>
  </item>
  <item>
    <title><link xref="observed-nzb-folder">Автоматично, з теки для спостереження</link></title>
    <p>Нові NZB-файли, знайдені в цій теці, завантажаться автоматично.</p>
  </item>
</terms>
<note>
  <p>Пошукові рушії Usenet, зазвичай вказують, скільки днів тому файли, перелічені в NZB-файлі були завантажені до Usenet. Будь ласка, переконайтеся, що час утримування вашого сервера не збіг, в іншому випадку <app>LottaNZB</app> не зможе завершити завантаження, тому що файли можуть бути вже вилучені з сервера.</p>
</note>
<p>Щойно додані завантаження, з’являються внизу списку завантаження за кілька секунд, це означає, що спочатку будуть завершені всі раніше додані завантаження, а лише потім розпочнеться щойно додане. Читайте тут, як <link xref="managing-downloads">керувати завантаженнями</link> після додавання їх до <app>LottaNZB</app>.</p>
</page>
