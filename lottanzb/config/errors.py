# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""
Common errors for the `config` module.
"""

from lottanzb.core.environ import _

__all__ = [
    "OptionError",
    "InexistentOptionError",
    "InvalidOptionError",
    "ConfigError",
    "LoadingError",
    "ConfigNotFoundError",
    "ParsingError",
    "SavingError"
]
    
class OptionError(Exception):
    """Raised if there is a problem accessing or changing an option"""
    
    def __init__(self, section, option):
        self.section = section
        self.option = option
        
        Exception.__init__(self)

class InexistentOptionError(OptionError, AttributeError, IndexError, KeyError):
    """Raised when trying to access an option that doesn't exist"""
    
    def __init__(self, section, option):
        OptionError.__init__(self, section, option)
        AttributeError.__init__(self)
        IndexError.__init__(self)
        KeyError.__init__(self)
    
    def __str__(self):
        return _("Invalid option '%s' in section '%s'.") % \
            (self.option, self.section.get_full_name())

class InvalidOptionError(OptionError, ValueError):
    """Raised when trying to assign an invalid value to an option"""
    
    def __init__(self, section, option):
        OptionError.__init__(self, section, option)
        ValueError.__init__(self)
    
    def __str__(self):
        return _("Invalid value for option '%s' in section '%s'.") % \
            (self.option, self.section.get_full_name())

class ConfigError(Exception):
    """Raised when a configuration file cannot be loaded or saved."""
    
    def __init__(self, config_root, message=""):
        self.config_root = config_root
        
        if message:
            self.message = message
        
        Exception.__init__(self)
    
class LoadingError(ConfigError):
    """Raised when a configuration file cannot be loaded."""
    
    def __str__(self):
        return _("Unable to load the configuration file %s: %s") % \
            (self.config_root.config_file, self.message)

class ConfigNotFoundError(LoadingError):
    """Raised when a configuration file cannot be found."""
    
    message = _("File not found.")

class ParsingError(LoadingError):
    """Raised when a configuration file does not follow legal syntax."""
    
    def __init__(self, config_root, line_no, message):
        message = _("Syntax error on line %i: %s") % (line_no, message)
        
        LoadingError.__init__(self, config_root, message)

class SavingError(ConfigError):
    """Raised when a configuration file cannot be saved."""
    
    def __str__(self):
        return _("Unable to save the configuration file %s: %s") % \
            (self.config_root.config_file, self.message)
