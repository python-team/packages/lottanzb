# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import logging
import collections

# pylint: disable-msg=C0103
LOG = logging.getLogger(__name__)

from lottanzb import resources
from lottanzb.core.environ import _
from lottanzb.core.component import Component, depends_on
from lottanzb.util.gobject_extras import gproperty
from lottanzb.config.section import ConfigSection
from lottanzb.config.roots import IniPathConfigRoot
from lottanzb.config.errors import (
    LoadingError,
    ConfigNotFoundError,
    OptionError,
    InexistentOptionError
)

__all__ = ["LottaConfig", "ConfigComponent", "ConfigurableComponent"]

class Config(IniPathConfigRoot):
    """
    Root ConfigSection that loads and saves the LottaNZB configuration file.
    """
    
    backend = gproperty(type=object)
    gui = gproperty(type=object)
    plugins = gproperty(type=object)
    
    revision = gproperty(type=int, default=5, minimum=1)
    
    def __init__(self, config_file):
        IniPathConfigRoot.__init__(self, config_file, "lottanzb")
    
    def __getinitargs__(self):
        return [self.config_file]
    
    def load(self, custom_config_file=""):
        IniPathConfigRoot.load(self, custom_config_file)
        
        self.upgrade()
    
    def upgrade(self):
        """
        Upgrades out-of-date configuration data based on the revision stored in
        the `revision` option.
        """
        
        # The configuration revision of a configuration file was previously
        # stored in the section `core` as `config_revision`. Now that it's
        # possible for the root section to have options, it has been moved.
        try:
            self.revision = self.core.config_revision
        except InexistentOptionError:
            pass
        else:
            self.delete_unknown_property("core")
        
        current_revision = self.revision
        newest_revision = self.get_property_default_value("revision")
        
        for revision in range(current_revision, newest_revision):
            method_name = "upgrade_%i_to_%i" % (revision, revision + 1)
            upgrade_method = getattr(self, method_name, None)
            
            if isinstance(upgrade_method, collections.Callable):
                upgrade_method()
        
        self.revision = newest_revision
        
        if current_revision != newest_revision:
            self.save()
            
            if newest_revision > current_revision:
                LOG.info(_("Configuration upgraded to revision %i."),
                    newest_revision)
    
    def try_to_get_property(self, path, default=None):
        """
        Invokes the `get_by_path` method and ignores any `OptionError`s that
        might be raised.
        
        The second argument `default` may be used to specify a default value
        that is returned if an `OptionError` occurs.
        """
        
        try:
            return self.get_by_path(path)
        except OptionError as error:
            LOG.debug(str(error))
            
            return default
    
    def try_to_set_property(self, path, value):
        """
        Invokes the `set_by_path` method and ignores any `OptionError`s that
        might be raised.
        
        `ValueError`s will not be catched because they need to be fixed by the
        developer.
        """
        
        try:
            self.set_by_path(path, value)
        except OptionError as error:
            LOG.debug(str(error))
    

    def try_to_move_property(self, src_path, dest_path):
        """
        Invokes the `move_unknown_property` method and ignores any
        `OptionError`s that might be raised.
        
        `ValueError`s will not be catched because they need to be fixed by the
        developer.
        """
        
        try:
            self.move_unknown_property(src_path, dest_path)
        except OptionError as error:
            LOG.debug(str(error))
    
    def try_to_delete_property(self, path):
        """
        Invokes the `delete_unknown_property` method and ignores any
        `OptionError`s that might be raised.
        
        `ValueError`s will not be catched because they need to be fixed by the
        developer.
        """
        
        try:
            section = self.get_by_path(path[:-1])
            section.delete_unknown_property(path[-1])
        except OptionError as error:
            LOG.debug(str(error))
    
    def upgrade_1_to_2(self):
        """
        Separating the GUI code from LottaNZB's core and and the creation of
        the "start_minimized" plug-in made it necessary to move options.
        """
        
        for key in ["height", "width", "x_pos", "y_pos", "maximized"]:
            self.try_to_move_property(
                ["gui", "window_%s" % key],
                ["gui", "main", key])
        
        self.try_to_move_property(
            ["gui", "start_minimized"],
            ["plugins", "start_minimized", "enabled"])
    
    def upgrade_2_to_3(self):
        """The "notification_area" plug-in has been renamed to "panel_menu"."""
        
        self.try_to_move_property(
            ["plugins", "notification_area"],
            ["plugins", "panel_menu"])
    
    def upgrade_3_to_4(self):
        """The 'newzbin' plug-in has been removed."""
        
        self.try_to_delete_property(["plugins", "newzbin"])
    
    def upgrade_4_to_5(self):
        session_type_map = {
            "standalone": "local",
            "local_frontend": "local",
            "remote_frontend": "remote"
        }
        
        active_mode = self.try_to_get_property(["modes", "active"], "")
        
        self.try_to_set_property(
            ["backend", "sessions", "active"],
            session_type_map.get(active_mode, ""))
        
        self.try_to_move_property(
            ["modes", "remote_frontend", "address"],
            ["backend", "sessions", "remote", "host"])
        
        self.try_to_delete_property(["modes"])
        self.try_to_delete_property(["backend", "update_interval"])
        
        self.try_to_delete_property(
            ["plugins", "categories", "categories_list"])
        self.try_to_delete_property(
            ["plugins", "categories", "separator"])


class ConfigComponent(Component):
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        self.root = Config(resources.get_config("lottanzb.conf"))
        
        try:
            self.root.load()
        except ConfigNotFoundError:
            # Don't log an error message if the configuration doesn't exist yet
            # This is the case during the first launch of LottaNZB.
            pass
        except LoadingError as error:
            LOG.error(str(error))
    
    def on_unload(self):
        self.root.save()


class ConfigurableComponent(Component):
    depends_on(ConfigComponent)
    
    _config = None
    
    @property
    def config(self):
        if self._config is None:
            self._config = self.get_config_section(self._component_manager)
        
        return self._config
    
    @classmethod
    def get_config_path(cls):
        return ConfigSection.name_to_path(cls.__module__)[1:]
    
    @classmethod
    def get_config_section(cls, component_manager):
        path = cls.get_config_path()
        config_root = component_manager.load(ConfigComponent).root
        config_root.create_path(path)
        
        return config_root.get_by_path(path)
