# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.config import ConfigSection
from lottanzb.util.gobject_extras import gproperty

class Config(ConfigSection):
    username = gproperty(type=str)
    bookmark_rate = gproperty(type=int, default=60, minimum=60)
    bookmarks = gproperty(type=bool, default=False)
    password = gproperty(type=str)
    unbookmark = gproperty(type=bool, default=True)
    
    # Available in SABnzbd >= 0.5.4.
    https = gproperty(type=bool, default=True)
