# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import locale

from os.path import expanduser, join

from lottanzb.core.constants import DefaultPortNumber, Priority, PostProcessing
from lottanzb.core.environ import _
from lottanzb.config import ConfigSection
from lottanzb.util.gobject_extras import gproperty
from lottanzb.resources.xdg import DATA_HOME, USER_DIRS

# The default SABnzbd data directory has been changed in order to adhere to
# the corresponding freedesktop.org standard. Existing users won't be affected.
_PREFIX_DIR = join(DATA_HOME, "sabnzbd")
_DOWNLOAD_DIR = USER_DIRS.get("XDG_DOWNLOAD_DIR", \
    expanduser(join("~", _("Downloads"))))
_INCOMPLETE_DIR = join(_DOWNLOAD_DIR, _("Incomplete"))

# Hard-coded list of languages supported by SABnzbd as they're not exposed
# through the API and locating them on the local machine will not work in many
# cases and might be a costly operation anyway.
# Therefore, this list needs to be updated by hand.
# TODO: Ask the SABnzbd developers to expose the list of supported languages
# through the API.
_AVAILABLE_LANGS = ["us-en", "de-de", "fr-fr", "sv-se", "nl-du"]

def get_default_lang():
    """
    Maps the current locale to a supported SABnzbd language.
    
    If it's not possible to do so, it will fall back to English ("us-en").
    """
    
    current_lang = locale.getlocale()[0].lower().replace("_", "-")
    
    if current_lang == "c":
        return "us-en"
    else:
        if current_lang in _AVAILABLE_LANGS:
            # A perfect match. Hooray!
            return current_lang
        elif "-" in current_lang:
            current_lang = current_lang.split("-")[0]
            
            for sabnzb_lang in _AVAILABLE_LANGS:
                if sabnzb_lang.startswith(current_lang):
                    return sabnzb_lang
    
    return "us-en"


class Config(ConfigSection):
    _list_options = ["movie_categories", "date_categories", "schedlines"]
    
    quick_check = gproperty(type=bool, default=True)
    fail_on_crc = gproperty(type=bool, default=False)
    send_group = gproperty(type=bool, default=False)
    sfv_check = gproperty(type=bool, default=True)
    
    email_server = gproperty(type=str)
    email_to = gproperty(type=str)
    email_from = gproperty(type=str)
    email_account = gproperty(type=str)
    email_pwd = gproperty(type=str)
    email_endjob = gproperty(type=bool, default=False)
    email_full = gproperty(type=bool, default=False)
    email_dir = gproperty(type=str)
    
    dirscan_opts = gproperty(type=int, default=PostProcessing.DELETE)
    dirscan_script = gproperty(type=str, default="None")
    dirscan_priority = gproperty(type=int, default=Priority.NORMAL)
    dirscan_speed = gproperty(type=int, default=5)
    
    auto_browser = gproperty(type=bool, default=False)
    check_new_rel = gproperty(type=bool, default=True)
    
    enable_unrar = gproperty(type=bool, default=True)
    enable_unzip = gproperty(type=bool, default=True)
    enable_filejoin = gproperty(type=bool, default=True)
    enable_tsjoin = gproperty(type=bool, default=True)
    enable_par_cleanup = gproperty(type=bool, default=True)
    
    # When True, only perform QuickCheck and par2-verify, but never par2-repair.
    # For low-performance systems that have trouble running repairs.
    # Available in SABnzbd 0.6 and newer.
    never_repair = gproperty(type=bool, default=False)
    
    par_option = gproperty(type=str)
    
    # The most conservative values possible that will cause post-processing
    # to have the lowest possible impact on the system's performance.
    # One might consider adjusting these values in the future.
    nice = gproperty(type=str, default="-n 19")
    ionice = gproperty(type=str, default="-c3")
    ignore_wrong_unrar = gproperty(type=bool, default=False)
    par2_multicore = gproperty(type=bool, default=True)
    
    # Available in SABnzbd 0.5.4 and newer.
    # Only affects 64-bit Windows installations.
    allow_64bit_tools = gproperty(type=bool, default=True)
    
    # Let jobs without a post-processing operation directly move to the history,
    # preserving the temporary folder. Afterwards the user can decide to
    # post-process or delete the job.
    # Available in SAbnzbd 0.6 and newer.
    allow_streaming = gproperty(type=bool, default=False)
    
    top_only = gproperty(type=bool, default=True)
    auto_disconnect = gproperty(type=bool, default=True)
    queue_complete = gproperty(type=str)
    
    # Available in SABnzbd 0.5.4 and newer.
    queue_complete_pers = gproperty(type=bool, default=True)
    
    replace_illegal = gproperty(type=bool, default=True)
    replace_spaces = gproperty(type=bool, default=False)
    replace_dots = gproperty(type=bool, default=False)
    
    no_dupes = gproperty(type=bool, default=True)
    ignore_samples = gproperty(type=bool, default=False)
    create_group_folders = gproperty(type=bool, default=False)
    auto_sort = gproperty(type=bool, default=False)
    folder_rename = gproperty(type=bool, default=True)
    folder_max_length = gproperty(type=int, default=2 ** 8, minimum=20,
        maximum=2 ** 16)
    
    safe_postproc = gproperty(type=bool, default=False)
    pause_on_post_processing = gproperty(type=bool, default=False)
    
    schedlines = gproperty(type=object)
    
    enable_tv_sorting = gproperty(type=bool, default=False)
    tv_sort_string = gproperty(type=str)
    tv_sort_countries = gproperty(type=bool, default=True)
    
    enable_movie_sorting = gproperty(type=bool, default=False)
    movie_sort_string = gproperty(type=str)
    movie_sort_extra = gproperty(type=str, default="-cd%1")
    movie_extra_folder = gproperty(type=bool, default=False)
    movie_categories = gproperty(type=object)
    
    enable_date_sorting = gproperty(type=bool, default=False)
    date_sort_string = gproperty(type=str)
    date_categories = gproperty(type=object)
    
    config_lock = gproperty(type=bool, default=False)
    
    permissions = gproperty(type=str)
    download_dir = gproperty(type=str, default=_INCOMPLETE_DIR)
    download_free = gproperty(type=str)
    complete_dir = gproperty(type=str, default=_DOWNLOAD_DIR)
    cache_dir = gproperty(type=str, default="cache")
    admin_dir = gproperty(type=str, default="admin")
    log_dir = gproperty(type=str, default="logs")
    
    nzb_backup_dir = gproperty(type=str)
    script_dir = gproperty(type=str)
    dirscan_dir = gproperty(type=str)
    
    host = gproperty(type=str, default="localhost")
    port = gproperty(type=int, default=DefaultPortNumber.HTTP, minimum=1,
        maximum=2 ** 16 - 1)
    
    username = gproperty(type=str)
    password = gproperty(type=str)
    
    # Available in SABnzbd 0.5.5 and newer.
    login_realm = gproperty(type=str, default="SABnzbd")
    
    bandwidth_limit = gproperty(type=int)
    refresh_rate = gproperty(type=int)
    rss_rate = gproperty(type=int, default=60, minimum=15, maximum=24 * 60)
    cache_limit = gproperty(type=str)
    web_dir = gproperty(type=str, default="Default")
    web_dir2 = gproperty(type=str)
    web_color = gproperty(type=str)
    web_color2 = gproperty(type=str)
    cleanup_list = gproperty(type=object)
    
    enable_https = gproperty(type=bool, default=False)
    https_port = gproperty(type=int, default=DefaultPortNumber.HTTPS,
        minimum=1, maximum=2 ** 16 - 1)
    https_key = gproperty(type=str, default="server.key")
    https_cert = gproperty(type=str, default="server.cert")
    ssl_type = gproperty(type=str, default="v23")
    
    language = gproperty(type=str, default=get_default_lang())
    unpack_check = gproperty(type=bool, default=True)
    no_penalties = gproperty(type=bool, default=False)
    
    api_key = gproperty(type=str)
    disable_api_key = gproperty(type=bool, default=False)
    
    # Available in SABnzbd 0.5.4 and newer.
    api_warnings = gproperty(type=bool, default=True)
    
    def __init__(self, name, parent=None, options=None):
        ConfigSection.__init__(self, name, parent, options)
        
        self.add_validation_function(self._validate_passwords)
    
    def reset_object_options(self):
        self.cleanup_list = [".nfo", ".sfv"]
        self.movie_categories = ["movies"]
        self.date_categories = ["tv"]
    
    def _validate_passwords(self, key, value):
        """
        Ensure that a clear-text password isn't overwritten by a masked password
        (e. g. *******) received by SABnzbd.
        
        SABnzbd never transmits clear-text passwords over the network when
        requested by a `GetConfigQuery`.
        
        However, if we don't know the actual password, it's safe to store the
        masked password, because we need to know that there *is* a password.
        
        TODO: Move this to a more general location because we'll also need this
        in other parts of the SABnzbd configuration.
        """
        
        if key == "password" and "*"*len(value) == value and \
            len(self.password) == len(value):
            return self.password
        else:
            return value
