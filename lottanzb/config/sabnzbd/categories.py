# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import gproperty
from lottanzb.core.constants import Priority
from lottanzb.config.sabnzbd import ListConfigSection, ListElementConfigSection

class Config(ListConfigSection):
    def reset_object_options(self):
        categories = {
            "misc":      _("Miscellaneous"),
            "tv":        _("TV"),
            "unknown":   _("Unknown"),
            "resources": _("Resources"),
            "apps":      _("Apps"),
            "movies":    _("Movies"),
            "consoles":  _("Consoles"),
            "books":     _("Books"),
            "games":     _("Games"),
            "anime":     _("Anime"),
            "music":     _("Music"),
            "pda":       _("PDA"),
            "emulation": _("Emulation")
        }
        
        for key, value in categories.items():
            self.append({
                "name": key,
                "newzbin": key.capitalize(),
                "dir": value
            })
    
    def find_section_class(self, section):
        return CategoryConfig

class CategoryConfig(ListElementConfigSection):
    name = gproperty(type=str)
    newzbin = gproperty(type=str)
    priority = gproperty(type=int, default=Priority.NORMAL)
    pp = gproperty(type=str)
    dir = gproperty(type=str)
    script = gproperty(type=str, default="Default")
    
    def get_identifier(self):
        return self.name
    
    def set_identifier(self, identifier):
        self.name = identifier
