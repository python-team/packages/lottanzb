# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.config import ConfigSection
from lottanzb.util.gobject_extras import gproperty

class Config(ConfigSection):
    # TODO: Should this configuration option ever be used by the rest of the
    # application, an integer like 5*2**20 would be more suitable for storing
    # the value.
    max_log_size = gproperty(type=str, default="5M")
    log_level = gproperty(type=int, default=0)
    log_backups = gproperty(type=int, default=5, minimum=1, maximum=2 ** 10)
    log_new = gproperty(type=bool, default=False)
    enable_cherrypy_logging = gproperty(type=bool, default=False)
