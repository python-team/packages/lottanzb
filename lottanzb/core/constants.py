# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.core.environ import _

__all__ = ["DefaultPortNumber", "Priority", "PostProcessing"]

class DefaultPortNumber:
    HTTP = 8080
    HTTPS = 9090


class Priority:
    TOP = 2
    HIGH = 1
    NORMAL = 0
    LOW = -1
    
    @classmethod
    def to_pretty_string(cls, priority):
        """Return a printable string given a numerical download priority."""
        
        MAP = {
            cls.TOP: _("Force"),
            cls.HIGH: _("High"),
            cls.NORMAL: _("Normal"),
            cls.LOW: _("Low")
        }
        
        try:
            return MAP[priority]
        except KeyError:
            raise ValueError("Unknown priority: %r", priority)


class PostProcessing:
    NOTHING = 0
    REPAIR = 1
    UNPACK = 2
    DELETE = 3
