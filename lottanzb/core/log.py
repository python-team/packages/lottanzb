# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import sys

import logging
LOG = logging.getLogger(__name__)

from traceback import format_exc

from lottanzb import resources
from lottanzb.core.environ import _
from lottanzb.core.component import Component
from lottanzb.util.gobject_extras import gsignal

__all__ = ["LoggingComponent"]

class LoggingComponent(Component):
    """Store and handle all LottaNZB log messages.
    
    All log messages are displayed on the console and written to a log file.
    """
    
    LOG_FILE = resources.get_config("log")
    
    # Emitted when a new message is logged. The only parameter is the
    # `logging.LogRecord` object.
    gsignal("record-added", object)
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        # Holds all `LogRecord`s created since the component was loaded.
        self._records = []
        
        # Setup console logging
        self._console_handler = logging.StreamHandler()
        self._console_handler.setLevel(logging.INFO)
        self._console_handler.setFormatter(logging.Formatter(
            "%(asctime)s %(name)-25s %(message)s",
            datefmt="%T"))
        
        # Setup file logging
        self._file_handler = logging.FileHandler(self.LOG_FILE, "w")
        self._file_handler.setFormatter(logging.Formatter(
            "%(asctime)s %(name)-25s %(levelname)-8s %(message)s",
            datefmt="%F %T"))
        
        self._simple_handler = SimpleLoggingHandler(self.on_log_record_emitted)
        
        root = logging.getLogger()
        root.addHandler(self._console_handler)
        root.addHandler(self._file_handler)
        root.addHandler(self._simple_handler)
        root.setLevel(logging.DEBUG)
        
        # Install a custom exception hook so that unhandled thread exceptions
        # produce a proper log entry.
        sys.excepthook = self.custom_except_hook
    
    def set_console_level(self, level):
        """Set the minimum importance level for log records in the console."""
        
        self._console_handler.setLevel(level)
    
    def get_console_level(self):
        """Get the minimum importance level for log records in the console."""
        
        return self._console_handler.level
    
    console_level = property(get_console_level, set_console_level)
    
    @property
    def records(self):
        """Returns a shallow copy of the array holding all `LogRecord`s."""
        
        return self._records[:]
    
    def on_log_record_emitted(self, value):
        """Append the `logging.LogRecord' `value' to `_records' and emit
        the 'record-added' signal.
        """
        
        self._records.append(value)
        self.emit("record-added", value)
    
    @staticmethod
    def custom_except_hook(type, value, tb):
        """Logs an unhandled exception with the highest importance.
        
        Besides an introductory text, a traceback is included in the message.
        """
        
        exception = format_exc()
        
        # When the exception is raised in the main thread, `format_exc' doesn't
        # produce the expected output. As the application will crash anyway in
        # such a case, it's fine to simply let the default exception hook take
        # care of printing the exception to the console.
        if exception.strip() != "None":
            LOG.error(_("An unhandled exception occurred:") + "\n" + exception)
        else:
            sys.__excepthook__(type, value, tb)


class SimpleLoggingHandler(logging.Handler):
    """Passes any received `logging.LogRecord' as an argument to a callback
    function specified upon construction.
    """
    
    def __init__(self, callback):
        self._callback = callback
        
        logging.Handler.__init__(self)
    
    def emit(self, log_record):
        self._callback(log_record)
