# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""
Component management system inspired by the one used by Trac.

The following description originates from
http://trac.edgewall.org/wiki/TracDev/ComponentArchitecture

For our purposes, a component is an object that provides a certain type of
service within the context of the application. There is at most one instance of
any component: components are singletons. That implies that a component
does not map to an entity of the application's object model; instead,
components represent functional subsystems.

Components can declare "extension points" that other components can plug in to.
This allows one component to enhance the functionality of the component it
extends, without the extended component even knowing that the extending
component exists.

A component can extend any number of other components and still offer its own
extension points. This allows a plug-in to itself offer a plug-in API (i.e.
extension point). This feature is the basis for a plug-in-based architecture. 

The actual functionality and APIs are defined by individual components.
The component kernel provides the "magic glue" to hook the different subsystems
together - without them necessarily knowing about each other.
"""

import sys

from logging import getLogger
LOG = getLogger(__name__)

from threading import Lock, Thread

from lottanzb.util.gobject_extras import GObject, GObjectMeta, gsignal

__all__ = ["ComponentError", "UnknownComponentError",
    "ComponentInstantiationError", "Interface", "ExtensionPoint", "Component",
    "implements", "depends_on", "ComponentManager", "ComponentError"]

class ComponentError(Exception):
    """
    Abstract base class for component-related exceptions.
    
    @param component: A `Component` subclass or instance.
    @param message: An optional message of what exactly went wrong.
    """
    
    message = ""
    
    def __init__(self, component, message=""):
        self.component = component
        self.message = message or self.message
        
        Exception.__init__(self)


class ComponentNotRegisteredError(ComponentError):
    def __str__(self):
        return "Component '%s' is not registered." % \
            self.component.get_full_name()


class Interface(object):
    """
    Marker base class for extension point interfaces.
    
    Every extension point specifies the contract that extensions must conform to
    via an `Interface` subclass.
    """


class ExtensionPoint(property):
    """
    Declare an extension point on a component that other components can plug
    into.
    """
    
    def __init__(self, interface):
        """
        Create the extension point.
        
        @param interface: The `Interface` subclass that defines the protocol
        for the extension point.
        """
        
        property.__init__(self, self.extensions)
        
        self.interface = interface
        self.__doc__ = "List of component classes that implement `%s`" % \
            self.interface.__name__
    
    def extensions(self, component):
        """
        Return a list of component classes that declare to implement the
        extension point interface.
        
        The reason why not the actual loaded components are returned is so that
        clients can choose on their own whether to load an extending component
        or not. Loading a component can be done by passing the component class
        to the `load` method of the component manager.
        """
        
        return ComponentMeta.get_extensions(self.interface)
    
    def __repr__(self):
        """Return a textual representation of the extension point."""
        
        return "<ExtensionPoint %s>" % self.interface.__name__


class ComponentMeta(GObjectMeta):
    """
    Meta class for components.
    
    Takes care of component, extension point and dependency registration.
    """
    
    _components = []
    _registry = {}
    _component_dependencies = {}
    _component_dependents = {}
    
    def __new__(cls, name, bases, d):
        """Create the component class."""
        
        new_class = GObjectMeta.__new__(cls, name, bases, d)
        
        if name == "Component":
            # Don't put the `Component` base class in the registry
            return new_class
        
        if d.get("abstract"):
            # Don't put abstract component classes in the registry
            return new_class
        
        ComponentMeta._components.append(new_class)
        
        registry = ComponentMeta._registry
        
        for interface in d.get("_implements", []):
            registry.setdefault(interface, []).append(new_class)
        
        for base in bases:
            for interface in getattr(base, "_implements", []):
                registry.setdefault(interface, []).append(new_class)
        
        dependencies = ComponentMeta._component_dependencies
        dependents = ComponentMeta._component_dependents
        
        for dependency_cls in d.get("_depends_on", []):
            dependents.setdefault(dependency_cls, []).append(new_class)
            dependencies.setdefault(new_class, []).append(dependency_cls)
        
        return new_class
    
    @classmethod
    def get_components(cls):
        """Return a list of all component classes known to the system."""
        
        return cls._components[:]
    
    @classmethod
    def get_component_dependencies(cls, component_class):
        """
        Return a list of all component classes that this particular component
        class depends on.
        """
        
        return cls._component_dependencies.get(component_class, [])[:]
    
    @classmethod
    def get_component_dependents(cls, component_class):
        """
        Return a list of all components that depend on that particular
        component.
        """
        
        return cls._component_dependents.get(component_class, [])[:]
    
    @classmethod
    def get_extensions(cls, interface):
        """
        Return a list of component classes that declare to implement the
        extension point interface.
        """
        
        return cls._registry.get(interface, [])[:]


class Component(GObject):
    """
    Abstract base class for components.
    
    Every component can declare what extension points it provides, as well as
    what extension points of other components it extends.
    """
    
    __metaclass__ = ComponentMeta
    
    gsignal("updated")
    
    def __init__(self, component_manager):
        GObject.__init__(self)
        
        self._component_manager = component_manager
    
    def on_unload(self):
        """
        Can be overridden by subclasses in order to perform certain actions
        when the component is removed from the component manager.
        """
    
    @staticmethod
    def implements(*interfaces):
        """
        Can be used in the class definition of `Component` subclasses to declare
        the extension points that are extended.
        """
        
        frame = sys._getframe(1)
        locals_ = frame.f_locals
        
        # Some sanity checks
        assert locals_ is not frame.f_globals and "__module__" in locals_, \
            "implements() can only be used in a class definition"
        
        locals_.setdefault("_implements", []).extend(interfaces)
    
    @staticmethod
    def depends_on(*component_classes):
        """
        Can be used in the class definition of `Component` subclasses to declare
        the components that need to be loaded whenever this component is loaded.
        """
        
        frame = sys._getframe(1)
        locals_ = frame.f_locals
        
        # Some sanity checks
        assert locals_ is not frame.f_globals and "__module__" in locals_, \
            "depends_on() can only be used in a class definition"
        
        locals_.setdefault("_depends_on", []).extend(component_classes)
    
    @classmethod
    def get_full_name(cls):
        """Return both the module name and the component class name."""
        
        return cls.__module__ + "." + cls.__name__
    
    @classmethod
    def get_known_exceptions(cls):
        """The list of classes of exceptions that may be raised when loading the
        component and are to be handled by clients of the component.
        """
        
        return ()


implements = Component.implements
depends_on = Component.depends_on


class ComponentManager(GObject):
    """
    Manage component life cycle, instantiating registered components on demand.
    """
    
    # Emitted when an known and enabled component is being loaded.
    # The only additional argument is the component class.
    gsignal("component-loading", object)
    
    # Emitted when a known and enabled component cannot be loaded because of
    # an unhandled exception.
    # The first additional argument is the component class and the second one
    # is the unhandled exception.
    gsignal("component-loading-failure", object, object)
    
    # Emitted when a previously unloaded component has been successfully loaded.
    # The only additional argument is the newly created component.
    gsignal("component-loaded", object)
    
    # Emitted when a previously loaded component has been successfully unloaded.
    # The first additional argument is the component class.
    # Optionally, the second object can be an `Exception` if the component has
    # been unloaded because of a problem. A component can have been unloaded
    # by itself.
    gsignal("component-unloaded", object, object)
    
    def __init__(self):
        """Initialize the component manager."""
        
        GObject.__init__(self)
        
        self._components = {}
        
        # Makes sure that for a component manager, a component can only be
        # created once, even if the application uses threads.
        # The initialization methods of some components may take some time to
        # execute.
        self._locks = {}
    
    def __contains__(self, component_class):
        """
        Return whether the given class is in the list of active components.
        """
        
        return component_class in self._components
    
    def get(self, component_class):
        if component_class not in ComponentMeta.get_components():
            raise ComponentNotRegisteredError(component_class)
        
        with self._locks.setdefault(component_class, Lock()):
            return self._components.get(component_class)
    
    def load(self, component_class):
        """
        Activate the component instance for the given class, or return the
        existing instance if the component has already been loaded.
        """
        
        if not self.is_component_enabled(component_class):
            return
        
        if component_class not in ComponentMeta.get_components():
            raise ComponentNotRegisteredError(component_class)
        
        self.emit("component-loading", component_class)
        
        component_already_loaded = False
        
        try:
            with self._locks.setdefault(component_class, Lock()):
                component = self._components.get(component_class)
                
                if component:
                    component_already_loaded = True
                else:
                    # Load all components that this component depends on.
                    for cls in ComponentMeta.get_component_dependencies(
                        component_class):
                        if not self.is_loading(cls):
                            self.load(cls)
                    
                    component = component_class(self)
                    self._components[component_class] = component
                    
                    LOG.debug("Component '%s' loaded.",
                        component.get_full_name())
        except Exception as error:
            # Emit signals after releasing the component lock, just in case.
            # NOTE: If an exception is raised in one of the handlers of the
            # 'component-loading-failure' signals, `error' will for some reason
            # be displayed twice in the log, while the actual error raised
            # in the signal handler will be invisible.
            # One solution for clients is to connect to the component manager
            # using `connect_async'.
            self.emit("component-loading-failure", component_class, error)
            
            raise
        
        if not component_already_loaded:
            self.emit("component-loaded", component)
        
        return component
    
    def load_async(self, component_class):
        """Load a component in a separate thread."""
        
        thread = ComponentLoadingThread(self, component_class)
        thread.start()
    
    def is_loading(self, component_class):
        """
        Is a certain component being loaded (based on a component class)?
        """
        
        try:
            return self._locks[component_class].locked()
        except KeyError:
            return False
    
    def unload(self, component_class, error=None):
        """
        Force a component to be unloaded.
        
        The argument `component_class` can be a `Component` subclass or an
        instance of it.
        
        The optional `error` argument can be an `Exception`. It can be passed if
        the component has been unloaded because of a problem.
        """
        
        if not isinstance(component_class, type):
            component_class = component_class.__class__
        
        already_unloaded = False
        
        with self._locks.setdefault(component_class, Lock()):
            component = self._components.pop(component_class, None)
            
            if component is not None:
                # Unload all components that depend on this component.
                for cls in ComponentMeta.get_component_dependents(
                    component_class):
                    self.unload(cls)
                
                component.on_unload()
                LOG.debug("Component '%s' unloaded.", component.get_full_name())
            else:
                already_unloaded = True
        
        if not already_unloaded:
            self.emit("component-unloaded", component_class, error)
    
    def unload_all(self):
        """Unload all components in no particular order."""
        
        for component in dict(self._components):
            self.unload(component)
    
    def is_component_enabled(self, component_class):
        """
        Can be overridden by sub-classes to veto the activation of a component.

        If this method returns False, the component with the given class will
        not be available.
        """
        
        return True


class ComponentLoadingThread(Thread):
    """Asynchronously loads a component.
    
    What is special about it is that it catches exceptions of those types listed
    in the tuple returned by the `get_known_exceptions' method of the component.
    Instead of letting the default exception hook process the exception,
    it only logs the exception as an error.
    """
    
    def __init__(self, component_manager, component_class):
        self._component_manager = component_manager
        self._component_class = component_class
        
        Thread.__init__(self)
    
    def run(self):
        try:
            self._component_manager.load(self._component_class)
        except self._component_class.get_known_exceptions() as error:
            LOG.error(error)
