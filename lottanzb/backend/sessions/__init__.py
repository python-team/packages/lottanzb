# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.config.section import ConfigSection
from lottanzb.util.gobject_extras import gproperty
from lottanzb.backend.sessions.base import ISession
from lottanzb.backend.sessions.local import LocalSession
from lottanzb.backend.sessions.remote import RemoteSession

__all__ = ["ISession", "LocalSession", "RemoteSession"]

class Config(ConfigSection):
    """Represents the configuration section 'backend.sessions'."""
    
    # The string representation of the currently active session, as defined
    # by each Session's `get_name` class method. 
    active = gproperty(type=str)
    
    def set_property(self, key, value):
        """Ensures that the 'active' option never holds an invalid value."""
        
        if key == "active" and value:
            # Makes it possible to directly assign a class implementing
            # `ISession` or an instance of it to the `active` option.
            try:
                value = value.get_name()
            except AttributeError:
                pass
        
        ConfigSection.set_property(self, key, value)
