# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.util.gobject_extras import gproperty
from lottanzb.core.constants import DefaultPortNumber
from lottanzb.config.section import ConfigSection
from lottanzb.backend.sessions.base import BaseSession
from lottanzb.backend.interface import ConnectionInformation

__all___ = ["RemoteSession"]

class Config(ConfigSection):
    host = gproperty(type=str)
    port = gproperty(type=int, default=DefaultPortNumber.HTTP, minimum=1,
        maximum=2 ** 16 - 1)
    username = gproperty(type=str)
    password = gproperty(type=str)
    api_key = gproperty(type=str)
    https = gproperty(type=bool, default=False)


class RemoteSession(BaseSession):
    abstract = False
    
    def get_connection_info(self):
        info = ConnectionInformation()
        
        for key in ("host", "port", "username", "password", "api_key", "https"):
            info.set_property(key, self.config[key])
        
        # TODO: The method might also be called by other clients rather than
        # the `Interface'. For all other clients, it's important not to mess
        # with the connection information values unless explicitly desired.
        info.connect("notify", self.on_connection_info_changed)
        
        return info
    
    def on_connection_info_changed(self, info, *args):
        """
        The connection information may be altered after the session has been
        started, mostly probably using the preferences UI.
        
        This method makes sure that these changes are written back to the
        configuration.
        """
        
        self.config.update({
            "port": info.port,
            "username": info.username,
            "password": info.password,
            "api_key": info.api_key
        })
