# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.config.lotta import ConfigurableComponent
from lottanzb.core.component import implements, Interface
from lottanzb.backend.interface import Interface as BackendInterface

__all__ = ["ISession", "BaseSession"]

class ISession(Interface):
    def get_interface(self):
        """The `backend.interface.Interface' to be used by the backend."""
        pass
    
    @classmethod
    def get_name(cls):
        pass


class BaseSession(ConfigurableComponent):
    implements(ISession)
    
    abstract = True
    
    def get_connection_info(self):
        raise NotImplementedError
    
    def get_interface(self):
        """The `backend.interface.Interface' to be used by the backend."""
        return BackendInterface(self.get_connection_info())
    
    @classmethod
    def get_name(cls):
        return cls.__module__.split(".")[-1]
