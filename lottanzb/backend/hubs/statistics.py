# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.core.component import Component, implements, depends_on
from lottanzb.util.gobject_extras import gproperty
from lottanzb.backend import Backend
from lottanzb.backend.hubs import IHub, DataSize
from lottanzb.backend.hubs.polling import PollingHub
from lottanzb.backend.interface.queries import QueueQuery, HistoryQuery

__all__ = ["StatisticsHub"]

class StatisticsHub(Component):
    implements(IHub)
    depends_on(Backend)
    depends_on(PollingHub)
    
    # These statistical values are only available using SABnzbd 0.6 or newer.
    # Otherwise, they default to 0 Bytes.
    history_total_size = gproperty(type=object)
    history_month_size = gproperty(type=object)
    history_week_size = gproperty(type=object)
    
    total_download_folder_space = gproperty(type=object)
    total_temp_folder_space = gproperty(type=object)
    free_download_folder_space = gproperty(type=object)
    free_temp_folder_space = gproperty(type=object)
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        self.history_total_size = DataSize()
        self.history_month_size = DataSize()
        self.history_week_size = DataSize()
        
        polling_hub = self._component_manager.load(PollingHub)
        polling_hub.schedule(15 * 1000, QueueQuery, self.on_queue_query)
        polling_hub.schedule(60 * 1000, HistoryQuery, self.on_history_query)
    
    def on_queue_query(self, query):
        response = query.response
        
        self.total_download_folder_space = DataSize(
            gibibytes=float(response["diskspacetotal2"]))
        self.total_temp_folder_space = DataSize(
            gibibytes=float(response["diskspacetotal1"]))
        self.free_download_folder_space = DataSize(
            gibibytes=float(response["diskspace2"]))
        self.free_temp_folder_space = DataSize(
            gibibytes=float(response["diskspace1"]))
    
    def on_history_query(self, query):
        # These statistical values are only available using SABnzbd 0.6 or
        # newer. Otherwise, they default to 0 Bytes.
        for time_frame in ("total", "month", "week"):
            response_key = "%s_size" % time_frame
            
            if response_key in query.response:
                attribute = "history_%s_size" % time_frame
                value = DataSize.from_string(query.response[response_key])
                
                setattr(self, attribute, value)
