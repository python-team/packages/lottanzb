# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.core.component import Component, implements, depends_on
from lottanzb.util.timer import Timer
from lottanzb.backend import Backend
from lottanzb.backend.hubs import IHub

class PollingHub(Component):
    implements(IHub)
    depends_on(Backend)
    
    # The minimal amount of time in milliseconds between two polling queries.
    # Will be used when calling `schedule_fast'.
    MIN_POLLING_INTERVAL = 1000
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        self._timers = {}
        self._interface = self._component_manager.load(Backend).interface
    
    def schedule(self, interval, query_class, handler, run_now=True):
        if interval < self.MIN_POLLING_INTERVAL:
            interval = self.MIN_POLLING_INTERVAL
        
        if query_class in self._timers:
            timer = self._timers[query_class]
            
            if interval < timer.timeout:
                timer.timeout = interval
        else:
            timer = Timer(interval)
            timer.connect("tick", self._on_tick, query_class)
            timer.start()
            
            self._on_tick(timer, query_class)
            
            self._timers[query_class] = timer
        
        def internal_handler(interface, query):
            handler(query)
        
        self._interface.connect_filtered(query_class, "query-success",
            internal_handler)
    
    def schedule_hourly(self, query_class, handler, run_now=True):
        self.schedule(1000 * 60 * 60, query_class, handler, run_now)
    
    def schedule_minutely(self, query_class, handler, run_now=True):
        self.schedule(1000 * 60, query_class, handler, run_now)
    
    def schedule_fast(self, query_class, handler, run_now=True):
        self.schedule(self.MIN_POLLING_INTERVAL, query_class, handler, run_now)
    
    def on_unload(self):
        for timer in self._timers.values():
            timer.stop()
    
    def _on_tick(self, timer, query_class):
        self._interface.run_query(query_class())
