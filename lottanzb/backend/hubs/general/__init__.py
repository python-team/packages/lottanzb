# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re

from threading import Thread, current_thread
from os.path import basename

from lottanzb.core.component import Component, implements, depends_on
from lottanzb.util.gobject_extras import gproperty
from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.backend import Backend
from lottanzb.backend.interface import QueryCache, Interface
from lottanzb.backend.interface.queries import *
from lottanzb.backend.hubs import IHub, DataSize, DataSpeed, TimeDelta, DateTime
from lottanzb.backend.hubs.polling import PollingHub
from lottanzb.backend.hubs.general.download import Download, Status, StatusGroup
from lottanzb.backend.hubs.general.download_list import DownloadListStore
from lottanzb.backend.hubs.general.updater import (DownloadListUpdater,
    DownloadListQueueUpdater)

__all__ = ["GeneralHub"]

class GeneralHub(Component):
    implements(IHub)
    depends_on(Backend)
    depends_on(PollingHub)
    
    paused = gproperty(type=bool, default=False)
    speed = gproperty(type=object)
    time_left = gproperty(type=object)
    size_left = gproperty(type=object)
    eta = gproperty(type=object)
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        with gtk_lock:
            self.downloads = DownloadListStore()
        
        self.time_left = TimeDelta()
        self.size_left = DataSize()
        self.speed = DataSpeed()
        
        # Cache the polling queries so that `downloads' is only updated if
        # there are new values.
        # In the future, we could add a more sophisticated caching mechanism
        # that also works on a per-download basis.
        self.queue_cache = QueryCache()
        self.history_cache = QueryCache()
        
        # The thread that is currently executing `on_queue_query', which
        # updates the `paused' property.
        # None if `on_queue_thread' is currently not being executed.
        self._thread_updating_queue = None
        
        self.interface = self._component_manager.load(Backend).interface
        
        self._queue_updater = DownloadListQueueUpdater(self.downloads,
            StatusGroup.INCOMPLETE)
        self._history_updater = DownloadListUpdater(self.downloads,
            StatusGroup.COMPLETE | StatusGroup.PROCESSING)
        
        polling_hub = self._component_manager.load(PollingHub)
        polling_hub.schedule_fast(QueueQuery, self.on_queue_query)
        
        # TODO: 15 seconds is too long for status reports during
        # post-processing. However, if no post-processing is taking place and
        # the history is large, frequent polling would be grossly inefficient.
        # Thus, polling needs to be much more fine-grained. For example,
        # it's possible to provide SABnzbd with a 'start' and 'limit' parameter
        # and to get the status of an individual download.
        polling_hub.schedule(15 * 1000, HistoryQuery, self.on_history_query)
        
        event_map = {
            DeleteDownloadsQuery: self.on_delete_downloads_query,
            DeleteHistoryQuery: self.on_delete_downloads_query,
            DeleteAllHistoryQuery: self.on_delete_all_history_query,
            RenameDownloadQuery: self.on_rename_query,
            PauseDownloadsQuery: self.on_pause_downloads_query,
            ResumeDownloadsQuery: self.on_resume_downloads_query,
            PriorityQuery: self.on_priority_query,
            SwitchDownloadQuery: self.on_switch_download_query
        }
        
        for query_cls, handler in event_map.items():
            self.interface.connect_query_started(query_cls, handler)
    
    def on_queue_query(self, query):
        # Avoid unnecessary work.
        if not self.queue_cache.has_changed(query):
            return
        
        self._thread_updating_queue = current_thread()
        
        response = query.response
        
        self.paused = response["paused"]
        self.size_left = DataSize(mebibytes=float(response["mbleft"]))
        
        slots = response.get("slots", [])[:]
        self._queue_updater.apply_slots(slots, self.paused)
        
        active_filter = self.downloads.get_filter_by_status(
            Status.DOWNLOADING | Status.DOWNLOADING_RECOVERY_DATA)
        
        # It looks like SABnzbd somehow smoothes the values of 'speed'
        # and 'time_left', which results in stranges values after having
        # paused SABnzbd or after the last download is finished.
        if len(active_filter) and not self.paused:
            self.speed = DataSpeed(kibibytes=float(response["kbpersec"]))
            self.time_left = TimeDelta.from_string(response["timeleft"])
        else:
            self.speed = DataSpeed()
            self.time_left = None
        
        self._thread_updating_queue = None
        
        self.emit("updated")
    
    def on_history_query(self, query):
        # Avoid unnecessary work.
        if not self.history_cache.has_changed(query):
            return
        
        slots = query.response["slots"][:]
        slots.reverse()
        
        self._history_updater.apply_slots(slots)
        
        self.emit("updated")
    
    def on_delete_downloads_query(self, interface, query):
        with gtk_lock:
            for row in self.downloads:
                download = row[DownloadListStore.COLUMN]
                
                if download.id in query.download_ids:
                    self.downloads.remove(row.iter)
        
        self.emit("updated")
    
    def on_delete_all_history_query(self, interface, query):
        with gtk_lock:
            for row in self.downloads:
                download = row[DownloadListStore.COLUMN]
                
                if download.has_status(StatusGroup.COMPLETE):
                    self.downloads.remove(row.iter)
        
        self.emit("updated")
    
    def on_rename_query(self, interface, query):
        with gtk_lock:
            if query.download_id in self.downloads._by_id:
                download = self.downloads._by_id[query.download_id]
                download.name = query.new_name
                
                self.downloads.download_changed(download)
        
        self.emit("updated")
    
    def on_pause_downloads_query(self, interface, query):
        """Properly update the status of each download affected by the `query'
        of type `PauseDownloadsQuery'.
        """
        
        self._apply_status_to_downloads(query.download_ids, Status.PAUSED)
    
    def on_resume_downloads_query(self, interface, query):
        """Properly update the status of each download affected by the `query'
        of type `ResumeDownloadsQuery'.
        """
        
        self._apply_status_to_downloads(query.download_ids, Status.QUEUED)
    
    def _apply_status_to_downloads(self, download_ids, status):
        """Properly set the status of each download with an ID in
        `download_ids' to `status'.
        """
        
        with gtk_lock:
            queue = self.downloads.get_filter_not_fully_loaded()
            
            for row in queue:
                download = row[DownloadListStore.COLUMN]
                child_iter = queue.convert_iter_to_child_iter(row.iter)
                child_path = queue.convert_path_to_child_path(row.path)
               
                if download.id in download_ids:
                    download.status = status
                    self.downloads.row_changed(child_path, child_iter)
        
        self._update_active_download()
    
    def _update_active_download(self):
        """Unless the application is paused, choose the first queued (but not
        paused) download in the queue to be the new (and only) active download.
        """
        
        if self.paused:
            return
        
        with gtk_lock:
            queue = self.downloads.get_filter_not_fully_loaded()
            has_active_download = False
            
            for row in queue:
                download = row[DownloadListStore.COLUMN]
                child_iter = queue.convert_iter_to_child_iter(row.iter)
                child_path = queue.convert_path_to_child_path(row.path)
                
                if has_active_download:
                    if download.status == Status.DOWNLOADING:
                        download.status = Status.QUEUED
                        self.downloads.row_changed(child_path, child_iter)
                else:
                    if download.status == Status.QUEUED:
                        download.status = Status.DOWNLOADING
                        self.downloads.row_changed(child_path, child_iter)
                
                if download.status == Status.DOWNLOADING:
                    has_active_download = True
    
    def on_priority_query(self, interface, query):
        with gtk_lock:
            for download_id in query.download_ids:
                download = self.downloads._by_id[download_id]
                download.priority = query.priority
                
                self.downloads.download_changed(download)
        
        self.emit("updated")
    
    def on_switch_download_query(self, interface, query):
        if query.download_id == query.other_download_id:
            return
        
        source_iter = None
        target_iter = None
        
        with gtk_lock:
            for row in self.downloads:
                download = row[DownloadListStore.COLUMN]
                
                if download.id == query.download_id:
                    source_iter = row.iter
                    source_position = row.path[0]
                elif download.id == query.other_download_id:
                    target_iter = row.iter
                    target_position = row.path[0]
                
                if source_iter is not None and target_iter is not None:
                    break
            
            if target_position < source_position:
                self.downloads.move_before(source_iter, target_iter)
            else:
                self.downloads.move_after(source_iter, target_iter)
        
        self._update_active_download()
    
    def set_property(self, key, value):
        changed = Component.set_property(self, key, value)
        
        # Only start a `PauseQuery' or `ResumeQuery' if the change of the
        # `paused' property actually originates from the LottaNZB application
        # and is not due to a `QueueQuery' response.
        if key == "paused" and changed and \
            self._thread_updating_queue is not current_thread():
            if value:
                query = PauseQuery()
            else:
                query = ResumeQuery()
            
            self.interface.run_query(query)
            
            if value:
                self.speed = DataSpeed()
                self.time_left = None
                self.eta = None
                
                with gtk_lock:
                    for row in self.downloads:
                        download = row[DownloadListStore.COLUMN]
                        
                        if download.has_status(Status.DOWNLOADING):
                            download.status = Status.QUEUED
                            self.downloads.row_changed(row.path, row.iter)
            
            self.emit("updated")
        if key == "time_left":
            if isinstance(value, TimeDelta):
                self.eta = DateTime.from_datetime(DateTime.now() + value)
            else:
                self.eta = None
        
        return changed
    
    def move_download(self, download, index):
        """Move `download' to a specific `index' in the queue.
        
        The operation will only succeed if the download can be moved and if the
        index is valid.
        """
        
        if not download.has_status(StatusGroup.MOVABLE):
            raise ValueError(
                "Download '{0}' with status '{1}' cannot be moved.".format(
                download.id, Status.to_pretty_string(download.status)))
        
        with gtk_lock:
            queued_filter = self.downloads.get_filter_not_fully_loaded()
            target_download = queued_filter[index][DownloadListStore.COLUMN]
        
        if download is target_download:
            # The download is already at the correct index.
            return
        
        query = SwitchDownloadQuery(download.id, target_download.id)
        
        self.interface.run_query(query)
    
    def move_download_relative(self, download, shift):
        """Move `download' to an index relative to the current one as specified
        by `shift'.
        
        The download will be moved to the index in the queue that is computed by
        adding `shift' to its current index.
        
        Thus, a positive value of `shift' causes the download to be moved
        down the queue while a negative value of `shift' causes the download to
        be moved to a higher place in the queue.
        
        The operation will only succeed if the download can be moved and if the
        target index is valid.
        """
        
        if not shift:
            # The download is already at the correct index.
            return
        
        with gtk_lock:
            queued_downloads = self.downloads.get_filter_not_fully_loaded()
            
            index = 0
            
            for row in queued_downloads:
                if row[DownloadListStore.COLUMN] is download:
                    index = row.path[0]
                    break
        
        self.move_download(download, index + shift)
    
    def move_download_up(self, download, shift=1):
        """Move `download' to a higher place in the queue.
        
        By default, the download changes exchanges its place with the
        download above it. In general, the `shift' argument can be used to
        specify the number of downloads above `download' that should
        be below it after the operation has taken place.
        
        The operation will only succeed if the download can be moved and if the
        target index is valid.
        
        A `ValueError' is raised if `shift' is smaller than 1.
        """
        
        if shift < 1:
            raise ValueError("The argument 'shift' of 'move_download_up' must "
                "be greater than zero, not %r." % shift)
        
        self.move_download_relative(download, -shift)
    
    def force_download(self, download):
        """Move `download' to the top of the queue and resume it if necessary,
        causing it to become active immediately.
        
        The operation will only succeed if the download can be moved.
        """
        
        self.move_download(download, 0)
        
        if download.status == Status.PAUSED:
            self.resume_downloads(download)
    
    def move_download_down(self, download, shift=1):
        """Move `download' to a lower place in the queue.
        
        By default, the download changes exchanges its place with the
        download below it. In general, the `shift' argument can be used to
        specify the number of number of downloads below `download' that should
        be above it after the operation has taken place.
        
        The operation will only succeed if the download can be moved and if the
        target index is valid.
        
        A `ValueError' is raised if `shift' is smaller than 1.
        """
        
        if shift < 1:
            raise ValueError("The argument 'shift' of 'move_download_down' "
                "must be greater than zero, not %r." % shift)
        
        self.move_download_relative(download, shift)
    
    def move_download_down_to_bottom(self, download):
        """Move `download' to the bottom of the queue.
        
        The operation will only succeed if the download can be moved.
        """
        
        with gtk_lock:
            queued_downloads = self.downloads.get_filter_not_fully_loaded()
        
        self.move_download(download, len(queued_downloads) - 1)
    
    def delete_all_downloads(self):
        return self.interface.run_query(DeleteAllDownloadsQuery())
    
    def delete_downloads(self, *downloads):
        ids = [download.id for download in downloads]
        return self.interface.run_query(DeleteDownloadsQuery(*ids))
    
    def pause_downloads(self, *downloads):
        ids = [download.id for download in downloads]
        return self.interface.run_query(PauseDownloadsQuery(*ids))
    
    def resume_downloads(self, *downloads):
        ids = [download.id for download in downloads]
        return self.interface.run_query(ResumeDownloadsQuery(*ids))
    
    def set_priority(self, priority, *downloads):
        ids = [download.id for download in downloads]
        return self.interface.run_query(PriorityQuery(priority, *ids))
        
    def rename_download(self, download, new_name):
        return self.interface.run_query(
            RenameDownloadQuery(download.id, new_name))
    
    def add_file(self, file_name, post_processor=None, script=None,
        category=None, priority=None, name=None):
        # If no pretty `name' has been specified, use the built-in method to
        # clean it.
        if name is None:
            name = Download.get_clean_name(basename(file_name))
        
        if self.interface.connection_info.is_local:
            query_class = AddLocalFileQuery
        else:
            query_class = AddRemoteFileQuery
        
        return self.interface.run_query(query_class(file_name, post_processor,
            script, category, priority, name))
    
    def add_url(self, url, post_processor=None, script=None, category=None,
        priority=None):
        query = AddURLQuery(url, post_processor, script, category, priority)
        return self.interface.run_query(query)
    
    def add_id(self, id, post_processor=None, script=None, category=None,
        priority=None):
        query = AddURLQuery(id, post_processor, script, category, priority)
        return self.interface.run_query(query)
    
    def pause(self):
        self.paused = True
    
    def resume(self):
        self.paused = False
    
    def shutdown(self):
        return self.interface.run_query(ShutdownQuery())
    
    def restart(self):
        return self.interface.run_query(RestartQuery())
    
    def delete_all_history(self):
        return self.interface.run_query(DeleteAllHistoryQuery())
    
    def delete_history(self, *downloads):
        ids = [download.id for download in downloads]
        return self.interface.run_query(DeleteHistoryQuery(*ids))
