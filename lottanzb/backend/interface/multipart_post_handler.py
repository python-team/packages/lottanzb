# Copyright (C) 2006 Will Holcomb <wholcomb@gmail.com>
# Copyright (C) 2007 Brian Schneide
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

"""Enables the use of multipart/form-data for posting forms

Inspirations:
  Upload files in python:
    http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/146306
  urllib2_file:
    Fabien Seisen: <fabien@seisen.org>

Example:
>>> from multi_partpost_handler import MultipartPostHandler
>>> from urllib2 import build_opener
>>> opener = build_opener(MultipartPostHandler)
>>> params = { "file" : open("path/to/file", "rb") }
>>> opener.open("http://wwww.example.com/", params)
"""

import mimetools
import mimetypes

from urllib import urlencode
from urllib2 import BaseHandler, HTTPHandler
from cStringIO import StringIO

__all__ = ("MultipartPostHandler")

class MultipartPostHandler(BaseHandler):
    handler_order = HTTPHandler.handler_order - 10 # Needs to run first
    
    def http_request(self, request):
        data = request.get_data()
        
        if data is not None and not isinstance(data, str):
            files = []
            variables = []
            
            for key, value in data.items():
                if isinstance(value, file):
                    files.append((key, value))
                else:
                    variables.append((key, value))
            
            if not files:
                data = urlencode(variables, 1)
            else:
                boundary, data = self.multipart_encode(variables, files)
                content_type = "multipart/form-data; boundary=%s" % boundary
                
                request.add_unredirected_header("Content-Type", content_type)
            
            request.add_data(data)
        
        return request
    
    @staticmethod
    def multipart_encode(variables, files, boundary=None, buf=None):
        if boundary is None:
            boundary = mimetools.choose_boundary()
        
        if buf is None:
            buf = StringIO()
        
        for key, value in variables:
            buf.write("--%s\r\n" % boundary)
            buf.write("Content-Disposition: form-data; name=\"%s\"" % key)
            buf.write("\r\n\r\n" + value + "\r\n")
        
        for key, fd in files:
            # Currently not used:
            # file_size = os.fstat(fd.fileno())[stat.ST_SIZE]
            
            filename = fd.name.split("/")[-1]
            content_type = mimetypes.guess_type(filename)[0] or "application/octet-stream"
            fd.seek(0)
            
            buf.write("--%s\r\n" % boundary)
            buf.write("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n" % (key, filename))
            buf.write("Content-Type: %s\r\n" % content_type)
            buf.write("\r\n" + fd.read() + "\r\n")
        
        buf.write("--" + boundary + "--\r\n\r\n")
        buf = buf.getvalue()
        
        return boundary, buf
    
    https_request = http_request
