# Copyright (C) 2007-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from logging import getLogger
LOG = getLogger(__name__)

import errno
import socket

from urllib2 import build_opener, Request, URLError, HTTPError
from json import loads as read_json
from time import sleep, time
from distutils.version import LooseVersion

from lottanzb import __version__
from lottanzb.core.environ import _
from lottanzb.util.threading_extras import Thread
from lottanzb.backend.interface.errors import (
    QueryError,
    QueryConnectionError,
    QueryUnknownHostError,
    QueryConnectionRefusedError,
    QueryHTTPError,
    QueryResponseError,
    QueryAPIKeyError,
    QueryUsernamePasswordError,
    QueryConnectionTimeout,
    QueryRestartTimeout,
    QueryArgumentsError,
    QueryNotImplementedError
)
from lottanzb.backend.interface.multipart_post_handler import (
    MultipartPostHandler
)

__all__ = [
    "Query",
    "SimpleSetConfigQuery",
    "ComplexSetConfigQuery",
    "GetConfigQuery",
    "DeleteConfigQuery",
    "StatusQuery",
    "CapabilitiesQuery",
    "QueueQuery",
    "SortQueueQuery",
    "DeleteDownloadsQuery",
    "DeleteAllDownloadsQuery",
    "DeleteFileQuery",
    "RenameDownloadQuery",
    "ChangeCompleteActionQuery",
    "PurgeQuery",
    "PauseDownloadsQuery",
    "ResumeDownloadsQuery",
    "PriorityQuery",
    "AddFileQuery",
    "AddRemoteFileQuery",
    "AddLocalFileQuery",
    "RetryQuery",
    "SwitchDownloadQuery",
    "ChangeCategoryQuery",
    "ChangeScriptQuery",
    "ChangePostProcessingQuery",
    "HistoryQuery",
    "AbstractDeleteHistoryQuery",
    "DeleteHistoryQuery",
    "DeleteAllHistoryQuery",
    "FilesQuery",
    "AddURLQuery",
    "AddIDQuery",
    "PauseQuery",
    "ResumeQuery",
    "ExclusiveQuery",
    "ShutdownQuery",
    "RestartQuery",
    "RestartRepairQuery",
    "DisconnectQuery",
    "RescanQuery",
    "WarningsQuery",
    "ClearWarningsQuery",
    "CategoriesQuery",
    "ScriptsQuery",
    "VersionQuery",
    "GetSpeedLimitQuery",
    "SetSpeedLimitQuery",
    "GetNewzbinBookmarksQuery",
    "AuthenticationQuery",
    "AuthenticationType",
    "AuthenticationTypeQuery"
]

class Query(Thread):
    OUTPUT_FORMAT = "json"
    
    # A shortcut for subclasses which sets the "mode" arguments so that they
    # don't need to override the constructor.
    METHOD = ""
    
    ARGUMENT_ERRORS = {
        "expect integer value": _("Wrong number of arguments."),
        "expect two parameters": _("Wrong number of arguments."),
        "expect integer value": _("No integer value."),
        "no file given": _("No regular file."),
        "file does not exist": _("File does not exist."),
        "Format not supported": _("Format not supported."),
        "Config item does not exist": _("Configuration option does not exist."),
        "Incorrect server settings": _("Invalid server settings.")
    }
    
    # The default amount of time after a query has been started until LottaNZB
    # gives up and raises a `QueryConnectionTimeout'.
    DEFAULT_TIMEOUT = 10
    
    def __init__(self, **arguments):
        Thread.__init__(self)
        
        self.connection_info = None
        self.arguments = arguments
        self.response = {}
        self.raw_response = ""
        self.url = ""
        self.timeout = self.DEFAULT_TIMEOUT
        
        arguments["output"] = self.OUTPUT_FORMAT
        
        if not "mode" in self.arguments and self.METHOD:
            self.arguments["mode"] = self.METHOD
        
        for key, value in self.arguments.items():
            # Convert all arguments of type 'bool' to 'int'.
            if isinstance(value, bool):
                self.arguments[key] = int(value)
            
            # Remove all arguments with the value `None` as they will be encoded
            # as 'None', which I cannot imagine to be desired in any case.
            if value is None:
                del self.arguments[key]
    
    @property
    def required_version(self):
        pass
    
    def affects_query(self, query):
        """Whether this query has an effect on the response of `query'.
        
        Before this query is started, any queries that are being executed and
        are affected by this query need to be completed. After this query is
        started, no query that is affected by this query must be started until
        this query is complete.
        
        By default, a query doesn't affect any other query.
        """
        
        return False
    
    def start(self, connection_info):
        self.connection_info = connection_info
        
        if self.connection_info.needs_authentication:
            self.arguments.update({
                "ma_username": self.connection_info.username,
                "ma_password": self.connection_info.password
            })
        
        if self.connection_info.api_key:
            self.arguments["apikey"] = self.connection_info.api_key
        
        Thread.start(self)
    
    def run(self):
        try:
            self._run()
        except QueryError as error:
            # `QueryError's should not cause a traceback to be displayed in the
            # log as they are known to occur and will be handled appropriately
            # by clients of the class.
            self.error = error
        except Exception as error:
            # Exceptions of other types however reveal a bug in the code that
            # needs to be fixed.
            self.error = error
            raise
        finally:
            self.emit("completed")
    
    def _run(self):
        self.url = self.connection_info.api_url
        
        try:
            request = Request(self.url)
            
            # The user agent defaults to 'Python-urllib/2.6' (on Python 2.6).
            # Because the user agent also appears in SABnzbd log messages,
            # it's changed to something more specific.
            request.add_header("User-agent", "LottaNZB/%s" % __version__)
            
            opener = build_opener(MultipartPostHandler)
            open_file = opener.open(request, self.arguments, self.timeout)
            raw_response = open_file.read()
            opener.close()
        except HTTPError as error:
            raise QueryHTTPError(self, error.code)
        except URLError as error:
            if error.args:
                embedded_error = error.args[0]
                
                if isinstance(embedded_error, socket.timeout):
                    raise QueryConnectionTimeout(self)
                elif isinstance(embedded_error, (socket.error,
                    socket.gaierror)):
                    error_number = embedded_error[0]
                    
                    if error_number == errno.ECONNREFUSED:
                        raise QueryConnectionRefusedError(self)
                    elif error_number == socket.EAI_NONAME:
                        raise QueryUnknownHostError(self)
            
            raise QueryConnectionError(self, error.reason)
        else:
            self.raw_response = raw_response
            
            try:
                self.response = self._parse_json_response(
                    read_json(self.raw_response))
            except ValueError:
                raise QueryResponseError(self)
    
    def _parse_json_response(self, response):
        status = response.pop("status", True)
        error = response.pop("error", "").lower()
        
        if status:
            if len(response) == 1:
                response = response.popitem()[1]
        else:
            if error == "missing authentication":
                raise QueryUsernamePasswordError(self)
            elif error in ("api key required", "api key incorrect"):
                raise QueryAPIKeyError(self)
            elif error == "not implemented":
                raise QueryNotImplementedError(self)
            else:
                # No `QueryResponseError' should be raised if the error is known
                # to have been caused by invalid arguments, but not by a 
                # connection issue or a problem within SABnzbd.
                for known_error, message in self.ARGUMENT_ERRORS.items():
                    if known_error == error:
                        raise QueryArgumentsError(self, message)
                
                raise QueryResponseError(self, error)
        
        # It's easier to check if the query was successful or not if a boolean
        # variable is returned instead of an empty dict.
        if response == {}:
            return status
        else:
            return response


class AbstractSetConfigQuery(Query):
    METHOD = "set_config"
    
    def __init__(self, section, key, **kwargs):
        self.section = section
        self.key = key
        
        Query.__init__(self, section=section, keyword=key, **kwargs)
    
    def affects_query(self, query):
        # Make the query exclusive if we're changing the connection
        # information. This prevents other queries from accidentally
        # triggering a "Invalid username or password" error.
        if self.changes_connection_info_option():
            return True
        
        if isinstance(query, GetConfigQuery):
            return True
        
        if isinstance(query, AbstractSetConfigQuery) and \
            self.section == query.section and \
            self.key == query.key:
            return True
        
        return False
    
    def changes_connection_info_option(self, immediate_only=True):
        """Whether the configuration information changed using this query is
        required to connect to a running instance of SABnzbd.
        
        By default, the "port" option will not be detected as changes to it
        are not applied immediately, but only after a restart. This can be
        overridden by setting the `immediate_only` parameter to False.
        """
        
        keys = ["username", "password", "api_key"]
        
        if not immediate_only:
            keys.append("port")
        
        return self.section == "misc" and self.key in keys


class SimpleSetConfigQuery(AbstractSetConfigQuery):
    def __init__(self, section, key, value):
        self.value = value
        
        AbstractSetConfigQuery.__init__(self, section, key, value=value)


class ComplexSetConfigQuery(AbstractSetConfigQuery):
    def __init__(self, section, key, values):
        self.values = values
        
        AbstractSetConfigQuery.__init__(self, section, key, **values)


class GetConfigQuery(Query):
    METHOD = "get_config"
    
    def __init__(self, section="", keyword=""):
        Query.__init__(self, section=section, keyword=keyword)


class DeleteConfigQuery(Query):
    METHOD = "del_config"
    
    def __init__(self, section="", keyword=""):
        Query.__init__(self, section=section, keyword=keyword)
    
    def affects_query(self, query):
        return isinstance(query, GetConfigQuery)


class StatusQuery(Query):
    METHOD = "qstatus"


class CapabilitiesQuery(Query):
    """
    Get a dictionary containing information about the availability of certain
    software components used to perform and post-process downloads.
    
    "yenc" is True if the Python yenc module is available that makes decoding
    downloads faster.
    
    "ssl" is True if the OpenSSL bindings for Python are available.
    
    The keys "par2", "par2c", "rar", "zip", "nice", "ionice" contain paths to
    the corresponding program while empty string indicate that the program is
    not available.
    """
    
    METHOD = "options"


class AbstractQueueQuery(Query):
    METHOD = "queue"


class QueueQuery(AbstractQueueQuery):
    def __init__(self, start=None, limit=None, history=True):
        AbstractQueueQuery.__init__(self, start=start, limit=limit,
            history=history)
    
    def affects_query(self, query):
        return isinstance(query, QueueQuery)


class AbstractQueueModificationQuery(AbstractQueueQuery):
    def affects_query(self, query):
        return isinstance(query, QueueQuery)


class SortQueueQuery(AbstractQueueModificationQuery):
    class Field:
        INDEX = "index"
        NAME = "name"
        SIZE = "size"
        AGE = "avg_age"
    
    class Direction:
        DESCENDING = "desc"
        ASCENDING = "asc"
    
    def __init__(self, field, direction=None):
        if direction is None:
            direction = self.Direction.ASCENDING
        
        AbstractQueueModificationQuery.__init__(self, name="sort", sort=field,
            dir=direction)


class DeleteDownloadsQuery(AbstractQueueModificationQuery):
    def __init__(self, *download_ids):
        self.download_ids = download_ids
        
        AbstractQueueModificationQuery.__init__(self, name="delete",
            value=",".join(self.download_ids))


class DeleteAllDownloadsQuery(DeleteDownloadsQuery):
    def __init__(self):
        DeleteDownloadsQuery.__init__(self, "all")


class DeleteFileQuery(AbstractQueueQuery):
    def __init__(self, download_id, file_id):
        AbstractQueueQuery.__init__(self, name="delete_nzf",
            value=download_id, value2=file_id)


class RenameDownloadQuery(AbstractQueueModificationQuery):
    def __init__(self, download_id, new_name):
        self.download_id = download_id
        self.new_name = new_name
        
        AbstractQueueModificationQuery.__init__(self, name="rename",
            value=self.download_id, value2=self.new_name)


class ChangeCompleteActionQuery(AbstractQueueModificationQuery):
    class Action:
        SHUTDOWN_PC = "shutdown_pc"
        HIBERNATE_PC = "hibernate_pc"
        STANDBY_PC = "standby_pc"
        SHUTDOWN_PROGRAM = "shutdown_program"
        SCRIPT = "script_%s"
    
    def __init__(self, action):
        AbstractQueueModificationQuery.__init__(self,
            name="change_complete_action", value=action)


class PurgeQuery(AbstractQueueModificationQuery):
    def __init__(self):
        AbstractQueueModificationQuery.__init__(self, name="purge")


class PauseDownloadsQuery(AbstractQueueModificationQuery):
    def __init__(self, *download_ids):
        self.download_ids = download_ids
        
        AbstractQueueModificationQuery.__init__(self, name="pause",
            value=",".join(self.download_ids))


class ResumeDownloadsQuery(AbstractQueueModificationQuery):
    def __init__(self, *download_ids):
        self.download_ids = download_ids
        
        AbstractQueueModificationQuery.__init__(self, name="resume",
            value=",".join(self.download_ids))


class PriorityQuery(AbstractQueueModificationQuery):
    def __init__(self, priority, *download_ids):
        self.download_ids = download_ids
        self.priority = priority
        
        AbstractQueueModificationQuery.__init__(self, name="priority",
            value=",".join(self.download_ids), value2=priority)


class AddFileQuery(Query):
    file_name = ""
    
    def __init__(self, file_name, send_file=False, **arguments):
        self.file_name = file_name
        
        Query.__init__(self, **arguments)
        
        if send_file:
            self.arguments["nzbfile"] = open(self.file_name, "r")


class AddRemoteFileQuery(AddFileQuery):
    METHOD = "addfile"
    
    def __init__(self, file_name, post_processor=None, script=None,
        category=None, priority=None, name=None):
        AddFileQuery.__init__(self, file_name, send_file=True,
            pp=post_processor, script=script, cat=category, priority=priority,
            nzbname=name)


class AddLocalFileQuery(AddFileQuery):
    METHOD = "addlocalfile"
    
    def __init__(self, file_name, post_processor=None, script=None,
        category=None, priority=None, name=None):
        AddFileQuery.__init__(self, file_name, pp=post_processor, script=script,
            cat=category, priority=priority, name=file_name, nzbname=name)


class RetryQuery(AddFileQuery):
    METHOD = "retry"
    
    def __init__(self, file_name, download_id):
        AddFileQuery.__ini__(self, file_name, send_file=True, value=download_id)
    
    @property
    def required_version(self):
        return LooseVersion("0.6")


class SwitchDownloadQuery(Query):
    METHOD = "switch"
    
    def __init__(self, download_id, other_download_id):
        self.download_id = download_id
        self.other_download_id = other_download_id
        
        Query.__init__(self, value=download_id, value2=other_download_id)
    
    def affects_query(self, query):
        return isinstance(query, (QueueQuery, SwitchDownloadQuery))


class ChangeCategoryQuery(Query):
    METHOD = "change_cat"
    
    def __init__(self, category, *download_ids):
        self.category = category
        self.download_ids = download_ids
        
        Query.__init__(self, value=",".join(self.download_ids), value2=category)
    
    @property
    def required_version(self):
        if len(self.download_ids) > 1:
            return LooseVersion("0.6")
    
    def affects_query(self, query):
        return isinstance(query, QueueQuery)


class ChangeScriptQuery(Query):
    METHOD = "change_script"
    
    def __init__(self, script, *download_ids):
        self.script = script
        self.download_ids = download_ids
        
        Query.__init__(self, value=",".join(self.download_ids), value2=script)
    
    @property
    def required_version(self):
        if len(self.download_ids) > 1:
            return LooseVersion("0.6")
    
    def affects_query(self, query):
        return isinstance(query, QueueQuery)


class ChangePostProcessingQuery(Query):
    METHOD = "change_opts"
    
    def __init__(self, post_processing, *download_ids):
        self.post_processing = post_processing
        self.download_ids = download_ids
        
        Query.__init__(self, value=",".join(self.download_ids),
            value2=post_processing)
    
    @property
    def required_version(self):
        if len(self.download_ids) > 1:
            return LooseVersion("0.6")
    
    def affects_query(self, query):
        return isinstance(query, QueueQuery)


class AbstractHistoryQuery(Query):
    METHOD = "history"
    
    def affects_query(self, query):
        return isinstance(query, AbstractHistoryQuery)


class HistoryQuery(AbstractHistoryQuery):
    def __init__(self, start=None, limit=None):
        arguments = {}
        
        if start:
            arguments["start"] = start
        
        if limit:
            arguments["limit"] = limit
        
        AbstractHistoryQuery.__init__(self, **arguments)


class AbstractDeleteHistoryQuery(AbstractHistoryQuery):
    def __init__(self, value):
        AbstractHistoryQuery.__init__(self, name="delete", value=value)


class DeleteHistoryQuery(AbstractDeleteHistoryQuery):
    def __init__(self, *download_ids):
        self.download_ids = download_ids
        
        AbstractDeleteHistoryQuery.__init__(self,
            value=",".join(self.download_ids))


class DeleteAllHistoryQuery(AbstractDeleteHistoryQuery):
    def __init__(self):
        AbstractDeleteHistoryQuery.__init__(self, "all")


class FilesQuery(Query):
    METHOD = "get_files"
    
    def __init__(self, download_id):
        Query.__init__(self, value=download_id)


class AddURLQuery(Query):
    METHOD = "addurl"
    
    def __init__(self, url, post_processor=None, script=None,
        category=None, priority=None):
        self.url = url
        
        Query.__init__(self, pp=post_processor, script=script,
            cat=category, priority=priority, name=url)


class AddIDQuery(Query):
    METHOD = "addid"
    
    def __init__(self, id, post_processor=None, script=None,
        category=None, priority=None):
        self.id = id
        
        Query.__init__(self, pp=post_processor, script=script,
            cat=category, priority=priority, name=id)


class PauseQuery(Query):
    METHOD = "pause"
    
    def affects_query(self, query):
        return isinstance(query, (QueueQuery, ResumeQuery))


class ResumeQuery(Query):
    METHOD = "resume"
    
    def affects_query(self, query):
        return isinstance(query, (QueueQuery, PauseQuery))


class ExclusiveQuery(Query):
    def affects_query(self, query):
        return True


class ShutdownQuery(ExclusiveQuery):
    METHOD = "shutdown"


class RestartQuery(ExclusiveQuery):
    """
    Restart the SABnzbd daemon.
    
    In most cases, this query will be used to cause certain configuration
    changes to take effect.
    """
    
    METHOD = "restart"
    
    def __init__(self, blocking=True, timeout=None, new_connection_info=None):
        """
        If `blocking` is set to True, the query will not be complete until a
        connection to the restarted daemon has been established. `blocking`
        defaults to True.
        
        A default timeout is used to ensure that the query does not run forever
        for certain reasons. It's possible to override the default value by
        specifying the `timeout` parameter.
        
        Since the restart query can also be used to cause connection information
        changes to take effect (mostly port changes), it's possible to
        pass a `ConnectionInformation` to the constructor of the query that
        will be used when trying to establish a connection.
        """
        
        ExclusiveQuery.__init__(self)
        
        self.blocking = blocking
        self.timeout = timeout
        self.new_connection_info = new_connection_info
        self.start_time = None
    
    def start(self, connection_info):
        LOG.info(_("Restarting SABnzbd..."))
        
        if self.new_connection_info is None:
            self.new_connection_info = connection_info
        
        # It might take a while until `_parse_json_response` is actually called.
        # We want to take that time into account as well.
        self.start_time = time()
        
        ExclusiveQuery.start(self, connection_info)
    
    def _run(self):
        """
        Sometimes, the restart query times out for no apparent reason, even
        though the SABnzbd daemon has already been restarted successfully.
        
        That's why this method sets up a temporary short timeout that will make
        sure that LottaNZB won't wait for a response, but start checking if the
        connection can be established again.
        """
        
        try:
            ExclusiveQuery._run(self)
        except QueryConnectionTimeout:
            self._check_connection()
    
    def _parse_json_response(self, response):
        if self.blocking:
            return self._check_connection()
        
        return True
    
    def _check_connection(self):
        # Avoid circular imports.
        from lottanzb.backend.interface import Interface
        
        interface = Interface(self.new_connection_info)
        connection_established = False
        timeout = self.timeout or interface.START_TIMEOUT
        
        while not connection_established and time() - self.start_time < timeout:
            sleep(interface.HANDSHAKE_ATTEMPT_INTERVAL)
            
            try:
                interface.handshake()
            except QueryConnectionRefusedError:
                pass
            else:
                connection_established = True
        
        if not connection_established:
            raise QueryRestartTimeout(self, timeout)
        
        LOG.info(_("SABnzbd has been successfully restarted."))
        
        return True


class RestartRepairQuery(Query):
    """Only available in SABnzbd 0.6 or newer."""
    
    METHOD = "restart-repair"
    
    @property
    def required_version(self):
        return LooseVersion("0.6")


class DisconnectQuery(Query):
    METHOD = "disconnect"


class RescanQuery(Query):
    """Only available in SABnzbd 0.6 or newer."""
    
    METHOD = "rescan"
    
    @property
    def required_version(self):
        return LooseVersion("0.6")


class WarningsQuery(Query):
    METHOD = "warnings"


class ClearWarningsQuery(Query):
    METHOD = "warnings"
    
    def __init__(self):
        Query.__init__(self, name="clear")
    
    def affects_query(self, query):
        return isinstance(query, WarningsQuery)


class CategoriesQuery(Query):
    METHOD = "get_cats"


class ScriptsQuery(Query):
    METHOD = "get_scripts"


class VersionQuery(Query):
    METHOD = "version"
    
    class SpecialVersion:
        TRUNK = LooseVersion("trunk")
    
    def _parse_json_response(self, response):
        return LooseVersion(Query._parse_json_response(self, response))


class GetSpeedLimitQuery(Query):
    METHOD = "config"
    
    def __init__(self):
        Query.__init__(self, name="get_speedlimit")


class SetSpeedLimitQuery(Query):
    METHOD = "config"
    
    def __init__(self, limit=0):
        Query.__init__(self, name="set_speedlimit", value=limit)


class GetNewzbinBookmarksQuery(Query):
    METHOD = "newzbin"
    
    def __init__(self):
        Query.__init__(self, name="get_bookmarks")


class AuthenticationQuery(Query):
    """
    The `VersionQuery` will be successfully executed even if the username,
    password or API key is invalid. This query however will not perform and
    actions either and doesn't have any output but simply checks whether
    the connection information are valid and raises the proper exceptions.
    """
    
    # Choose a method name that will always yield a `QueryNotImplementedError`.
    METHOD = "will_never_be_implemented"
    
    def _parse_json_response(self, response):
        try:
            Query._parse_json_response(self, response)
        except QueryNotImplementedError:
            pass
        
        return True


class AuthenticationType:
    """
    Enumeration of different authentication types.
    
    Meaning of the three constants:
    
    - NOTHING: No authentication required.
    - USERNAME_PASSWORD: A username and a password is required, but no API key.
    - API_KEY: The API key is required, but no username and password.
    """
    
    NOTHING, USERNAME_PASSWORD, API_KEY = range(0, 3)


class AuthenticationTypeQuery(Query):
    """
    Determines the authentication type for a certain SABnzbd instance.
    
    It doesn't matter what username, password and API key is specified using the
    `ConnectionInformation`.
    
    The response is one of the constants in `AuthenticationType`.
    """
    
    METHOD = "auth"
    
    def _parse_json_response(self, response):
        auth_type = Query._parse_json_response(self, response)
        auth_type_map = {
            "None": AuthenticationType.NOTHING,
            "login": AuthenticationType.USERNAME_PASSWORD,
            "apikey": AuthenticationType.API_KEY
        }
        
        try:
            return auth_type_map[auth_type]
        except KeyError:
            raise QueryResponseError("Invalid authentication type '%s'." % 
                auth_type)
