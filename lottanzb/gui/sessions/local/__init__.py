# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.core.environ import _
from lottanzb.backend.sessions.local import LocalSession
from lottanzb.backend.sessions.errors import SessionNoServerError
from lottanzb.config.sabnzbd.servers import ServerConfig
from lottanzb.gui.prefs.servers import ServerEditorPane
from lottanzb.gui.sessions.base import View
from lottanzb.gui.sessions.local.sabnzbd_pane import SABnzbdPane
from lottanzb.gui.framework.proxy import (
    Conduit, GObjectEndPoint, EntryEndPoint, ToggleEndPoint, HostConduit,
    ValidatableConduitCollection, ValidatableConduit, SpinButtonEndPoint)


class LocalView(View):
    abstract = False
    builder_file = "session_selection_local"
    session_error_required = True
    
    def __init__(self, component_manager):
        View.__init__(self, component_manager)
        
        self._sabnzbd_config_copy = self._session.sabnzbd_config.deep_copy()
        self._conduits = ValidatableConduitCollection()
        
        self.sabnzbd_pane = SABnzbdPane(self._session.config)
        self.attach_slave(self.sabnzbd_pane_parent, self.sabnzbd_pane)
        
        if self._session.config.command:
            # For now, we assume that the specified command is valid.
            # `SessionError's might be passed to the view later on.
            self.sabnzbd_pane.hide()
        
        server_section = self._session.sabnzbd_config.servers
        
        if len(self._session.sabnzbd_config.servers):
            # TODO: The first server in the list may not be the wisest choice.
            server = server_section[0]
        else:
            server = ServerConfig(None)
            server_section.append(server)
        
        self.editor_pane = ServerEditorPane(server_section, server)
        self.attach_slave(self.server_editor_pane_parent, self.editor_pane)
        
        if not self.editor_pane.conduits.is_valid:
            self.server_settings.show()
        
        self._conduits.add(self.sabnzbd_pane.command_conduit)
        self._conduits.connect("notify::is-valid", self.on_validation_changed)
        self.editor_pane.conduits.connect("notify::is-valid",
            self.on_validation_changed)
        
        self.on_validation_changed()
    
    def on_validation_changed(self, *args):
        self.is_valid = self._conduits.is_valid and \
            self.editor_pane.conduits.is_valid
    
    def get_session_type(self):
        return LocalSession
    
    def get_radio_button_text(self):
        return _("_Download to this computer")
    
    def set_session_error(self, error):
        if self.sabnzbd_pane.can_handle_session_error(error):
            self.sabnzbd_pane.show()
            self.sabnzbd_pane.handle_session_error(error)
        elif not isinstance(error, SessionNoServerError):
            View.set_session_error(self, error)
    
    def restore(self):
        """Revert all changes made by the user while the view was displayed.
        
        Besides restoring the LottaNZB configuration, also the SABnzbd
        configuration file is restored to its initial state, prior to
        launching the session.
        """
        
        View.restore(self)
        
        self._sabnzbd_config_copy.save()
        self._session.sabnzbd_config.load()
