# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.core.environ import _
from lottanzb.backend import Backend
from lottanzb.config.sabnzbd import create_api_key
from lottanzb.gui.prefs.base import Tab
from lottanzb.gui.framework.proxy import (
    GObjectEndPoint, ToggleEndPoint, SpinButtonEndPoint, DelayedEntryEndPoint,
    LabelEndPoint, Conduit, ConduitCollection, DelayedPasswordEntryEndPoint)

class RemoteAccessTab(Tab):
    abstract = False
    builder_file = "prefs_tab_remote_access"
    label = _("Remote Access")
    help_topic = "remote-access"
    
    def __init__(self, component_manager):
        Tab.__init__(self, component_manager)
        
        self.conduits = ConduitCollection()
    
    def set_config(self, lottanzb_config, sabnzbd_config):
        self.conduits.remove_all()
        
        self.conduits.add(Conduit(
            GObjectEndPoint(sabnzbd_config.misc, "host"),
            RemoteConnnectionEndPoint(self.allow_remote_connections)))
        
        self.conduits.add(Conduit(
            GObjectEndPoint(sabnzbd_config.misc, "port"),
            SpinButtonEndPoint(self.port)))
        
        self.conduits.add(Conduit(
            GObjectEndPoint(sabnzbd_config.misc, "username"),
            DelayedEntryEndPoint(self.username)))
        
        self.conduits.add(Conduit(
            GObjectEndPoint(sabnzbd_config.misc, "password"),
            DelayedPasswordEntryEndPoint(self.password)))
        
        self.conduits.add(Conduit(
            GObjectEndPoint(sabnzbd_config.misc, "api_key"),
            LabelEndPoint(self.api_key)))
        
        info = self._component_manager.get(Backend).interface.connection_info
        
        self.allow_remote_connections.set_sensitive(info.is_local)
        
        if sabnzbd_config.misc.username or sabnzbd_config.misc.password:
            self.authenticate.set_active(True)
        else:
            self.authenticate.set_active(False)
        
        self.authenticate.emit("toggled")
    
    def on_authenticate__toggled(self, widget):
        active = widget.get_active()
        
        self.username.set_sensitive(active)
        self.username_label.set_sensitive(active)
        self.password.set_sensitive(active)
        self.password_label.set_sensitive(active)
        
        if not active:
            self.username.set_text("")
            self.password.set_text("")
    
    def on_generate_api_key__clicked(self, widget):
        self.api_key.set_text(create_api_key())
    
    def needs_restart(self, lottanzb_config, old_lottanzb_config,
        sabnzbd_config, old_sabnzbd_config):
        old = [old_sabnzbd_config.misc.host, old_sabnzbd_config.misc.port]
        new = [sabnzbd_config.misc.host, sabnzbd_config.misc.port]
        
        return old != new


class RemoteConnnectionEndPoint(ToggleEndPoint):
    def get_value(self):
        if self._object.get_active():
            return "0.0.0.0"
        else:
            return "localhost"
    
    def set_value(self, value):
        if value in ("localhost", "127.0.0.1"):
            self._object.set_active(False)
        else:
            self._object.set_active(True)
