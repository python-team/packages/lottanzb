# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from lottanzb.config.lotta import ConfigComponent
from lottanzb.core.component import ExtensionPoint, depends_on
from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import gproperty
from lottanzb.gui.help import open_help
from lottanzb.gui.prefs import base, general, servers, remote_access
from lottanzb.gui.framework import MainDelegateComponent
from lottanzb.backend import Backend
from lottanzb.backend.hubs.config import ConfigHub
from lottanzb.backend.interface.queries import RestartQuery

class Window(MainDelegateComponent):
    depends_on(ConfigComponent)
    depends_on(ConfigHub)
    
    builder_file = "prefs_window"
    tabs = ExtensionPoint(base.ITab)
    
    def __init__(self, component_manager):
        MainDelegateComponent.__init__(self, component_manager)
        
        # Get both LottaNZB's configuration as well as the configuration of
        # SABnzbd instance LottaNZB is connected to.
        lotta_config_component = self._component_manager.get(ConfigComponent)
        sabnzbd_config_hub = self._component_manager.get(ConfigHub)
        
        self.lottanzb_config = lotta_config_component.root
        self.sabnzbd_config = sabnzbd_config_hub.config
        
        # Make a backup of the configurations that will be used to check
        # if the user has changed anything.
        self.old_lottanzb_config = self.lottanzb_config.deep_copy()
        self.old_sabnzbd_config = self.sabnzbd_config.silent_deep_copy()
        
        # Add all available tabs to the dialog.
        # TODO: Tab order?
        for tab_class in self.tabs:
            tab = self._component_manager.load(tab_class)
            tab.set_config(self.lottanzb_config, self.sabnzbd_config)
            
            self._add_tab(tab)
    
    def _add_tab(self, tab):
        """Add a tab to the preferences window."""
        
        position = len(self.tabs)
        eventbox = gtk.EventBox()
        eventbox.show()
        
        self.notebook.insert_page(eventbox, gtk.Label(tab.label), position)
        self.attach_slave(eventbox, tab)
    
    def on_unload(self):
        """
        Save LottaNZB's configuration, unload all tabs and restart SABnzbd
        if the user has made changes to the configuration that require so.
        """
        
        # Only save the configuration if it has changed.
        if self.old_lottanzb_config != self.lottanzb_config:
            self.lottanzb_config.save()
        
        needs_restart = False
        
        for tab_class in self.tabs:
            tab = self._component_manager.get(tab_class)
            
            if tab and tab.needs_restart(
                self.lottanzb_config, self.old_lottanzb_config,
                self.sabnzbd_config, self.old_sabnzbd_config):
                needs_restart = True
        
        for tab_class in self.tabs:
            self._component_manager.unload(tab_class)
        
        if needs_restart:
            backend = self._component_manager.get(Backend)
            backend.interface.run_query(RestartQuery())
        
        MainDelegateComponent.on_unload(self)
    
    @property
    def current_tab(self):
        """The tab currently being displayed."""
        
        current_page = self.notebook.get_current_page()
        eventbox = self.notebook.get_nth_page(current_page)
        
        return self.get_slave(eventbox)
    
    def on_prefs_window__response(self, window, response):
        if response == gtk.RESPONSE_HELP:
            open_help(self.current_tab.help_topic)
            window.stop_emission("response")
