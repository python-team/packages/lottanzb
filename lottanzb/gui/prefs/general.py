# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from os.path import isfile, isdir

from lottanzb.core.environ import _
from lottanzb.core.constants import PostProcessing
from lottanzb.core.component import depends_on
from lottanzb.resources.xdg import USER_DIRS
from lottanzb.backend import Backend
from lottanzb.backend.hubs.capabilities import CapabilitiesHub
from lottanzb.gui.prefs.widgets import (AdditionalSoftwareNotice,
    AdditionalSoftwareDialog)
from lottanzb.gui.prefs.base import Tab
from lottanzb.gui.framework.proxy import (
    GObjectEndPoint, ToggleEndPoint, DelayedEntryEndPoint, SpinButtonEndPoint,
    FileChooserFolderEndPoint, Conduit, ConduitCollection)

class GeneralTab(Tab):
    depends_on(CapabilitiesHub)
    
    abstract = False
    builder_file = "prefs_tab_general"
    label = _("General")
    help_topic = "prefs-general-tab"
    
    def __init__(self, component_manager):
        self._software_warning = None
        self._conduit_collection = ConduitCollection()
        
        Tab.__init__(self, component_manager)
        
        self._capabilities_hub = self._component_manager.load(CapabilitiesHub)
        self._capabilities_hub.connect("updated", self.on_capabilities_updated)
    
    def create_ui(self):
        cell = gtk.CellRendererText()
        
        self.post_processing.set_model(PostProcessingComboBoxModel())
        self.post_processing.pack_start(cell, True)
        self.post_processing.add_attribute(cell, "markup",
            PostProcessingComboBoxModel.Column.DESCRIPTION)
        
        self._software_warning = AdditionalSoftwareNotice.build_for_widget(
            self.post_processing)
        self._software_warning.connect("activated", self.show_software_dialog)
    
    def set_config(self, lottanzb_config, sabnzbd_config):
        self._conduit_collection.remove_all()
        
        interface = self._component_manager.load(Backend).interface
        is_local = interface.connection_info.is_local
        
        # If no folder has yet been chosen to be watched for new NZB files,
        # use the default download folder rather than the working directory.
        self._conduit_collection.add(TogglableFolderConduit(
            GObjectEndPoint(sabnzbd_config.misc, "dirscan_dir"), self.dirscan,
            self.observed_folder_button, self.observed_folder_entry, is_local,
            USER_DIRS.get("XDG_DOWNLOAD_DIR", "")))
        
        self._conduit_collection.add(FolderConduit(
            GObjectEndPoint(sabnzbd_config.misc, "complete_dir"),
            self.download_folder_button, self.download_folder_entry, is_local))
        
        self._conduit_collection.add(Conduit(
            GObjectEndPoint(sabnzbd_config.misc, "dirscan_opts"),
            ProcessingComboBoxEndPoint(self.post_processing)))
        
        self._conduit_collection.add(ToggableConduit(
            GObjectEndPoint(sabnzbd_config.misc, "bandwidth_limit"),
            self.enforce_max_rate, 1000, 0, SpinButtonEndPoint(self.max_rate)))
        
        self.enforce_max_rate.emit("toggled")
        self._capabilities_hub.refresh()
    
    def on_capabilities_updated(self, capabilities_hub):
        self.update_software_warning_visibility()
    
    def on_post_processing__changed(self, widget):
        self.update_software_warning_visibility()
    
    def show_software_dialog(self, *args):
        from lottanzb.gui.prefs import Window
        
        prefs_window = self._component_manager.get(Window)
        
        dialog = self._component_manager.load(PostProcessingSoftwareDialog)
        dialog.show(prefs_window.get_toplevel())
    
    def update_software_warning_visibility(self):
        constant = self.get_selected_post_processing_constant()
        supported = self._capabilities_hub.supports_post_processing(constant)
        
        self._software_warning.set_property("visible", not supported)
        
        if supported:
            self._component_manager.unload(PostProcessingSoftwareDialog)
    
    def get_selected_post_processing_constant(self):
        index = self.post_processing.get_active()
        column = PostProcessingComboBoxModel.Column.ID
        
        return self.post_processing.get_model()[index][column]
    
    def on_enforce_max_rate__toggled(self, widget):
        active = self.enforce_max_rate.get_active()
        
        self.max_rate.set_sensitive(active)
        self.max_rate_scale.set_sensitive(active)


class ComplexToggleEndPoint(ToggleEndPoint):
    def __init__(self, widget, default_active_value, inactive_value):
        self._old_active_value = default_active_value
        self._inactive_value = inactive_value
        
        ToggleEndPoint.__init__(self, widget)
    
    def get_value(self):
        if ToggleEndPoint.get_value(self):
            return self._old_active_value
        else:
            return self._inactive_value
    
    def set_value(self, value):
        ToggleEndPoint.set_value(self, value)
        
        if value:
            self._old_active_value = value


class ToggableConduit(Conduit):
    def __init__(self, model_endpoint, checkbox, default_active_value,
        inactive_value, *ui_endpoints):
        if model_endpoint.get_value() != inactive_value:
            default_active_value = model_endpoint.get_value()
        
        self._inactive_value = inactive_value
        self._ui_endpoints = ui_endpoints
        
        Conduit.__init__(self, model_endpoint, ComplexToggleEndPoint(checkbox,
            default_active_value, inactive_value))
        
        if model_endpoint.get_value() == inactive_value:
            for ui_endpoint in self._ui_endpoints:
                ui_endpoint.set_value(default_active_value)
    
    def _propagate_change(self, changed_endpoint, value):
        if changed_endpoint not in self._ui_endpoints:
            active = value != self._inactive_value
            
            for endpoint in self._ui_endpoints:
                if active and endpoint not in self._endpoints:
                    self.add_endpoint(endpoint)
                elif not active and endpoint in self._endpoints:
                    self.remove_endpoint(endpoint)
                    
                endpoint._object.set_sensitive(active)
        
        Conduit._propagate_change(self, changed_endpoint, value)


class FolderConduitMixin:
    def get_ui_endpoints(self, file_chooser, entry, is_local=True):
        endpoints = [DelayedEntryEndPoint(entry)]
        
        if is_local:
            endpoints.append(FileChooserFolderEndPoint(file_chooser))
        
        file_chooser.set_property("visible", is_local)
        entry.set_property("visible", not is_local)
        
        return endpoints


class FolderConduit(Conduit, FolderConduitMixin):
    def __init__(self, model_endpoint, file_chooser, entry, is_local=True):
        ui_endpoints = self.get_ui_endpoints(file_chooser, entry, is_local)
        Conduit.__init__(self, model_endpoint, *ui_endpoints)


class TogglableFolderConduit(ToggableConduit, FolderConduitMixin):
    def __init__(self, model_endpoint, checkbox, file_chooser, entry,
        is_local=True, default_folder=""):
        ui_endpoints = self.get_ui_endpoints(file_chooser, entry, is_local)
        
        ToggableConduit.__init__(self, model_endpoint, checkbox, default_folder,
            "", *ui_endpoints)


class PostProcessingComboBoxModel(gtk.ListStore):
    class Column:
        ID, DESCRIPTION = range(0, 2)
    
    def __init__(self):
        gtk.ListStore.__init__(self, int, str)
        
        self.append([PostProcessing.NOTHING,
            _("<i>Do nothing</i>")])
        self.append([PostProcessing.REPAIR,
            _("Repair downloaded archives if necessary")])
        self.append([PostProcessing.UNPACK,
            _("Repair and extract downloaded archives")])
        self.append([PostProcessing.DELETE,
            _("Repair, extract and delete downloaded archives")])
    
    def post_processing_constant_to_index(self, constant):
        return [row[self.Column.ID] for row in self].index(constant)


class ProcessingComboBoxEndPoint(GObjectEndPoint):
    SIGNAL = "changed"
    
    def get_value(self):
        model = self._object.get_model()
        iter = self._object.get_active_iter()
        
        return model.get_value(iter, PostProcessingComboBoxModel.Column.ID)
    
    def set_value(self, value):
        model = self._object.get_model()
        index = model.post_processing_constant_to_index(value)
        
        self._object.set_active(index)


class PostProcessingSoftwareDialog(AdditionalSoftwareDialog):
    depends_on(CapabilitiesHub)
    
    def create_ui(self):
        AdditionalSoftwareDialog.create_ui(self)
        
        capabilities_hub = self._component_manager.load(CapabilitiesHub)
        interface = self._component_manager.load(Backend).interface
        
        descriptions = {
            "par": _("%s for verifying and reparing downloads"),
            "rar": _("%s for extracting downloaded archives"),
            "zip": _("%s for extracting downloaded archives")
        }
        
        names = {
            "par": _("<b>par2</b> or <b>par2cmdline</b>"),
            "rar": "<b>unrar</b>",
            "zip": "<b>unzip</b>"
        }
        
        if interface.connection_info.is_local:
            if isfile("/etc/fedora-release"):
                names["par"] = "<b>par2cmdline</b>"
            else:
                try:
                    with open("/etc/lsb-release") as release_file:
                        content = release_file.read()
                        
                        if "Ubuntu" in content or "Debian" in content:
                            names["par"] = "<b>par2</b>"
                except IOError:
                    pass
        
        for software in names.keys():
            if not capabilities_hub.supports_software(software):
                self.add_software(descriptions[software] % names[software])
