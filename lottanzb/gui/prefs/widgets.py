# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re
import gtk

from lottanzb.core.component import depends_on
from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import gsignal
from lottanzb.backend import Backend
from lottanzb.backend.interface.queries import RestartQuery
from lottanzb.gui.framework import MainDelegateComponent

__all__ = ["WarningNotice", "AdditionalSoftwareNotice",
    "AdditionalSoftwareDialog"]

class WarningNotice(gtk.Alignment):
    """Simple widget that contains a warning icon and a text right to it."""
    
    def __init__(self, text):
        gtk.Alignment.__init__(self)
        
        self._icon = gtk.image_new_from_stock(gtk.STOCK_DIALOG_WARNING,
            gtk.ICON_SIZE_MENU)
        self._label = gtk.Label()

        self._hbox = gtk.HBox(spacing=6)
        self._hbox.pack_start(self._icon, expand=False)
        self._hbox.pack_start(self._label)
        self._hbox.show_all()
        
        self.add(self._hbox)
        self._label.set_markup(text)
    
    @classmethod
    def build_for_widget(cls, widget, *args):
        """Create a new warning notice and add it below a given widget.
        
        This requires the parent of the widget to be of type `gtk.Bin'.
        """
        
        notice = cls(*args)
        bin = widget.parent
        
        if not isinstance(bin, gtk.Bin):
            raise ValueError("Parent of `widget' needs to be of type "
                "`gtk.Bin', not %r." % type(bin))
        
        bin.remove(widget)
        
        vbox = gtk.VBox()
        vbox.pack_start(widget, expand=True)
        vbox.pack_start(notice, expand=False)
        vbox.set_property("visible", True)
        
        notice.set_property("top-padding", 3)
        bin.add(vbox)
        
        return notice


class AdditionalSoftwareNotice(WarningNotice):
    gsignal("activated")
    
    def __init__(self):
        text = _("<a>Additional software</a> is required.")
        text = text.replace("<a>", "<a href='lottanzb:null'>")
        
        WarningNotice.__init__(self, text)
        
        def on_activate_link(widget, *args):
            self.emit("activated")
        
        self._label.connect("activate-link", on_activate_link)


class AdditionalSoftwareDialog(MainDelegateComponent):
    depends_on(Backend)
    
    builder_file = "additional_software_dialog"
    
    def create_ui(self):
        interface = self._component_manager.load(Backend).interface
        
        if interface.connection_info.is_remote:
            software_text = _("To make full use of LottaNZB, please install "
                "the following software on '%s' and restart SABnzbd using the "
                "button below.") % interface.connection_info.host
        else:
            software_text = _("To make full use of LottaNZB, please install "
                "the following software and restart SABnzbd using the button "
                "below.")
        
        self.get_toplevel().format_secondary_markup(software_text + "\n\n")
        self.get_toplevel().set_default_response(gtk.RESPONSE_CLOSE)
        self.get_toplevel().set_focus(self.close)
    
    def add_software(self, software):
        text = self.get_toplevel().get_property("secondary-text")
        text += "  - " + software + "\n"
        
        self.get_toplevel().format_secondary_markup(text)
    
    def on_additional_software_dialog__response(self, widget, response):
        if response == gtk.RESPONSE_OK:
            backend = self._component_manager.get(Backend)
            backend.interface.run_query(RestartQuery())
