# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from lottanzb.gui.framework import SlaveDelegateComponent
from lottanzb.core.component import implements

class ITab:
    label = ""
    
    # Help topic to be opened when clicking 'Help'.
    help_topic = ""
    
    def set_config(self, lottanzb_config, sabnzbd_config):
        pass
    
    def needs_restart(self, lottanzb_config, old_lottanzb_config,
        sabnzbd_config, old_sabnzbd_config):
        pass


class Tab(SlaveDelegateComponent):
    implements(ITab)
    
    help_topic = ""
    abstract = True
    
    def set_config(self, lottanzb_config, sabnzbd_config):
        pass
    
    def needs_restart(self, lottanzb_config, old_lottanzb_config,
        sabnzbd_config, old_sabnzbd_config):
        return False