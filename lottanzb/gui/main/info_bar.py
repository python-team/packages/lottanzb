# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from lottanzb.core.component import depends_on
from lottanzb.core.environ import _
from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.gui.framework import SlaveDelegateComponent
from lottanzb.backend.hubs.general import GeneralHub
from lottanzb.backend.hubs.statistics import StatisticsHub
from lottanzb.gui.main.speed_limit_menu import SpeedLimitMenuComponent
from lottanzb.gui.framework.widgets import MenuButton

__all__ = ["InfoBar"]

class InfoBar(SlaveDelegateComponent):
    builder_file = "info_bar"
    
    depends_on(GeneralHub)
    depends_on(StatisticsHub)
    
    def create_ui(self):
        speed_icon = gtk.image_new_from_stock(gtk.STOCK_GO_DOWN,
            gtk.ICON_SIZE_MENU)
        
        self.speed_button = MenuButton()
        self.speed_button.set_image(speed_icon)
        self.speed_button.set_relief(gtk.RELIEF_NONE)
        self.speed_button.set_tooltip_text(_("Limit the download speed"))
        self.speed_button.show()
        self.speed_container.pack_start(self.speed_button)
        
        general_hub = self._component_manager.get(GeneralHub)
        
        with gtk_lock:
            general_hub.connect("notify::speed",
                gtk_lock.locked(self.on_speed_changed))
            general_hub.connect("notify::size-left",
                gtk_lock.locked(self.on_remaining_changed))
            general_hub.connect("notify::time-left",
                gtk_lock.locked(self.on_remaining_changed))
        
        self.on_speed_changed(general_hub)
        self.on_remaining_changed(general_hub)
        
        statistics_hub = self._component_manager.get(StatisticsHub)
        
        with gtk_lock:
            for folder_type in ("download", "temp"):
                statistics_hub.connect("notify::free-%s-folder-space" % \
                    folder_type, gtk_lock.locked(self.on_free_space_changed),
                    general_hub)
        
        self.on_free_space_changed(statistics_hub, None, general_hub)
    
    def on_speed_changed(self, general_hub, *args):
        new_label = str(general_hub.speed)
        
        with gtk_lock:
            if self.speed.get_label() != new_label:
                self.speed.set_label(new_label)
    
    def on_remaining_changed(self, general_hub, *args):
        if general_hub.time_left:
            text = _("{0.size_left} left ({0.time_left.short})")
        else:
            text = _("{0.size_left} left")
        
        new_label = text.format(general_hub)
        
        with gtk_lock:
            if self.total_remaining.get_label() != new_label:
                self.total_remaining.set_label(new_label)
    
    def on_free_space_changed(self, statistics_hub, param, general_hub):
        # NOTE: Don't get the general hub from the component manager within
        # the method mody. It might have been unloaded since the signal was
        # emitted. Thus, prevent this race condition by using the reference
        # obtained in the constructor.
        free_download_space = statistics_hub.free_download_folder_space
        free_temp_space = statistics_hub.free_temp_folder_space
        
        if free_download_space is None or free_temp_space is None:
            return
        
        new_label = _("{0} free").format(free_download_space)
        size_left = general_hub.size_left
        
        # Use red color to indicate that the size of the queue exceeds the
        # available space in the download folder.
        if size_left is not None and free_download_space < size_left:
            new_label = "<span color=\"red\">{0}</span>".format(new_label)
        
        # Only show the free space in the temporary folder if it is on a
        # different partition.
        # TODO: Could also use red color to indicate potential problems.
        if free_download_space != free_temp_space:
            new_label += "\n" + _("{0} free (temp)").format(free_temp_space)
            new_label = "<span size=\"7200\">{0}</span>".format(new_label)
        
        with gtk_lock:
            self.free_space_container.show()
            
            if self.free_space.get_label() != new_label:
                self.free_space.set_markup(new_label)
    
    def on_speed_button__show_menu(self, button):
        menu_component = self._component_manager.load(SpeedLimitMenuComponent)
        button.menu = menu_component.get_menu()
