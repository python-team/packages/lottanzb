# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from lottanzb.core.component import depends_on, Component
from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import gsignal
from lottanzb.backend.hubs import DataSpeed
from lottanzb.backend.hubs.general import GeneralHub
from lottanzb.backend.hubs.config import ConfigHub

__all__ = ["SpeedLimitMenuComponent"]

class SpeedLimit(DataSpeed):
    """A download speed limit given in kilobytes per second, where as a
    a value of 0 indicates unlimited download speed.
    """
    
    def __init__(self, speed_limit):
        DataSpeed.__init__(self, kibibytes=speed_limit)
    
    def __str__(self):
        """Overrides the string representation defined by `DataSpeed' such that
        unlimited download speed is indicated by a localized 'Unlimited'.
        """
        
        if self._data == 0:
            return _("Unlimited")
        else:
            return DataSpeed.__str__(self)


class SpeedLimitMenuComponent(Component):
    """Allows clients to display a menu that makes it possible to review and set
    the download speed limit.
    
    It's possible to spawn as many instances of the menu using the method
    `popup'. However, existing instances won't be updated when the configuration
    changes in any way.
    
    Changes to the download speed limit are instantaneously applied using the
    `ConfigHub'.
    
    At the top of the menu, the user is presented with a way of removing any
    download speed limit, followed by a list of predefined possible download
    speed limits.
    """
    
    depends_on(ConfigHub)
    depends_on(GeneralHub)
    
    # A predefined list of download speed limits in decreasing order.
    #
    # TODO: One might consider measuring the maximum download speed when no
    # limit is set and use that information to compute a reasonable set of
    # predefined speed limits. However, this would need to be constantly updated
    # as the user might access different networks.
    DEFAULT_SPEED_LIMITS = map(SpeedLimit, [2000, 1000, 500, 250, 100, 50])
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        self._config_hub = self._component_manager.get(ConfigHub)
    
    def get_menu(self):
        """Build a new menu for setting the download speed limit"""
        
        speed_limit = self._get_speed_limit()
        
        menu = SpeedLimitMenu(speed_limit)
        menu.add_speed_limit(SpeedLimit(0))
        
        if speed_limit and speed_limit not in self.DEFAULT_SPEED_LIMITS:
            menu.add_speed_limit(speed_limit)
        
        menu.add_separator()
        
        for default_speed_limit in self.DEFAULT_SPEED_LIMITS:
            menu.add_speed_limit(default_speed_limit)
        
        def on_speed_limit_changed(menu, speed_limit):
            self._set_speed_limit(speed_limit)
        
        menu.connect("speed-limit-changed", on_speed_limit_changed)
        
        return menu
    
    def _get_speed_limit(self):
        return SpeedLimit(self._config_hub.get_bandwidth_limit())
    
    def _set_speed_limit(self, speed_limit):
        self._config_hub.set_bandwidth_limit(speed_limit.kibibytes_per_second)


class SpeedLimitMenu(gtk.Menu):
    gsignal("speed-limit-changed", object)
    
    def __init__(self, active_speed_limit):
        self.active_speed_limit = active_speed_limit
        
        gtk.Menu.__init__(self)
    
    def add_speed_limit(self, speed_limit):
        try:
            radio_group = self.get_children()[0]
        except IndexError:
            radio_group = None
        
        index = len(self)
        menu_item = gtk.RadioMenuItem(radio_group, str(speed_limit))
        
        if self.active_speed_limit == speed_limit:
            menu_item.set_active(True)
        
        menu_item.connect("toggled", self.on_menu_item_toggled, speed_limit)
        menu_item.show()
        
        self.attach(menu_item, 0, 1, index, index + 1)
    
    def add_separator(self):
        index = len(self)
        menu_item = gtk.SeparatorMenuItem()
        menu_item.show()
        
        self.attach(menu_item, 0, 1, index, index + 1)
    
    def on_menu_item_toggled(self, radio_menu_item, speed_limit):
        if radio_menu_item.get_active():
            self.emit("speed-limit-changed", speed_limit)
