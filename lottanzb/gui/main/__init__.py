# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import sys
import gtk
import webbrowser

import logging
LOG = logging.getLogger(__name__)

from threading import Thread

from lottanzb.core.environ import _
from lottanzb.backend import Backend
from lottanzb.backend.sessions.local import LocalSession
from lottanzb.backend.sessions.remote import RemoteSession
from lottanzb.backend.hubs.config import ConfigHub
from lottanzb.backend.hubs.general import GeneralHub
from lottanzb.config.errors import OptionError
from lottanzb.config.section import ConfigSection
from lottanzb.config.lotta import ConfigComponent
from lottanzb.util.gobject_extras import gproperty
from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.util.signalmanager import SignalManager
from lottanzb.util.misc import open_folder
from lottanzb.gui import add_file, add_url, about, log, prefs, sessions
from lottanzb.gui.help import open_help
from lottanzb.gui.messaging import MessagingManager
from lottanzb.gui.main.download_list import DownloadList
from lottanzb.gui.main.info_bar import InfoBar
from lottanzb.gui.framework import MainDelegateComponent
from lottanzb.gui.framework.proxy import (
    ConduitCollection, Conduit, GObjectEndPoint, ToggleActionEndPoint)

class Config(ConfigSection):
    # Holds a configuration section of type `ViewConfig'.
    view = gproperty(type=object)
    
    width = gproperty(type=int, default=700)
    height = gproperty(type=int, default=350)
    x_pos = gproperty(type=int, default=100)
    y_pos = gproperty(type=int, default=100)
    maximized = gproperty(type=bool, default=False)

    def find_section_class(self, section):
        if section == "view":
            return ViewConfig
        
        return ConfigSection.find_section_class(self, section)


class ViewConfig(ConfigSection):
    """Holds information about what elements are visible in the main window.
    
    The boolean options have been moved to a separate configuration section
    because they are closely related and the functionality might one day be
    moved to a separate submodule called `view'.
    """
    
    toolbar = gproperty(type=bool, default=True)
    infobar = gproperty(type=bool, default=True)
    reordering_pane = gproperty(type=bool, default=True)


class WidgetVisibilityEndPoint(GObjectEndPoint):
    """An endpoint for a widget that modifies the widget's visibility given
    boolean values sent through the conduit.
    """
    
    PROPERTY = "visible"


class MainWindow(MainDelegateComponent):
    builder_file = "main_window"
    general_hub = gproperty(type=object)
    
    # If True, the application is entirely shut down when the window is closed.
    # If False, the application is only hidden when the window is closed.
    quit_on_delete = True
    
    def __init__(self, component_manager):
        MainDelegateComponent.__init__(self, component_manager)
        
        self.session_signals = SignalManager()
        self.session_conduits = ConduitCollection()
        
        # The list of actions that may only be used if connected to an instance
        # of SABnzbd.
        self.backend_actions = [self.add, self.add_url, self.clear, self.paused,
            self.open_download_folder, self.edit_preferences,
            self.select_local_session, self.select_remote_session,
            self.open_web_interface]
        
        def on_general_hub_changed(self, *args):
            self.on_general_hub_updated(None)
        
        self.connect("notify::general-hub", on_general_hub_changed)
        self.notify("general-hub")
    
    def create_ui(self):
        self.enable_rgba_support()
        self.restore_window_geometry()
        
        # Make the 'View -> Toolbar' `gtk.CheckMenuItem' functional.
        self.view_toolbar_conduit = Conduit(
            GObjectEndPoint(self.config.view, "toolbar"),
            ToggleActionEndPoint(self.view_toolbar),
            WidgetVisibilityEndPoint(self.toolbar))
        
        # Make the 'View -> Infobar' `gtk.CheckMenuItem' functional.
        self.view_infobar_conduit = Conduit(
            GObjectEndPoint(self.config.view, "infobar"),
            ToggleActionEndPoint(self.view_infobar),
            WidgetVisibilityEndPoint(self.infobar))
        
        # Make the 'View -> Reordering Pane' `gtk.CheckMenuItem' functional.
        # Because the actual reordering pane is dynamically added to the window
        # when LottaNZB is connected to SABnzbd, the corresponding end point
        # will also be added and removed dynamically.
        # This is required so that the check menu items is still functional
        # even if LottaNZB is not connected to SABnzbd.
        self.view_reordering_pane_conduit = Conduit(
            GObjectEndPoint(self.config.view, "reordering_pane"),
            ToggleActionEndPoint(self.view_reordering_pane))
        
        self.view_reordering_pane_endpoint = None
        
        self.select_local_session.set_property("visible", False)
        
        # Show the panel menu unless the user has explicitly disabled it in
        # the configuration file, either using a previous version of LottaNZB
        # or by hand.
        # Right now, it's not possible to disable or enable it using the UI,
        # as the plug-in infrastructure has not been ported yet.
        config = self._component_manager.get(ConfigComponent).root
        panel_menu_enabled = True
        
        try:
            if config["plugins"]["panel_menu"]["enabled"] == "False":
                panel_menu_enabled = False
        except OptionError:
            pass
        
        if panel_menu_enabled:
            # Lazy import.
            from lottanzb.gui.panel_menu import PanelMenuComponent
            self._component_manager.load_async(PanelMenuComponent)
    
    def attach_to_backend(self, backend):
        self.general_hub = self._component_manager.load(GeneralHub)
        
        self.session_signals.connect(self.general_hub, "updated",
            self.on_general_hub_updated)
        self.session_conduits.add(Conduit(
            GObjectEndPoint(self.general_hub, "paused"),
            ToggleActionEndPoint(self.paused)))
        
        # Lazy import.
        from lottanzb.gui.messaging import (download_import,
            postprocessing_priority, servers)
        
        infobar = self._component_manager.load(InfoBar)
        download_list = self._component_manager.load(DownloadList)
        manager = self._component_manager.load(MessagingManager)
        
        with gtk_lock:
            self.open_download_folder.set_property("visible",
                backend.interface.connection_info.is_local)
            
            self.attach_slave(self.infobar, infobar)
            self.attach_slave(self.download_list, download_list)
            self.attach_slave(self.message, manager)
            
            self.view_reordering_pane_endpoint = WidgetVisibilityEndPoint(
                download_list.reordering_pane)
            self.view_reordering_pane_conduit.add_endpoint(
                self.view_reordering_pane_endpoint)
            
            self.toolbar_remove.set_related_action(download_list.remove)
            self.select_local_session.set_property("visible",
                not isinstance(backend.session, LocalSession))
    
    def detach_from_backend(self):
        self.detach_all_slaves()
        self.session_signals.disconnect_all()
        self.session_conduits.remove_all()
        
        if self.view_reordering_pane_endpoint:
            self.view_reordering_pane_conduit.remove_endpoint(
                self.view_reordering_pane_endpoint)
        
        self._component_manager.unload(DownloadList)
        self._component_manager.unload(MessagingManager)
        
        with gtk_lock:
            self.open_download_folder.set_property("visible", False)
        
        self.general_hub = None
    
    def enable_rgba_support(self):
        """Try to enable RGBA support"""
        
        try:
            gtk_screen = self.get_toplevel().get_screen()
            rgba_colormap = gtk_screen.get_rgba_colormap()
            
            if rgba_colormap:
                gtk.widget_set_default_colormap(rgba_colormap)
        except AttributeError:
            LOG.debug("RGBA support not available.")
        else:
            LOG.debug("RGBA support enabled.")
    
    def restore_window_geometry(self):
        """
        Restore the size and position, as well as the maximization state of the
        window according to the values in the configuration.
        """
        
        if self.config.maximized:
            self.get_toplevel().maximize()
        else:
            self.get_toplevel().resize(self.config.width, self.config.height)
            self.get_toplevel().move(self.config.x_pos, self.config.y_pos)
    
    def on_general_hub_updated(self, hub):
        """When the general hub is not available, all actions depending on it
        are made insensitive.
        
        The 'Clear completed' action is only made sensitive if there are
        completed downloads.
        """
        
        action_visibility = {}
        
        for action in self.backend_actions:
            action_visibility[action] = bool(hub)
        
        if self.general_hub:
            history = self.general_hub.downloads.get_filter_complete()
            action_visibility[self.clear] = bool(len(history))
        else:
            with gtk_lock:
                # FIXME: While `DownloadList' is not loaded, this toolbar button
                # is not connected to an action and thus sensitive by default.
                self.toolbar_remove.set_property("sensitive", False)
        
        with gtk_lock:
            for action, visibility in action_visibility.items():
                action.set_property("sensitive", visibility)
    
    def on_widget__configure_event(self, widget, event):
        """
        Store the window size and position in the configuration if it's
        not maximized.
        """
        
        if not self.config.maximized:
            position = widget.get_position()
            
            self.config.width = event.width
            self.config.height = event.height
            self.config.x_pos = position[0]
            self.config.y_pos = position[1]
    
    def on_widget__window_state_event(self, widget, event):
        """
        Store the window state in the configuration if it has been maximized.
        """
        
        if event.changed_mask & gtk.gdk.WINDOW_STATE_MAXIMIZED:
            maximized = event.new_window_state & gtk.gdk.WINDOW_STATE_MAXIMIZED
            self.config.maximized = maximized
        
        return False
    
    def on_widget__delete_event(self, widget, event):
        """Quits the application if necessary."""
        
        if self.quit_on_delete:
            Thread(target=sys.exit).run()
        else:
            self.hide()
        
        return True
    
    def show_session_selection_dialog(self, session_type):
        self._component_manager.unload(Backend)
        
        dialog = self._component_manager.load(sessions.SelectionDialog)
        dialog.preselect_session_type(session_type)
    
    def on_select_local_session__activate(self, widget):
        self.show_session_selection_dialog(LocalSession)
    
    def on_select_remote_session__activate(self, widget):
        self.show_session_selection_dialog(RemoteSession)
    
    def on_open_web_interface__activate(self, widget):
        """Open the SABnzbd web interface in a browser tab."""
        
        backend = self._component_manager.get(Backend)
        url = backend.interface.connection_info.url
        
        webbrowser.open_new_tab(url)
    
    def on_edit_preferences__activate(self, widget):
        """Open the preferences dialog."""
        
        window = self._component_manager.load(prefs.Window)
        window.show(self.get_toplevel())
    
    def on_report_bug__activate(self, widget):
        webbrowser.open_new_tab("https://bugs.launchpad.net/lottanzb/+filebug")
    
    def on_translate_application__activate(self, widget):
        webbrowser.open_new_tab("https://translations.launchpad.net/lottanzb")
    
    def on_show_message_log__activate(self, widget):
        window = self._component_manager.load(log.Window)
        window.show(self.get_toplevel())
    
    def on_show_help_content__activate(self, widget):
        open_help()
    
    def on_show_about_dialog__activate(self, widget):
        dialog = self._component_manager.load(about.Dialog)
        dialog.show(self.get_toplevel())
    
    def on_add__activate(self, *args):
        """Display a dialog using which a user can add one ore more NZB files
        to the download queue.
        """
        
        dialog = self._component_manager.load(add_file.Dialog)
        dialog.show(self.get_toplevel())
    
    def on_add_url__activate(self, *args):
        dialog = self._component_manager.load(add_url.Dialog)
        dialog.show(self.get_toplevel())
    
    def on_quit__activate(self, *args):
        Thread(target=sys.exit).run()
    
    def on_clear__activate(self, widget):
        if self.general_hub:
            self.general_hub.delete_all_history()
    
    def on_open_download_folder__activate(self, widget):
        config_hub = self._component_manager.get(ConfigHub)
        
        if config_hub:
            download_folder = config_hub.config.misc.complete_dir
            
            open_folder(download_folder)
