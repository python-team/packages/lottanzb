# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk
import re

from lottanzb.core.component import depends_on
from lottanzb.core.environ import _
from lottanzb.gui.help import open_help
from lottanzb.gui.framework import MainDelegateComponent
from lottanzb.gui.framework.proxy import (ValidatableConduit, EntryEndPoint, 
    ValidationInvalidError)
from lottanzb.backend.hubs.general import GeneralHub

class Dialog(MainDelegateComponent):
    depends_on(GeneralHub)
    
    builder_file = "add_url_dialog"
    
    def create_ui(self):
        self.conduit = URLConduit(EntryEndPoint(self.url))
        self.conduit.mandatory = True
        self.conduit.connect("notify::is-valid", self.on_change_validity)
        self.on_change_validity()
    
    def on_change_validity(self, *args):
        self.add.set_sensitive(self.conduit.is_valid)
    
    def on_add_url_dialog__response(self, dialog, response):
        general_hub = self._component_manager.get(GeneralHub)
        
        if response == gtk.RESPONSE_OK and general_hub:
            general_hub.add_url(self.url.get_text())
        elif response == gtk.RESPONSE_HELP:
            open_help("adding-downloads")
            dialog.stop_emission("response")


class URLConduit(ValidatableConduit):
    URL_REGEX = re.compile(r"""
        https?:((//)|(\\\\))+[\w\d:#@%/;$()~_?\+-=\\\.&]*
        """, re.VERBOSE)
    
    def validate_endpoint_value(self, endpoint, value):
        ValidatableConduit.validate_endpoint_value(self, endpoint, value)
        
        if not self.URL_REGEX.match(value):
            raise ValidationInvalidError(self, endpoint, value, \
                _("Invalid URL"))
