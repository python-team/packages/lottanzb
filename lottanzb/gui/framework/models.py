# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from lottanzb.config.errors import InexistentOptionError

class ListConfigSectionTreeModel(gtk.GenericTreeModel):
    def __init__(self, section):
        self._section = section
        self._section.connect("property-deleted", self.on_property_deleted)
        self._section.connect("property-changed", self.on_property_changed)
        
        self._items_by_id = {}
        
        for item in section.values():
            self._items_by_id[id(item)] = item
        
        gtk.GenericTreeModel.__init__(self)

    def on_property_deleted(self, section, source_section, key, value):
        if source_section is section:
            path = self.get_index(value)
            self.emit("row-deleted", path)
            del self._items_by_id[id(value)]
    
    def on_property_changed(self, section, source_section, key, value):
        if source_section is section:
            item = value
        else:
            item = source_section
            
            while not item.get_parent() is section:
                item = item.get_parent()
        
        path = self.get_index(item)
        
        if id(item) in self._items_by_id:
            self.emit("row-changed", path, self.get_iter(path))
        else:
            self._items_by_id[id(item)] = item
            self.emit("row-inserted", path, self.get_iter(path))
    
    def on_get_flags(self):
        return gtk.TREE_MODEL_ITERS_PERSIST | gtk.TREE_MODEL_LIST_ONLY
    
    def on_get_n_columns(self):
        return 1
    
    def on_get_column_type(self, index):
        return object
    
    def on_get_iter(self, path):
        # Will be called at least once with argument `(0,)', no matter whether
        # there are any items in the list.
        try:
            return id(self._section[path[0]])
        except InexistentOptionError:
            return None
    
    def on_get_path(self, rowref):
        return self.get_index(self._items_by_id[rowref])
    
    def on_get_value(self, rowref, column):
        return self._items_by_id[rowref]
    
    def on_iter_next(self, rowref):
        try:
            return id(self._section[self.on_get_path(rowref) + 1])
        except InexistentOptionError:
            return None
    
    def on_iter_children(self, parent):
        if parent:
            return None
        
        return self._section[0]
    
    def on_iter_has_child(self, rowref):
        return False
    
    def on_iter_n_children(self, rowref):
        if rowref:
            return 0
        
        return len(self._section)
    
    def on_iter_nth_child(self, parent, n):
        if parent:
            return None
        
        try:
            return self._section[n]
        except InexistentOptionError:
            return None
    
    def on_iter_parent(self, child):
        return None
    
    def get_index(self, item):
        return int(item.get_name())
