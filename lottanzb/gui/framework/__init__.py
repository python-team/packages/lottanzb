# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gobject
import gtk
import re

from lottanzb import resources
from lottanzb.core import environ
from lottanzb.config.lotta import ConfigurableComponent
from lottanzb.util.signalmanager import SignalManager
from lottanzb.util.gtk_extras import gtk_lock

__all__ = ["BaseDelegate", "SlaveDelegate", "MainDelegate",
    "SlaveDelegateComponent", "MainDelegateComponent"]

class BaseDelegate:
    """
    Abstract base delegate functionality.
    
    It uses hand-created and GTKBuilder-created UIs, and combinations of the
    two, and is responsible for automatically loading UI files from resources,
    and connecting signals.
    
    The abstract elements of this class are:

        1. The way it gets a top-level widget from a UI file
        2. How it creates a default top-level widget if one was not found in the
           UI file, or no UI file is specified.
    """
    
    builder_file = None
    toplevel_name = "main"
    
    def __init__(self):
        self._toplevel = None
        self._slaves = {}
        self._signal_manager = SignalManager()
        
        self._load_builder()
        
        if self._toplevel is None:
            self._toplevel = self.create_default_toplevel()
        
        self.widget = self._toplevel
        
        self.create_ui()
        self._connect_signals()
    
    # Public API
    def get_builder_toplevel(self, builder):
        """Get the top-level widget from a GTKBuilder file."""
        
        raise NotImplementedError
    
    def create_default_toplevel(self):
        raise NotImplementedError
    
    def _load_builder(self):
        if not self.builder_file:
            return
        
        builder_file = self.builder_file
        
        if not builder_file.endswith(".ui"):
            builder_file += ".ui"
        
        builder = gtk.Builder()
        builder.set_translation_domain(environ.DOMAIN)
        builder.add_from_file(resources.get_ui(builder_file))
        
        self._toplevel = self.get_builder_toplevel(builder)
        
        for obj in builder.get_objects():
            name = ""
            
            try:
                # Ugly fix for bug #507739.
                # Starting with GTK 2.19, the `gtk.Widget` method `get_name`
                # doesn't work anymore. The problem is that the "name" property
                # is only meant to be used w.r.t. themes and doesn't need to be
                # unique. The GtkBuilder ID however needs to be unique and was
                # bound to the "name" property before GTK 2.18.
                # Right now, there is no clean way to get the GtkBuilder ID
                # through the PyGTK API.
                # For those objects, that don't implement `gtk.Buildable`, it's
                # still necesssary to use the `name` property.
                if isinstance(obj, gtk.Buildable):
                    name = gtk.Buildable.get_name(obj)
                else:
                    name = obj.get_property("name")
            except TypeError:
                pass
            else:
                if name:
                    setattr(self, name, obj)
    
    def _connect_signals(self):
        HANDLER_PATTERN = re.compile("""
            (?P<signal_type>(on|after))_
            (?P<widget_name>\w+)__
            (?P<signal_name>\w+)
        """, re.VERBOSE)
        
        for name in dir(self):
            match = HANDLER_PATTERN.match(name)
            
            if match:
                method = getattr(self, name)
                signal_type = match.groupdict()["signal_type"]
                widget_name = match.groupdict()["widget_name"]
                signal_name = match.groupdict()["signal_name"]
                widget = getattr(self, widget_name, None)
                
                if widget is None:
                    raise LookupError("Widget named '%s' is not available." % \
                        widget_name)
                
                if signal_type == "on":
                    widget.connect(signal_name, gtk_lock.locked(method))
                elif signal_type == "after":
                    widget.connect_after(signal_name, gtk_lock.locked(method))
    
    @staticmethod
    def _get_first_builder_window(builder):
        """
        Get the first top-level widget in a gtk.Builder hierarchy.
        
        This is mostly used for guessing purposes, and an explicit naming is
        always going to be a better situation.
        """
        
        for obj in builder.get_objects():
            if isinstance(obj, gtk.Window):
                return obj
    
    def show(self, parent=None):
        """Shows the top-level widget"""
        
        if hasattr(self._toplevel, "present"):
            self._toplevel.present()
        else:
            self._toplevel.show()
        
        if parent is not None:
            self._toplevel.set_transient_for(parent)
    
    def hide(self):
        """Hide the top-level widget"""
        
        self._toplevel.hide()
    
    def get_slave(self, parent):
        return self._slaves.get(parent, None)
    
    def attach_slave(self, parent, slave, sync_visibility=True):
        """Add `slave' to the `parent' widget and make the slave visible.
        
        No other slave must already be attached to that parent.
        
        When `sync_visibility' is set to True, the parent's visibility
        is synchronized with the slave's visibiity. Thus, if the slave is
        hidden, the parent will be, too. This is useful if the parent is a
        simple `gtk.EventBox' in a `gtk.HBox' or `gtk.VBox'. If the box
        has a spacing, setting `sync_visibility' to True will prevent
        unnecessary spacings when the slave is hidden.
        """
        
        if not isinstance(slave, SlaveDelegate):
            raise ValueError("Slave %r is not a SlaveDelegate." % slave);
        
        if parent in self._slaves:
            raise ValueError("A slave is already attached to %r." % parent);
        
        self._slaves[parent] = slave
        
        parent.add(slave._toplevel)
        slave.handle_attached(self)
        
        if sync_visibility:
            self._signal_manager.connect(slave.get_toplevel(),
                "notify::visible", self.on_slave_visibility_changed, parent)
        
        slave.show()
    
    def detach_slave(self, parent):
        if parent in self._slaves:
            slave = self._slaves[parent]
            parent.remove(slave.get_toplevel())
            slave.handle_detached(self)
            
            # Does not care whether `sync_visibility' was set or not.
            self._signal_manager.disconnect(slave)
            
            del self._slaves[parent]
    
    def detach_all_slaves(self):
        for parent in dict(self._slaves):
            self.detach_slave(parent)
    
    def get_toplevel(self):
        return self._toplevel
    
    def create_ui(self):
        """
        Create any UI by hand.

        Override to create additional UI here.

        This can contain any instance initialization, so for example mutation of
        the gtk.Builder generated UI, or creating the UI in its entirety.
        """
    
    def on_slave_visibility_changed(self, slave, prop, parent):
        """Apply the visibility of `slave' to `parent'."""
        parent.set_property("visible", slave.get_property("visible"))


class SlaveDelegate(BaseDelegate):
    def get_builder_toplevel(self, builder):
        """
        Get the top-level widget from a gtk.Builder file.
        
        The slave view implementation first searches for the widget named as
        self.top-level_name (which defaults to "main". If this is missing, the
        first top-level widget is discovered in the Builder file, and it's
        immediate child is used as the top-level widget for the delegate.
        """
        
        toplevel = builder.get_object(self.toplevel_name)
        
        if toplevel is None:
            toplevel = self._get_first_builder_window(builder).child
        
        if toplevel is not None:
            toplevel.unparent()
        
        return toplevel
    
    def create_default_toplevel(self):
        return gtk.VBox()
    
    def handle_attached(self, parent_delegate):
        """Called when the slave is attached to a `BaseDelegate'.
        
        The `BaseDelegate' is passed as the only argument.
        Can be overridden by subclasses and doesn't do anything by default.
        """
        
        pass
    
    def handle_detached(self, parent_delegate):
        """Called when the slave is detached from a `BaseDelegate'.
        
        The `BaseDelegate' is passed as the only argument.
        Can be overridden by subclasses and doesn't do anything by default.
        """
        
        pass


class MainDelegate(BaseDelegate):
    def get_builder_toplevel(self, builder):
        """
        Get the top-level widget from a gtk.Builder file.

        The main view implementation first searches for the widget named as
        self.top-level_name (which defaults to "main". If this is missing, or not
        a gtk.Window, the first top-level window found in the gtk.Builder is
        used.
        """
        
        toplevel = builder.get_object(self.toplevel_name)
        
        if not gobject.type_is_a(toplevel, gtk.Window):
            toplevel = None
        
        if toplevel is None:
            toplevel = self._get_first_builder_window(builder)
        
        return toplevel
    
    def create_default_toplevel(self):
        return gtk.Window()


class SlaveDelegateComponent(ConfigurableComponent, SlaveDelegate):
    def __init__(self, component_manager):
        ConfigurableComponent.__init__(self, component_manager)
        SlaveDelegate.__init__(self)
        
        def unload_component(widget, *args):
            self._component_manager.unload(self)
        
        self.get_toplevel().connect_after("delete-event", unload_component)
    
    def on_unload(self):
        """Destroy the top-level widget when the component is unloaded."""
        
        self.detach_all_slaves()
        self.get_toplevel().destroy()


class MainDelegateComponent(ConfigurableComponent, MainDelegate):
    def __init__(self, component_manager):
        ConfigurableComponent.__init__(self, component_manager)
        MainDelegate.__init__(self)

        # Has the descruction of the top-level widget begun are already ended?
        self._destroyed = False

        # Has the `on_unload' methode been called?
        self._on_unload_called = False
        
        if isinstance(self.get_toplevel(), gtk.Dialog):
            self.get_toplevel().connect_after("response", self.on_response)
        
        self.get_toplevel().connect_after("destroy", self.on_destroy)
    
    def on_unload(self):
        """Destroy the top-level widget when the component is unloaded."""
        self._on_unload_called = True
        
        if not self._destroyed:
            self.detach_all_slaves()
            self.get_toplevel().destroy()
    
    def on_response(self, widget, response):
        self._component_manager.unload(self)
    
    def on_destroy(self, widget):
        self._destroyed = True
        
        if not self._on_unload_called:
            self._component_manager.unload(self)
