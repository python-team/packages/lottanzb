# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from logging import getLogger
LOG = getLogger(__name__)

from gobject import idle_add
from threading import Lock
from os.path import isdir, isfile

from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import GObject, gproperty, gsignal
from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.util.regex import (
    IPv4_ADDRESS_PATTERN,
    IPv6_ADDRESS_PATTERN,
    HOST_NAME_PATTERN
)

class EndPoint(object):
    validation_error = None
    
    def attach_to_conduit(self, change_callback):
        raise NotImplementedError
    
    def detach_from_conduit(self):
        raise NotImplementedError
    
    def get_value(self):
        raise NotImplementedError
    
    def set_value(self, value):
        raise NotImplementedError
    
    def set_valid(self):
        self.validation_error = None
    
    def set_invalid(self, error):
        self.validation_error = error


class GObjectEndPoint(EndPoint):
    PROPERTY = ""
    SIGNAL = ""
    
    def __init__(self, _object, prop="", signal=""):
        EndPoint.__init__(self)
        
        self._object = _object
        self._property = prop or self.PROPERTY
        
        if signal or self.SIGNAL:
            self._signal = signal or self.SIGNAL
        elif self._property:
            # That was one nasty bug:
            # The 'notify::' signal will only be emitted properly when the
            # property name contains '-' characters instead of '_'.
            self._signal = "notify::%s" % self._property.replace("_", "-")
        else:
            raise ValueError("%r requires a property or signal name." % self)
        
        self._signal_id = -1
        self._change_callback = None
    
    def attach_to_conduit(self, change_callback):
        self._change_callback = change_callback
        
        if isinstance(self._object, gtk.Widget):
            handler = gtk_lock.locked(self._on_changed)
        else:
            handler = self._on_changed
        
        self._signal_id = self._object.connect(self._signal, handler)
    
    def _on_changed(self, *args):
        self._change_callback(self)
    
    def detach_from_conduit(self):
        if self._signal_id == -1:
            raise ValueError("%r is not attached to a conduit" % self)
        else:
            self._object.disconnect(self._signal_id)
            self._signal_id = -1
            self._change_callback = None
    
    def get_value(self):
        if not self._property:
            raise ValueError("%r needs to override `get_value` or specify a "
                "property." % self)
        
        return self._object.get_property(self._property)
    
    def set_value(self, value):
        if not self._property:
            raise ValueError("%r needs to override `set_value` or specify a "
                "property." % self)
        
        return self._object.set_property(self._property, value)


class ToggleEndPoint(GObjectEndPoint):
    SIGNAL = "toggled"
    PROPERTY = "active"


class LabelEndPoint(GObjectEndPoint):
    PROPERTY = "label"


class EntryEndPoint(GObjectEndPoint):
    PROPERTY = "text"
    
    def set_valid(self):
        GObjectEndPoint.set_valid(self)
        
        self._object.set_property("secondary-icon-stock", None)
        self._object.set_property("secondary-icon-tooltip-markup", None)
    
    def set_invalid(self, error):
        GObjectEndPoint.set_invalid(self, error)
        
        if isinstance(error, ValidationEmptyError):
            stock_id = "gtk-edit"
        else:
            stock_id = "gtk-dialog-error"
        
        self._object.set_property("secondary-icon-stock", stock_id)
        self._object.set_property("secondary-icon-tooltip-text", error.message)


class DelayedEntryEndPoint(EntryEndPoint):
    def __init__(self, widget):
        EntryEndPoint.__init__(self, widget)
        
        self._changed = False
        self._focus_out_signal_id = -1
    
    def attach_to_conduit(self, change_callback):
        EntryEndPoint.attach_to_conduit(self, change_callback)
        
        self._focus_out_signal_id = self._object.connect("focus-out-event",
            self._on_focus_out_event)
    
    def detach_from_conduit(self):
        EntryEndPoint.detach_from_conduit(self)
        
        self._object.disconnect(self._focus_out_signal_id)
    
    def _on_changed(self, *args):
        if self._object.get_property("has-focus"):
            self._changed = True
        else:
            EntryEndPoint._on_changed(self, *args)
    
    def _on_focus_out_event(self, widget, event):
        if self._changed:
            self._on_changed()
            self._changed = False
    
    def set_value(self, value):
        if self._object.get_property("has-focus"):
            return
        
        EntryEndPoint.set_value(self, value)


class PasswordEntryEndPoint(EntryEndPoint):
    def __init__(self, widget):
        self._old_value = None
        
        EntryEndPoint.__init__(self, widget)
    
    def _on_changed(self, *args):
        # Only clear a masked password (e.g. '****') as soon as the user makes
        # the first change to it, rather than when the entry is focused.
        # This prevents the user from accidentally clearing the password by
        # clicking on the entry.
        if self._old_value is None:
            self._old_value = self.get_value()
        elif self._old_value and "*" * len(self._old_value) == self._old_value:
            self._old_value = ""
            
            # This method is called by a handler of the 'notify::text' signal.
            # Since, `set_text' will also cause this signal to be emitted,
            # we defer the call to `set_text' so that the first signal be
            # fully emitted.
            idle_add(self._object.set_text, "")
            return
        
        EntryEndPoint._on_changed(self, *args)


class DelayedPasswordEntryEndPoint(DelayedEntryEndPoint):
    def __init__(self, widget):
        DelayedEntryEndPoint.__init__(self, widget)
        
        self._object.connect("focus-in-event", self.on_focus_in_event)
    
    def on_focus_in_event(self, widget, event):
        value = widget.get_text()
        
        if "*" * len(value) == value:
            widget.set_text("")


class FileChooserFolderEndPoint(GObjectEndPoint):
    SIGNAL = "current-folder-changed"
    
    def get_value(self):
        return self._object.get_current_folder()
    
    def set_value(self, value):
        if isdir(value):
            self._object.set_current_folder(value)


class ToggleActionEndPoint(GObjectEndPoint):
    SIGNAL = "toggled"
    
    def get_value(self):
        return self._object.get_active()
    
    def set_value(self, value):
        self._object.set_active(value)


class FileChooserButtonEndPoint(GObjectEndPoint):
    SIGNAL = "file-set"
    
    def get_value(self):
        return self._object.get_filename()
    
    def set_value(self, value):
        if isfile(value):
            self._object.set_filename(value)


class SpinButtonEndPoint(GObjectEndPoint):
    SIGNAL = "value-changed"
    PROPERTY = "value"


class Conduit(GObject):
    gsignal("changed", object, object)
    
    def __init__(self, model_endpoint, *endpoints):
        GObject.__init__(self)
        
        self._endpoints = []
        self._lock = Lock()
        
        self.add_endpoint(model_endpoint)
        
        for endpoint in endpoints:
            self.add_endpoint(endpoint)
    
    def get_model_endpoint(self):
        if self._endpoints:
            return self._endpoints[0]
    
    def is_model_endpoint(self, endpoint):
        return endpoint is self._endpoints[0]
    
    def publish_model_endpoint(self):
        model_endpoint = self.get_model_endpoint()
        
        if model_endpoint:
            self.on_endpoint_changed(model_endpoint)
    
    def add_endpoint(self, endpoint):
        endpoint.attach_to_conduit(self.on_endpoint_changed)
        
        self._endpoints.append(endpoint)
        
        # If new endpoints are added to the conduit, they must be updated
        # based on the model endpoint.
        # every endpoint except for the model endpoint, which is inefficient
        # in the case of three or more endpoints.
        self.publish_model_endpoint()
    
    def remove_endpoint(self, endpoint):
        self._endpoints.remove(endpoint)
        
        endpoint.detach_from_conduit()
    
    def on_endpoint_changed(self, changed_endpoint):
        if self._lock.acquire(False):
            try:
                value = changed_endpoint.get_value()
                
                self._propagate_change(changed_endpoint, value)
            finally:
                self._lock.release()
    
    def _propagate_change(self, changed_endpoint, value):
        for endpoint in self._endpoints:
            if not endpoint is changed_endpoint:
                endpoint.set_value(value)
        
        self.emit("changed", changed_endpoint, value)
    
    def destroy(self):
        for endpoint in self._endpoints:
            self.remove_endpoint(endpoint)


class ValidationError(Exception):
    def __init__(self, conduit, end_point, value, message=""):
        self.conduit = conduit
        self.end_point = end_point
        self.value = value
        self.message = message or _("Invalid value")
    
    def __str__(self):
        return "Invalid value %r for end point %r in conduit %r." % (
            self.value, self.end_point, self.conduit)


class ValidationEmptyError(ValidationError):
    def __init__(self, conduit, end_point, value, message=""):
        message = message or _("This field is mandatory")
        
        ValidationError.__init__(self, conduit, end_point, value, message)
    
    def __str__(self):
        return "Missing value for end point %r in mandatory conduit %r." % (
            self.end_point, self.conduit)


class ValidationInvalidError(ValidationError):
    pass


class ValidatableConduit(Conduit):
    enable_validation = gproperty(type=bool, default=True)
    mandatory = gproperty(type=bool, default=False)
    is_valid = gproperty(type=bool, default=True)
    
    def _propagate_change(self, changed_endpoint, value):
        validation_error = None
        
        if self.enable_validation:
            try:
                self.validate_endpoint_value(changed_endpoint, value)
            except ValidationError as error:
                LOG.debug(str(error))
                
                validation_error = error
        
        Conduit._propagate_change(self, changed_endpoint, value)
        
        if validation_error:
            self.is_valid = False
            
            for endpoint in self._endpoints:
                endpoint.set_invalid(validation_error)
        elif not self.is_valid:
            self.is_valid = True
            
            for endpoint in self._endpoints:
                endpoint.set_valid()
    
    def validate(self):
        is_valid = True
        
        if self.enable_validation:
            for endpoint in self._endpoints:
                try:
                    self.validate_endpoint_value(endpoint, endpoint.get_value())
                except ValidationError as error:
                    is_valid = False
                    
                    if endpoint is self.get_model_endpoint():
                        for endpoint in self._endpoints:
                            endpoint.set_invalid(error)
                    else:
                        endpoint.set_invalid(error)
                else:
                    endpoint.set_valid()
        
        self.is_valid = is_valid
    
    def validate_endpoint_value(self, endpoint, value):
        if self.mandatory and not value:
            raise ValidationEmptyError(self, endpoint, value)
    
    def set_property(self, key, value):
        changed = Conduit.set_property(self, key, value)
        
        if changed and key in ("mandatory", "enable_validation"):
            self.validate()


class HostConduit(ValidatableConduit):
    PATTERNS = (IPv4_ADDRESS_PATTERN, IPv6_ADDRESS_PATTERN, HOST_NAME_PATTERN)
    
    def validate_endpoint_value(self, endpoint, value):
        ValidatableConduit.validate_endpoint_value(self, endpoint, value)
        
        for pattern in self.PATTERNS:
            if pattern.match(value):
                return
        
        raise ValidationInvalidError(self, endpoint, value,
            _("Invalid IP address or host name"))


class ConduitCollection(GObject):
    def __init__(self, *conduits):
        GObject.__init__(self)
        
        self._conduits = []
        
        for conduit in conduits:
            self.add(conduit)
    
    def add(self, conduit):
        self._conduits.append(conduit)
    
    def remove(self, conduit):
        self._conduits.remove(conduit)
        conduit.destroy()
    
    def remove_all(self):
        for conduit in self._conduits:
            self.remove(conduit)


class ValidatableConduitCollection(ConduitCollection):
    is_valid = gproperty(type=bool, default=True)
    
    def __init__(self, *conduits):
        self._signal_ids = {}
        
        ConduitCollection.__init__(self, *conduits)
    
    def add(self, conduit):
        ConduitCollection.add(self, conduit)
        
        if isinstance(conduit, ValidatableConduit):
            self._signal_ids[conduit] = conduit.connect("notify::is-valid",
                self.on_validation_changed)
            
            if self.is_valid and not conduit.is_valid:
                self.is_valid = False
    
    def remove(self, conduit):
        ConduitCollection.remove(self, conduit)
        
        if isinstance(conduit, ValidatableConduit):
            conduit.disconnect(self._signal_ids.pop(conduit))
            self.on_validation_changed()
    
    def validate(self):
        for conduit in self._conduits:
            if isinstance(conduit, ValidatableConduit):
                conduit.validate()
    
    def on_validation_changed(self, *args):
        is_valid = True
        
        for conduit in self._conduits:
            if isinstance(conduit, ValidatableConduit) and not conduit.is_valid:
                is_valid = False
        
        self.is_valid = is_valid
