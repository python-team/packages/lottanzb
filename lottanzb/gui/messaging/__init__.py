# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""Facility making it possible to display non-critical messages to the user in a
non-obstrusive way.

Messages may be provided only by components implementing `IMessageProvider'.
Messages must be objects of type `SlaveDelegate' and must be hidden (emit the
'hide' signal) after the user has interacted with them.
"""

import gtk

from time import time
from gobject import timeout_add
from threading import Lock

from lottanzb.gui.framework import SlaveDelegateComponent
from lottanzb.gui.messaging.base import IMessageProvider, MessageProvider
from lottanzb.core.component import ExtensionPoint

__all__ = ["MessagingManager"]

# The minimal number of seconds between a message being hidden for an arbitrary
# reason and the following message being displayed.
MESSAGE_DELAY = 1.0

class MessagingManager(SlaveDelegateComponent):
    """The area where messages are being displayed.
    
    All message providers are loaded asynchronously.
    
    Decide what messages to display and when and make sure that the messages are
    displayed in-order and that only one message is visible at any given
    point of time.
    
    If no message is being showed at a given point of time, the messaging area
    is hidden.
    
    The component only performs and work if `gtk.InfoBar' is available.
    """
    
    message_providers = ExtensionPoint(IMessageProvider)
    
    def __init__(self, component_manager):
        SlaveDelegateComponent.__init__(self, component_manager)
        
        # Access to the the following properties should be mutually exclusive.
        self._lock = Lock()
        
        # The point of time when the last message was hidden.
        self._last_message_hidden = -1
        
        # The message currently being displayed.
        self._current_message = None
        
        # The list of messages to be displayed, where the first message in the
        # list will be displayed next.
        self._message_queue = []
        
        # TODO: Could be solved more elegantly, because `gtk.InfoBar' is
        # actually a requirement of `Message' not `MessagingManager'.
        if not hasattr(gtk, "InfoBar"):
            # The class `gtk.InfoBar' is not available.
            return
        
        self._component_manager.connect_async("component-loaded",
            self.on_component_loaded)
        
        # Load all message providers asynchronously.
        for message_provider_cls in self.message_providers:
            self._component_manager.load_async(message_provider_cls)
    
    def create_default_toplevel(self):
        return gtk.EventBox()
    
    def maybe_present_message(self):
        """Display the next message, if it's possible to do so.
        
        For this method to display a new message, there must not be a message
        currently being displayed, `_message_queue' must be non-empty and the
        the amount of time since the last message was hidden must exceed
        `MESSAGE_DELAY'.
        """
        
        with self._lock:
            if self._current_message or not self._message_queue:
                return
            
            if time() - self._last_message_hidden < MESSAGE_DELAY:
                return
            
            self._current_message = self._message_queue.pop()
            self._current_message.get_toplevel().connect("hide",
                self.on_message_hidden, self._current_message)
            self.attach_slave(self.get_toplevel(), self._current_message)
            self.show()
    
    def on_component_loaded(self, component_manager, component):
        """If a new message provider has been loaded, get its list of messages
        and monitor any new messages published by it.
        """
        
        if isinstance(component, MessageProvider):
            with self._lock:
                self._message_queue.extend(component.messages)
            
            component.connect("message-added", self.on_message_added)
            
            self.maybe_present_message()
    
    def on_message_added(self, component, message):
        """Add a newly published message to `_message_queue'."""
        with self._lock:
            if message not in self._message_queue:
                self._message_queue.append(message)
        
        self.maybe_present_message()
    
    def on_message_hidden(self, message_toplevel, message):
        """Handle a message that has been hidden for an arbitrary reason.
        
        After `MESSAGE_DELAY' the class will try to display the next message.
        """
        
        if self._current_message is message:
            with self._lock:
                self.detach_slave(self.get_toplevel())
                self.hide()
                
                self._current_message = None
                self._last_message_hidden = time()
                
                timeout_add(int(MESSAGE_DELAY * 1000),
                    self.on_message_hidden_timeout)
    
    def on_message_hidden_timeout(self):
        self.maybe_present_message()
        
        return False
    
    def on_unload(self):
        """Unload all message providers."""
        for message_provider_cls in self.message_providers:
            self._component_manager.unload(message_provider_cls)
        
        SlaveDelegateComponent.on_unload(self)
