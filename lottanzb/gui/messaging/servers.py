# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from lottanzb.core.environ import _
from lottanzb.core.component import depends_on
from lottanzb.gui.messaging import base
from lottanzb.backend.hubs.config import ConfigHub
from lottanzb.config.sabnzbd.servers import ServerConfig
from lottanzb.gui.prefs.servers import AddServerDialog

class MessageProvider(base.MessageProvider):
    depends_on(ConfigHub)
    
    abstract = False
    
    def __init__(self, component_manager):
        base.MessageProvider.__init__(self, component_manager)
        
        config_hub = self._component_manager.get(ConfigHub)
        server_section = config_hub.config.servers
        
        if not len(server_section):
            message = NoServerMessage(server_section)
            
            self.publish_message(message)


class NoServerMessage(base.Message):
    def __init__(self, server_section):
        self._server_section = server_section
        
        base.Message.__init__(self, gtk.MESSAGE_WARNING)
        
        server_section.connect("property-changed",
            self.on_server_section_changed)
    
    def create_ui(self):
        icon = self.create_stock_icon(gtk.STOCK_DIALOG_WARNING)
        label = self.create_multiline_label(
            _("No news server has been specified."),
            _("At least one server is required for downloading."))
        
        self._content_area.pack_start(icon, expand=False)
        self._content_area.pack_start(label)
        
        self.add_button(_("_Add Server"), gtk.RESPONSE_OK)
    
    def on_server_section_changed(self, server_section, section, key, value):
        if server_section is section:
            if isinstance(value, ServerConfig):
                self.hide()
    
    def on_widget__response(self, wigdet, response):
        if response == gtk.RESPONSE_OK:
            dialog = AddServerDialog(self._server_section)
            dialog.show()
