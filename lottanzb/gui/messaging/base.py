# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""Base classes to be used for implementing message providers.

Message providers are recommended to inherit from `MessageProvider', while
messages are recommended to be objects of a type inheriting from `Message'.
"""

import gtk

from lottanzb.core.component import Interface, implements
from lottanzb.util.gobject_extras import gsignal
from lottanzb.config.lotta import ConfigurableComponent
from lottanzb.gui.framework import SlaveDelegate

__all__ = ["IMessageProvider", "MessageProvider", "Message"]

class IMessageProvider(Interface):
    """Besides the `messages' property, components implementing
    `IMessageProvider' must also provide a signal called 'message-added' with a
    single argument providing the message in question.
    """
    
    messages = []


class MessageProvider(ConfigurableComponent):
    """Abstract configurabe component implementing `IMessageProvider'."""
    implements(IMessageProvider)
    gsignal("message-added", object)
    
    abstract = True
    
    def __init__(self, component_manager):
        ConfigurableComponent.__init__(self, component_manager)
        
        self._messages = []
    
    def publish_message(self, message):
        """Append `message' to `_messages' and emit the 'message-added' signal.
        """
        
        self._messages.append(message)
        self.emit("message-added", message)
    
    @property
    def messages(self):
        """A copy of the internal list of messages."""
        return self._messages[:]


class Message(SlaveDelegate):
    """`SlaveDelegate' whose toplevel widget is a `gtk.InfoBar'.
    
    The message type to be used by be specified by passing an appropriate
    `gtk.MESSAGE_*' constant to the constructor. It defaults to
    `gtk.MESSAGE_INFO'.
    """
    
    def __init__(self, message_type=gtk.MESSAGE_INFO):
        if not hasattr(gtk, "InfoBar"):
            raise EnvironmentError("The class 'gtk.InfoBar' is not available.")
        
        self.message_type = message_type
        
        SlaveDelegate.__init__(self)
    
    @property
    def _content_area(self):
        """The content area of the info bar."""
        return self.get_toplevel().get_content_area()
    
    @property
    def _action_area(self):
        """The action area of the info bar."""
        return self.get_toplevel().get_action_area()
    
    def add_button(self, button_text, response_id):
        """Add a button with the given text and set things up so that clicking
        the button will emit the 'response' signal with thegiven `response_id'.
        """
        
        return self.get_toplevel().add_button(button_text, response_id)
    
    def create_default_toplevel(self):
        info_bar = gtk.InfoBar()
        info_bar.set_message_type(self.message_type)
        
        # Center the buttons in the action area.
        info_bar.get_action_area().set_property("layout-style",
            gtk.BUTTONBOX_EDGE)
        
        return info_bar
    
    @classmethod
    def create_label(cls, markup=""):
        """Create an object of type `gtk.Label' whose markup is set to `markup'.
        """
        
        label = gtk.Label()
        label.set_line_wrap(True)
        label.set_selectable(True)
        label.set_markup(markup)
        label.set_alignment(0.0, 0.5)
        label.show()
        
        return label
    
    @classmethod
    def create_multiline_label(cls, primary_markup, secondary_markup=""):
        """Create an object of type `gtk.VBox' that contains two labels, where
        the one above has the bold-faced markup `primary_markup' and the one
        below has the small-sized markup `secondary_markup'.
        """
        
        vbox = gtk.VBox(spacing=6)
        vbox.show()
        
        primary_label = cls.create_label(
            "<b>{0}</b>".format(primary_markup))
        vbox.pack_start(primary_label, expand=False)
        
        if secondary_markup:
            secondary_label = cls.create_label(
                "<small>{0}</small>".format(secondary_markup))
            vbox.pack_start(secondary_label, expand=False)
        
        return vbox
    
    @classmethod
    def create_stock_icon(cls, stock_id):
        """Create a visible stock icon given `stock_id'."""
        image = gtk.Image()
        image.set_from_stock(stock_id, gtk.ICON_SIZE_DIALOG)
        image.show()
        
        return image
