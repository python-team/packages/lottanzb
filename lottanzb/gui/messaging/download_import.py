# Copyright (C) 2010-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

from glob import glob
from os.path import join
from gettext import ngettext

from lottanzb.core.component import depends_on
from lottanzb.core.environ import _
from lottanzb.util.gobject_extras import GObject, gproperty, gsignal
from lottanzb.util.threading_extras import Thread
from lottanzb.config import hellanzb
from lottanzb.config.section import ConfigSection
from lottanzb.config.errors import LoadingError
from lottanzb.gui.messaging import base
from lottanzb.backend.sessions.local import HELLANZB_CONFIG_FILE
from lottanzb.backend.hubs.general import GeneralHub

"""Facilities for importing the NZB files of unfinished HellaNZB downloads.

The module will only consider the HellaNZB directories listed in the HellaNZB
configuration file managed by previous versions of LottaNZB. Both NZBs of
queued and active downloads will be proposed for import using a message
in the main window.

The original NZB files will not be deleted when successfully imported, but
the module remembers that an import has occurred and won't ask once more.

Note that the module depends on GTK 2.18 because its usage of `gtk.InfoBar'.
If an older version of GTK is being used, no message will be displayed.
"""

__all__ = ["MessageProvider", "Message"]


class Action:
    """Enumerations of actions that can be performed by the user.
    
    - `NONE': The user hasn't taken any action.
    - `CANCELED': The user has canceled a proposed import.
    - `IMPORTED': The user has performed a proposed import.
    """
    
    NONE, CANCELED, IMPORTED = range(0, 3)


class Config(ConfigSection):
    """Store information used by this module across sessions."""
    
    last_action = gproperty(
        nick="The last action performed by the user",
        type=int,
        default=Action.NONE)


class MessageProvider(base.MessageProvider):
    """When loading the component, an instance of `Message' is published only if
    NZB files could be found and no other import has previously been performed
    or canceled by the user.
    """
    
    depends_on(GeneralHub)
    abstract = False
    
    def __init__(self, component_manager):
        base.MessageProvider.__init__(self, component_manager)
        
        if self.config.last_action == Action.CANCELED:
            # A previously proposed import has been completed.
            return
        
        if self.config.last_action == Action.IMPORTED:
            # A previously proposed import has been performed.
            return
        
        nzb_files = self.get_hellanzb_nzb_files()
        
        if not nzb_files:
            # No importable NZB files found.
            return
        
        general_hub = self._component_manager.get(GeneralHub)
        message = Message(nzb_files, general_hub)
        message.connect("action", self.on_message_action)
        
        self.publish_message(message)
    
    def on_message_action(self, message, action):
        """Remember the action performed by the user."""
        self.config.last_action = action
    
    @staticmethod
    def get_hellanzb_nzb_files():
        """Return a list of all NZB files corresponding to unfinished HellaNZB
        downloads.
        
        Both the directories `current_dir' and `queue_dir' specified in
        HellaNZB's configuration are searched.
        """
        
        config = hellanzb.Config(HELLANZB_CONFIG_FILE)
        result = []
        
        try:
            config.load()
        except LoadingError:
            return
        
        for nzb_directory in [config.current_dir, config.queue_dir]:
            if isinstance(nzb_directory, str):
                result += glob(join(nzb_directory, "*.nzb"))
        
        return result


class Message(base.Message, GObject):
    """The message letting the user interact with the import functionality.
    
    The 'action' signal is emitted when the user has clicked one of the two
    buttons 'Cancel' or 'Import'. Its only argument is a constant of `Action'.
    """
    
    gsignal("action", int)
    
    def __init__(self, nzb_files, general_hub):
        self._nzb_files = nzb_files
        
        self._batch_thread = BatchNZBAdditionThread(general_hub, nzb_files[:])
        self._batch_thread.connect_async("progress",
            self.on_batch_thread_progress)
        self._batch_thread.connect_async("completed",
            self.on_batch_thread_completed)
        
        base.Message.__init__(self, gtk.MESSAGE_QUESTION)
        GObject.__init__(self)
    
    def create_ui(self):
        """Create all parts of the UI and connect to the appropriate signals."""
        
        icon = self.create_stock_icon(gtk.STOCK_DIALOG_QUESTION)
        
        self._intro_view = self.create_multiline_label(
            ngettext(
                "Import %i incomplete download?",
                "Import %i incomplete downloads?",
                len(self._nzb_files)) % len(self._nzb_files),
            ngettext(
                "It was started using a previous version of the program.",
                "They were started using a previous versions of the program.",
                len(self._nzb_files)) + " " + \
            _("Any progress will be lost."))
        
        self._progress_view = self.create_multiline_label(
            ngettext(
            "Importing %i download...",
            "Importing %i downloads...",
            len(self._nzb_files)) % len(self._nzb_files))
        self._progress_view.hide()
        
        self._progress_bar = gtk.ProgressBar()
        self._progress_bar.show()
        
        self._progress_view.pack_start(self._progress_bar, expand=False)
        
        self._content_area.pack_start(icon, expand=False)
        self._content_area.pack_start(self._intro_view)
        self._content_area.pack_start(self._progress_view)
        
        self._import_button = self.add_button(_("_Import"), gtk.RESPONSE_OK)
        self._cancel_button = self.add_button(_("_Cancel"), gtk.RESPONSE_CANCEL)
    
    def on_widget__response(self, widget, response):
        """Perform the appropriate action when one of the buttons is clicked.
        
        If the 'Cancel' button is clicked, stop any active import and hide
        the message.
        
        If the 'Import' button is clicked, instantiate `batch_thread' with a
        `BatchNZBAdditionThread' given the general hub and the list of NZB
        files. The 'Import' button is made insensitive, so that the user cannot
        initiate more than one import at once.
        """
        
        if response == gtk.RESPONSE_OK:
            self._import_button.set_property("sensitive", False)
            self._intro_view.hide()
            self._progress_view.show_all()
            self._batch_thread.start()
        elif response == gtk.RESPONSE_CANCEL:
            if self._batch_thread.is_alive():
                self._batch_thread.stop()
                self._batch_thread.join()
            
            self.emit("action", Action.CANCELED)
            self.hide()
    
    def on_batch_thread_completed(self, thread):
        """Hide the message upon completion of the import and remember it."""
        
        self.emit("action", Action.IMPORTED)
        self.hide()
    
    def on_batch_thread_progress(self, thread, progress):
        """Update the progress bar after another NZB file has been imported."""
        
        self._progress_bar.set_fraction(progress)


class BatchNZBAdditionThread(Thread):
    """Adds all NZB files in a list to the SABnzbd instance associated with a
    given general hub.
    
    It's possible to stop the import by calling the method `stop'.
    """
    
    def __init__(self, general_hub, nzb_files):
        self.general_hub = general_hub
        self.nzb_files = nzb_files
        
        Thread.__init__(self)
    
    def run(self):
        for index, nzb_file in enumerate(self.nzb_files):
            if self.thread_stopped.isSet():
                return
            
            # TODO: Ugly as the returned thread can either be a `Query' or a
            # `QueryWaitingThread'.
            thread = self.general_hub.add_file(nzb_file)
            thread.join()
            
            self.emit("progress", float(index + 1) / len(self.nzb_files))
        
        self.emit("completed")
