# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import gtk

import logging
LOG = logging.getLogger(__name__)

from os.path import isfile

from lottanzb.core.environ import _
from lottanzb.core.component import depends_on
from lottanzb.util.gobject_extras import gproperty
from lottanzb.config import ConfigSection
from lottanzb.gui.help import open_help
from lottanzb.gui.framework import MainDelegateComponent
from lottanzb.gui.framework.proxy import (
    Conduit, GObjectEndPoint, FileChooserFolderEndPoint)
from lottanzb.backend.hubs.general import GeneralHub

class Config(ConfigSection):
    last_location = gproperty(type=str)


class Dialog(MainDelegateComponent):
    depends_on(GeneralHub)
    
    builder_file = "add_file_dialog"
    
    def create_ui(self):
        file_filter = self.get_toplevel().get_filter()
        file_filter.set_name(_("NZB files and archives"))
        
        lowercase_patterns = ["*.nzb", "*.zip", "*.rar", "*.gz"]
        uppercase_patterns = [pattern.upper() for pattern in lowercase_patterns]
        
        for pattern in lowercase_patterns + uppercase_patterns:
            file_filter.add_pattern(pattern)
        
        Conduit(
            GObjectEndPoint(self.config, "last_location"),
            FileChooserFolderEndPoint(self.get_toplevel()))
    
    def enqueue_selection(self):
        files = self.get_toplevel().get_filenames()
        
        for a_file in [a_file for a_file in files if isfile(a_file)]:
            general_hub = self._component_manager.get(GeneralHub)
            general_hub.add_file(a_file)
    
    def on_add_file_dialog__response(self, dialog, response):
        if response == gtk.RESPONSE_OK:
            self.enqueue_selection()
        elif response == gtk.RESPONSE_HELP:
            open_help("adding-downloads")
            dialog.stop_emission("response")
