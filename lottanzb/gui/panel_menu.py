# Copyright (C) 2009-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""Display a menu in the panel (accessible through an icon) that provides
shortcuts for frequently-used features of the application.

The module both supports the old-fashioned `gtk.StatusIcon' as well as the
concept of application indicators introduced in Ubuntu 10.04. If the module
`appindicator' can be imported, the latter will automatically be used.
"""

import gtk

from lottanzb.core.environ import _
from lottanzb.core.component import (Component, depends_on, implements,
    ExtensionPoint, Interface)
from lottanzb.util.gtk_extras import gtk_lock
from lottanzb.gui.main import MainWindow
from lottanzb.gui.framework.proxy import Conduit, EndPoint, ToggleEndPoint

try:
    import appindicator
except ImportError:
    appindicator = None

class IMenuContainer(Interface):
    """Interface for classes that can set up a menu in the panel."""
    
    def set_menu(self, menu):
        pass
    
    def set_visible(self, visible):
        pass


class PanelMenuComponent(Component):
    """Build the panel menu and instantiate a suitable `IMenuContainer'.
    
    The menu items are associated with `gtk.Actions' provided by `MainWindow'
    and are thus automatically kept in sync in terms of appearance.
    
    A menu item 'Close' is added to the 'File' menu of the main window, allowing
    the user to close the window without quitting the application. Additionally,
    closing the main window using the window button only hides it.
    """
    
    depends_on(MainWindow)
    
    menu_containers = ExtensionPoint(IMenuContainer)
    
    # The `gtk.Menu' used as the panel menu.
    menu = None
    
    # Contains a `MenuContainer` object that represents the actual icon in
    # the panel.
    menu_container = None
    
    def __init__(self, component_manager):
        Component.__init__(self, component_manager)
        
        main_window = self._component_manager.get(MainWindow)
        main_window.quit_on_delete = False
        
        with gtk_lock:
            self.close_menu_item = gtk.ImageMenuItem(gtk.STOCK_CLOSE)
            self.close_menu_item.connect("activate",
                gtk_lock.locked(self.on_close_activate))
            self.close_menu_item.show()
            
            accel_group = gtk.AccelGroup()
            main_window.get_toplevel().add_accel_group(accel_group)
            key, mod = gtk.accelerator_parse("<ctrl>W")
            
            self.close_menu_item.add_accelerator("activate", accel_group, key,
                mod, gtk.ACCEL_VISIBLE)
            
            file_menu = main_window.file_menu
            quit_index = file_menu.get_children().index(main_window.menu_quit)
            file_menu.insert(self.close_menu_item, quit_index)
            
            self.visibility_menu_item = gtk.CheckMenuItem(_("Show LottaNZB"))
            self.visibility_conduit = Conduit(
                MainWindowVisibilityEndPoint(main_window.get_toplevel()),
                ToggleEndPoint(self.visibility_menu_item))
            
            self.menu = gtk.Menu()
            self.menu.append(self.visibility_menu_item)
            self.menu.append(gtk.SeparatorMenuItem())
            self.menu.append(main_window.add.create_menu_item())
            self.menu.append(main_window.add_url.create_menu_item())
            self.menu.append(gtk.SeparatorMenuItem())
            self.menu.append(main_window.paused.create_menu_item())
            self.menu.append(main_window.edit_preferences.create_menu_item())
            self.menu.append(gtk.SeparatorMenuItem())
            self.menu.append(main_window.quit.create_menu_item())
            self.menu.show_all()
        
        # Choose the right `MenuContainer'.
        for cls in self.menu_containers:
            try:
                self.menu_container = self._component_manager.load(cls)
            except:
                pass
            else:
                self.menu_container.set_menu(self.menu)
                self.menu_container.set_visible(True)
                
                break
    
    def on_close_activate(self, menu_item):
        """Hide the main window when the 'Close' menu item is activated."""
        main_window = self._component_manager.get(MainWindow)
        main_window.hide()


class MainWindowVisibilityEndPoint(EndPoint):
    """Keep the visibility of the main window in sync with the boolean
    visibility value of a `Conduit'.
    
    Send False to the conduit if the window is hidden or minimized.
    If True is received from the conduit, unminimize the window if it was
    previously minimized and present the window if it was previously hidden.
    """
    
    def __init__(self, main_window):
        EndPoint.__init__(self)
        
        # Simple property that indicates whether the main window is minimized
        # to the window list.
        # It's the cached value from the previous 'window-state-event' signal
        # emitted by the main window as it cannot be accessed directly.
        self._minimized = False
        
        self._change_callback = None
        
        self._main_window = main_window
        
        with gtk_lock:
            self._main_window.connect("notify::visible",
                gtk_lock.locked(self.on_visibility_changed))
            self._main_window.connect("window-state-event",
                gtk_lock.locked(self.on_state_event))
    
    def attach_to_conduit(self, change_callback):
        self._change_callback = change_callback
    
    def detach_from_conduit(self):
        self._change_callback = None
    
    def get_value(self):
        with gtk_lock:
            return self._main_window.get_property("visible") and \
                not self._minimized
    
    def set_value(self, value):
        with gtk_lock:
            if value:
                if self._minimized:
                    self._main_window.deiconify()
                else:
                    self._main_window.present()
            else:
                self._main_window.hide()
    
    def on_visibility_changed(self, main_window, prop):
        if callable(self._change_callback):
            self._change_callback(self)
    
    def on_state_event(self, main_window, event):
        iconified_state = gtk.gdk.WINDOW_STATE_ICONIFIED
        run_callback = False
        
        with gtk_lock:
            if event.changed_mask & iconified_state:
                self._minimized = bool(event.new_window_state & \
                    iconified_state)
                run_callback = True
        
        if run_callback and callable(self._change_callback):
            self._change_callback(self)


class MenuContainer(Component):
    APPLICATION = "lottanzb"
    
    abstract = True
    implements(IMenuContainer)
    
    def set_menu(self, menu):
        raise NotImplementedError
    
    def set_visible(self, visible):
        raise NotImplementedError
    
    def on_unload(self):
        self.set_visible(False)


class IndicatorMenuContainer(MenuContainer):
    abstract = False
    
    def __init__(self, component_manager):
        MenuContainer.__init__(self, component_manager)
        
        self._indicator = appindicator.Indicator(self.APPLICATION,
            self.APPLICATION, appindicator.CATEGORY_APPLICATION_STATUS)
        
        self.set_visible(False)
   
    def set_menu(self, menu):
        self._indicator.set_menu(menu)
   
    def set_visible(self, visible):
        if visible:
            status = appindicator.STATUS_ACTIVE
        else:
            status = appindicator.STATUS_PASSIVE
        
        self._indicator.set_status(status)


class StatusIconMenuContainer(MenuContainer):
    abstract = False
    
    def __init__(self, component_manager):
        MenuContainer.__init__(self, component_manager)
        
        self._menu = None
        
        with gtk_lock:
            self._icon = gtk.StatusIcon()
            self._icon.set_from_icon_name(self.APPLICATION)
            
            self._icon.connect("popup-menu",
                gtk_lock.locked(self.on_icon_popup_menu))
            self._icon.connect("activate",
                gtk_lock.locked(self.on_icon_activate))
        
        self.set_visible(False)
    
    def set_menu(self, menu):
        self._menu = menu
    
    def set_visible(self, visible):
        with gtk_lock:
            self._icon.set_visible(visible)
    
    def on_icon_popup_menu(self, icon, button, activate_time):
        """Display the status icon menu."""
        
        if self._menu:
            with gtk_lock:
                self._menu.popup(None, None, gtk.status_icon_position_menu,
                    button, activate_time, icon)
    
    def on_icon_activate(self, icon):
        if self._menu:
            with gtk_lock:
                menu_item = self._menu.get_children()[0]
                menu_item.set_active(not menu_item.get_active())
