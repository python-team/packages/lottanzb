# Copyright (C) 2008-2009 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import re
import gtk

from lottanzb.core.constants import Priority
from lottanzb.core.environ import _
from lottanzb.core.component import depends_on
from lottanzb.util.misc import open_folder
from lottanzb.util.signalmanager import SignalManager
from lottanzb.backend import Backend
from lottanzb.backend.hubs.general import GeneralHub
from lottanzb.backend.hubs.general.download import Status, StatusGroup
from lottanzb.gui.framework import MainDelegateComponent

class Dialog(MainDelegateComponent):
    depends_on(GeneralHub)
    depends_on(Backend)
    
    builder_file = "properties_dialog"
    
    PRIORITIES = [Priority.TOP, Priority.HIGH, Priority.NORMAL, Priority.LOW]
    
    def __init__(self, component_manager):
        self.download = None
        
        self._signal_manager = SignalManager()
        
        # Holds the objects of type `ActivityField' bound to a certain download
        # set using `set_download'.
        self._activity_fields = []
        
        MainDelegateComponent.__init__(self, component_manager)
    
    def create_ui(self):
        for priority in self.PRIORITIES:
            self.priority_store.append((Priority.to_pretty_string(priority), ))
    
    def set_download(self, download):
        self.download = download
        
        self._signal_manager.disconnect_all()
        
        # Remove all existing activity fields.
        for activity_field in self._activity_fields:
            self.activity_table.remove(activity_field.label)
            self.activity_table.remove(activity_field.content)
            self.label_size_group.remove_widget(activity_field.label)
            
            activity_field.destroy()
        
        # Instantiate new activity fields, bound to `download'.
        self._activity_fields = [
            StatusField(download),
            FormattedActivityField(download, _("Downloaded"),
                "{0.size_downloaded} / {0.size} ({0.percentage}%)",
                StatusGroup.NOT_FULLY_LOADED),
            FormattedActivityField(download, _("Size"),
                "{0.size}", StatusGroup.FULLY_LOADED),
            FormattedActivityField(download, _("Time left"),
                "{0.time_left.short}", Status.DOWNLOADING),
            FormattedActivityField(download, _("ETA"),
                "{0.eta.full}", Status.DOWNLOADING),
            FormattedActivityField(download, _("Age"),
                "{0.average_age.short}", StatusGroup.NOT_FULLY_LOADED),
            FormattedActivityField(download, _("Download time"),
                "{0.download_time.short}", StatusGroup.FULLY_LOADED),
            FormattedActivityField(download, _("Post-processing"),
                "{0.postprocessing_time.short}", StatusGroup.COMPLETE),
            FormattedActivityField(download, _("Completed"),
                "{0.completed.full}", StatusGroup.COMPLETE),
            ErrorMessageField(download)]
        
        self.activity_table.resize(len(self._activity_fields), 2)
        
        # Add all activity fields to the activity table
        for index, activity_field in enumerate(self._activity_fields):
            self.activity_table.attach(activity_field.label, 0, 1, index,
                index + 1, xoptions=gtk.FILL, ypadding=3)
            self.activity_table.attach(activity_field.content, 1, 2, index,
                index + 1, ypadding=3)
            
            self.label_size_group.add_widget(activity_field.label)
        
        self._signal_manager.connect(self.download, "notify::status",
            self.on_download_status_changed)
        
        self.on_download_status_changed(self.download)
        
        # Change download name seamlessly
        self.name.set_text(self.download.name)
        self._signal_manager.connect(self.name, "changed", self.update_name)
        
        # Change download priority seamlessly
        self.priority.set_active(self.PRIORITIES.index(self.download.priority))
        self._signal_manager.connect(self.priority, "changed",
            self.update_priority)
    
    def update_name(self, *args):
        if self.name.get_text() != "":
            new_name = self.name.get_text()
            
            general_hub = self._component_manager.get(GeneralHub)
            general_hub.rename_download(self.download, new_name)
    
    def update_priority(self, *args):
        new_priority = self.PRIORITIES[self.priority.get_active()]
        general_hub = self._component_manager.get(GeneralHub)
        general_hub.set_priority(new_priority, self.download)
    
    def on_download_status_changed(self, download, *args):
        backend = self._component_manager.get(Backend)
        is_local = backend.interface.connection_info.is_local
        fully_loaded = download.has_status(StatusGroup.FULLY_LOADED)
        
        self.open_button.set_property("visible", is_local and fully_loaded)
        self.name.set_sensitive(not fully_loaded)
        self.priority.set_sensitive(not fully_loaded)
    
    def on_unload(self):
        self._signal_manager.disconnect_all()
        
        MainDelegateComponent.on_unload(self)
    
    def on_open_download_dir__activate(self, widget):
        open_folder(self.download.storage_path)


class ActivityField:
    """A certain entry in the 'Activity' section of the dialog.
    
    It's bound to a certain download and only exists as long as that particular
    download is being displayed in the dialog. The activity field will take care
    of updating its information when the download information changes.
    
    It will be visible iff the download has the status passed as the
    `visibility' argument.
    """
    
    def __init__(self, download, label, visibility=StatusGroup.ANY_STATUS):
        self._label = gtk.Label(label + ":")
        self._label.set_alignment(0.0, 0.0)
        
        self.download = download
        self.visibility = visibility
        
        self.download.connect("notify::status", self.on_download_status_changed)
        self.download.notify("status")
    
    def on_download_status_changed(self, download, prop):
        """Update the visibility of the activity field."""
        visible = download.has_status(self.visibility)
        
        self.label.set_property("visible", visible)
        self.content.set_property("visible", visible)
    
    @property
    def label(self):
        """The `gtk.Label' to be displayed on the left of the activity field."""
        return self._label
    
    @property
    def content(self):
        """The `gtk.Widget' to be displayed on the right of the activity field.
        
        To be implemented by subclasses.
        """
        
        raise NotImplementedError
    
    def destroy(self):
        """Called when the activity field is no longer in use."""
        pass


class SimpleActivityField(ActivityField):
    """Abstract activity field whose content is a simple `gtk.Label'.
    
    It features a facility for automatically observing certain properties of
    the download for changes. After such a change `update' is called, which
    is meant to be implemented by subclasses.
    """
    
    def __init__(self, download, label, visibility=StatusGroup.ANY_STATUS):
        self._create_content()
        self._signal_manager = SignalManager()
        
        ActivityField.__init__(self, download, label, visibility)
        
        # Observe the download for changes to the properties specified in
        # `required_download_properties'.
        for prop in self.required_download_properties:
            self._signal_manager.connect(download, "notify::" + \
                prop.replace("_", "-"), self.on_download_changed)
        
        # Initialize the content.
        self.update()
    
    def _create_content(self):
        """Creates the `gtk.Label' that serves as the content."""
        self._content = gtk.Label()
        self._content.set_alignment(0.0, 0.0)
        self._content.set_line_wrap(True)
    
    @property
    def content(self):
        """The widget to be displayed on the right of the activity field."""
        return self._content
    
    @property
    def required_download_properties(self):
        """The names of the properties of the download to be observed for
        changes. When such a property is changed, `update' will be called.
        
        To be implemented by subclasses. Simply returns an empty list.
        """
        
        return []
    
    def update(self):
        """Fill `content' with information provided by the download.
        
        To be implemented by subclasses.
        """
        
        raise NotImplementedError
    
    def destroy(self):
        """Called when the activity field is no longer in use."""
        self._signal_manager.disconnect_all()
        
        ActivityField.destroy(self)
    
    def on_download_changed(self, download, prop):
        self.update()


class FormattedActivityField(SimpleActivityField):
    """Activity field whose content is generated using a format string on
    which the download is applied. The download will thereby be the first
    and only formatting argument.
    
    Example: "<b>{0.error_message}</b>"
    
    All properties referenced in the content will automatically be observed
    for changes.
    """
    
    PROPERTY_PATTERN = re.compile("{0\.([a-zA-Z0-9_]*)")
    
    def __init__(self, download, label, format_string,
        visibility=StatusGroup.ANY_STATUS):
        self.format_string = format_string
        
        SimpleActivityField.__init__(self, download, label, visibility)
    
    def update(self):
        """The text of `content' is generated by formatting `self.format_string'
        using `self.download'.
        
        If a referenced property doesn't exist, the whole string will be
        replaced with "Unknown".
        """
        
        try:
            text = self.format_string.format(self.download)
        except AttributeError:
            text = _("Unknown")
        
        self.content.set_text(text)
    
    @property
    def required_download_properties(self):
        return self.PROPERTY_PATTERN.findall(self.format_string)


class StatusField(SimpleActivityField):
    """The 'Status' activity field."""
    
    def __init__(self, download):
        SimpleActivityField.__init__(self, download, _("Status"))
    
    def update(self):
        mapping = {
            Status.VERIFYING: self.download.verification_percentage,
            Status.REPAIRING: self.download.repair_percentage,
            Status.EXTRACTING: self.download.unpack_percentage
        }
        
        text = Status.to_pretty_string(self.download.status)
        
        if self.download.status in mapping:
            percentage = mapping[self.download.status]
            text += " ({0}%)".format(percentage)
        
        self.content.set_text(text)
    
    @property
    def required_download_properties(self):
        return ["status", "verification_percentage", "repair_percentage",
            "unpack_percentage"]


class ErrorMessageField(SimpleActivityField):
    """The "Error" activity field."""
    
    def __init__(self, download):
        SimpleActivityField.__init__(self, download, _("Error"), Status.FAILED)
    
    def _create_content(self):
        text_view = gtk.TextView()
        text_view.set_wrap_mode(gtk.WRAP_WORD)
        text_view.set_editable(False)
        
        self._content = gtk.ScrolledWindow()
        self._content.add(text_view)
        self._content.show_all()
        self._content.set_policy(gtk.POLICY_NEVER, gtk.POLICY_NEVER)
        self._content.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        
        self._buffer = text_view.get_buffer()
    
    def update(self):
        message = self.download.error_message
        
        self._buffer.set_text(message)
    
    @property
    def required_download_properties(self):
        return ["error-message"]
