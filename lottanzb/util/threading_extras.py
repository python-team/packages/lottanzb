# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

import threading

from threading import Event, Thread as _Thread

from lottanzb.core.component import Component
from lottanzb.util.gobject_extras import GObject, gsignal

__all__ = ["Thread", "ThreadingComponent"]

class Thread(_Thread, GObject):
    """
    Cancellable thread which uses gobject signals to return information to
    the GUI.
    """
    
    gsignal("completed")
    gsignal("success")
    gsignal("failure", object)
    
    gsignal("progress", float)
    
    def __init__(self):
        _Thread.__init__(self)
        GObject.__init__(self)
        
        # As we can't handle exceptions directly within the thread, they are
        # stored in this property.
        self.error = None
        
        self.thread_stopped = Event()
        self.connect("completed", self.on_thread_completed)
    
    def on_thread_completed(self, thread):
        if thread.error:
            self.emit("failure", thread.error)
        else:
            self.emit("success")
    
    def stop(self):
        """
        Threads in Python are not cancellable, so we implement our own
        cancellation logic.
        """
        
        self.thread_stopped.set()
    
    def run(self):
        """
        Method representing the thread's activity. Must be defined by
        subclasses.
        
        Example code:
        while not self.thread_stopped.isSet():
            ...
            self.emit("progress", x)
        self.emit("completed")
        """
        
        raise NotImplementedError

class ThreadingComponent(Component):
    @property
    def active_threads(self):
        return threading.enumerate()
    
    def stop_all_threads(self, block=False):
        for thread in self.active_threads:
            try:
                thread.stop()
            except:
                pass
        
        if block:
            self.wait_for_all_threads()
    
    def wait_for_all_threads(self):
        for thread in self.active_threads:
            try:
                if thread is not threading.currentThread():
                    thread.join()
            except:
                pass
    
    def unload(self):
        self.stop_all_threads(True)
