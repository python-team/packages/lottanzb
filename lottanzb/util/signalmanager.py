# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from threading import Lock

from lottanzb.util.gobject_extras import GObject

__all__ = ["SignalManager", "SwitchableSignalManager",
    "SignalSynchronicityProvider"]

class SignalManager(dict):
    """
    Manages the signal handlers used in GObject's signal system.
    
    It makes it easier to disconnect one or several handlers without because
    it's not necessary anymore to remember signal IDs, since `SignalManager`
    takes care of this.
    
    The object that wants to observe other objects should create a
    `SignalManager` and use it to connect to a signal of another object
    instead of directly invoking the other object's `connect` method.
    """
    
    def __init__(self):
        """
        Doesn't accept any arguments and hides the fact that `SignalManager` is
        actually a dictionary.
        """
        
        dict.__init__(self)
    
    def connect(self, an_object, signal, handler, *args):
        """
        Use this method to observe other objects instead of directly invoking
        the other object's `connect` method.
        """
        
        handlers = self.setdefault(an_object, {}).setdefault(signal, {})
        
        if handlers.get(handler, None) is None:
            handlers[handler] = an_object.connect(signal, handler, *args)
    
    def disconnect_all(self):
        """Disconnects all handlers from their corresponding signal."""
        
        for an_object in self.keys():
            self.disconnect(an_object)
    
    def disconnect(self, an_object, signal=""):
        """
        Disconnects a handlers associated with a certain object.
        
        It's also possible to limit the disconnection of handlers to a certain
        signal of that particular object.
        """
        
        signals = self.setdefault(an_object, {})
        
        if signal:
            signals = {
                signal: signals.get(signal, {})
            }
        
        for handlers in signals.values():
            for handler, handler_id in handlers.items():
                if handler_id is not None:
                    an_object.disconnect(handler_id)
                    # Don't remove any namespaces (this is on purpose).
                    handlers[handler] = None

class SwitchableSignalManager(SignalManager):
    """
    Disconnect and reconnect all events at once.
    
    All handlers managed by this signal managers will only be connected to
    their corresponding objects if the `enable` method has been called. You
    can disconnect all handlers by invoking the `disable` method. Calling
    the `connect` method will store the handler, but only connect it if the
    the signal manager is `enabled`.
    """
    
    enabled = False
    
    def enable(self):
        """
        Connect all registered handlers to their corresponding signals.
        """
        
        if not self.enabled:
            self.enabled = True
            self.reconnect_all()
    
    def disable(self):
        """
        Disconnect all registered handlers from their corresponding signals.
        
        All handlers will be remembered.
        """
        
        if self.enabled:
            self.enabled = False
            self.disconnect_all()
    
    def connect(self, an_object, signal, handler, *args):
        """
        Stores the handler in the signal manager but only connects it to the
        corresponding signal if the manager is enabled.
        """
        
        handler_id = self.setdefault(an_object, {}).setdefault(signal,
            {}).setdefault(handler, None)
        
        if handler_id is None and self.enabled:
            SignalManager.connect(self, an_object, signal, handler, *args)
    
    def reconnect_all(self):
        """
        Reconnect all handlers that have been previously registered and
        possibly disconnected.
        """
        
        for an_object, signals in self.items():
            for signal, handlers in signals.items():
                for handler, handler_id in handlers.items():
                    if handler_id is None:
                        handler_id = an_object.connect(signal, handler)
                        handlers[handler] = handler_id


class SignalSynchronicityProvider(GObject):
    """
    Ensure that execution of signal handlers of all objects obeys the
    temporal order of the signal registration. All signal handlers that are not
    in temporal order are simply dropped.
    
    Suppose you have a number of similar queries:
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10.
    
    Suppose the queries are completed in the following order:
        3, 1, 2, 4, 5, 8, 7, 6, 9, 10
        
    This means that `SignalSynchronicityProvider` won't call the signal handlers
    of queries 1, 2, 6, 7, but only the signal handlers of the following
    queries, in this exact order:
        3, 4, 5, 8, 9, 10.
    """
    
    def __init__(self, signal_name, callback, *args):
        self.signal_name = signal_name
        self.callback = callback
        self.args = args
        
        self._counter = 0
        self._lock = Lock()
        self._obj_queue = []
        self._signal_ids = {}
    
    def connect(self, obj):
        with self._lock:
            signal_id = obj.connect(self.signal_name, self.on_signal, *self.args)
            
            self._signal_ids[obj] = signal_id
            self._obj_queue.append(obj)
    
    def on_signal(self, obj, *args):
        with self._lock:
            try:
                index = self._obj_queue.index(obj)
            except ValueError:
                return
            else:
                obj.disconnect(self._signal_ids.pop(obj))
                
                self._obj_queue = self._obj_queue[index+1:]
                self.callback(obj, *args)
