# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from gobject import timeout_add

from lottanzb.util.gobject_extras import GObject, gproperty, gsignal
import collections

__all__ = ["Timer"]

class _TimerBase(GObject):
    """
    Helper class for Timer.
    """
    
    stopped = gproperty(type=bool, default=True)
    timeout = gproperty(type=int, default=1000)
    
    gsignal("tick")
    
    def __init__(self, timeout):
        GObject.__init__(self)
        
        self.timeout = timeout
    
    def stop(self):
        """
        Stop the emission of the 'tick' signal.
        """
        
        if not self.stopped:
            self.stopped = True
    
    def start(self):
        """
        Start the regular emission of the 'tick' signal.
        
        The Times class ensures thanks to the _TimerSequence helper class,
        that it will exactly take the specific amount of time after this
        method is invoked until the 'tick' signal is emitted.
        """
        
        if self.stopped:
            self.stopped = False

class _TimerSequence(_TimerBase):
    """
    Helper class for Timer.
    """
    
    def start(self):
        """
        This is where the gobject.timeout_add is actually called.
        """
        
        _TimerBase.start(self)
        
        timeout_add(self.timeout, self.on_gobject_timeout)
    
    def on_gobject_timeout(self):
        if self.stopped:
            return False
        
        self.emit("tick")
        
        return True

class Timer(_TimerBase):
    """
    Wrapper for `gobject.timeout_add`, which is more object-oriented and
    which provides the two convenient methods `start` and `stop`.
    
    The timeout is the number of milliseconds between two emissions of the
    'tick' signal. Optionally, it's possible to directly specify a handler
    like using `gobject.timeout_add`.
    """
    
    def __init__(self, timeout, handler=None, *args):
        self.current_sequence = None
        
        _TimerBase.__init__(self, timeout)
        
        if isinstance(handler, collections.Callable):
            self.connect("tick", handler, *args)
        
        self.connect("notify", self.on_state_changed)
    
    def on_state_changed(self, *args):
        if self.current_sequence:
            self.current_sequence.stop()
        
        if self.stopped:
            self.current_sequence = None
        else:
            self.current_sequence = _TimerSequence(self.timeout)
            self.current_sequence.connect("tick", self.on_sequence_tick)
            self.current_sequence.start()
    
    def on_sequence_tick(self, *args):
        self.emit("tick")
