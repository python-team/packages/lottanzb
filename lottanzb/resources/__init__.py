# Copyright (C) 2008-2011 LottaNZB Development Team
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

from os import makedirs
from os.path import join, isdir

from lottanzb.resources import xdg
from lottanzb.resources.platform import (
    INSTALLED,
    DATA_DIR,
    HELP_DIR,
    LOCALE_DIR
)

UI_DIR = join(DATA_DIR, "ui")

# Starting with LottaNZB 0.5, its configuration data is no longer saved in
# ~/.lottanzb but in ~/.config/lottanzb in order to adhere to the corresponding
# freedesktop.org standard.
CONFIG_DIR = join(xdg.CONFIG_HOME, "lottanzb")
USER_DATA_DIR = join(xdg.DATA_HOME, "lottanzb")

def is_installed():
    return INSTALLED

def get_data(*subpaths):
    return join(DATA_DIR, *subpaths)

def get_help(*subpaths):
    return join(HELP_DIR, *subpaths)

def get_locale(*subpaths):
    return join(LOCALE_DIR, *subpaths)

def get_ui(*subpaths):
    return join(UI_DIR, *subpaths)

def get_config(*subpaths):
    return join(CONFIG_DIR, *subpaths)

def get_user_data(*subpaths):
    return join(USER_DATA_DIR, *subpaths)

def create_user_dirs():
    for directory in [CONFIG_DIR, USER_DATA_DIR]:
        if not isdir(directory):
            # ~/.local or ~/.local/share may not exist on fresh installations.
            # Therefore use the recursive version of the `mkdir` function.
            makedirs(directory)
